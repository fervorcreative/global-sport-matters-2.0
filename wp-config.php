<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'jj1djz1uTf7r8CaPKDg1PiATEHAQJY52NXJHoINRGutwfXQqtjID7sTKHLy1F6iRhEEwNUKj+nXk9n7yDTLRzQ==');
define('SECURE_AUTH_KEY',  'ErsuefVv+W4ejSr/MnMK+Q8MGGFMb654XykrqulnWZc5ZrFmaNuIYH4od4Jfqmx2HgHuhWdprJvra5lek1Y9sA==');
define('LOGGED_IN_KEY',    'ZHSa/Z0SQ0c56TD5F0ZS1ExU990aqhYE3x3ihUf4RgicnJ3EiVQZr6mWeMO/Vw/b7J3o48MQ4rSMTTFbNzrR3g==');
define('NONCE_KEY',        'WMbYwhP9kdvwHdPsQ3/tnPgjrIzcyYtW9iINqb50HbA40mD5pG64wz5XP6QK4OSEtbygjFUugc9mGje/iIgI8A==');
define('AUTH_SALT',        'Qr0aY2fHPi+DVFl/5iCo56bVnEp4qPPisZZOXVjtXfspZM3t1qa/Qe3rPmBXRhBwfRBt1yEw2NES1a8InpjChg==');
define('SECURE_AUTH_SALT', '1Xt0h0/gZpRJhS3/KDYZ7wocXOBPxh2RTQ02K/E7hz8vigwgv0veP5WNiaH2z/WIXtxFVNjOe9Os4+dvGocgKw==');
define('LOGGED_IN_SALT',   'hFCPKDVcVgeFjja+X4tab54Wb9o5poIMPvmTnv3DiPfbPfjVRUMfCRv03HR3fpJ0iVUFWWLo81ctNhCMUTChgA==');
define('NONCE_SALT',       'oc0Fz7ypcz8k9Xht1qzEgjj+zE+bHo6l9vSGQdYtn6WWo/4lR7F8LRuaA/qSVeCvzoNehLwkXnkBqgnLhfRs9A==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

define('WP_CACHE', 'false');




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
