-- MySQL dump 10.13  Distrib 8.0.16, for macos10.14 (x86_64)
--
-- Host: localhost    Database: local
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_commentmeta`
--

DROP TABLE IF EXISTS `wp_commentmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_commentmeta`
--

LOCK TABLES `wp_commentmeta` WRITE;
/*!40000 ALTER TABLE `wp_commentmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_commentmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_comments`
--

LOCK TABLES `wp_comments` WRITE;
/*!40000 ALTER TABLE `wp_comments` DISABLE KEYS */;
INSERT INTO `wp_comments` VALUES (1,1,'A WordPress Commenter','wapuu@wordpress.example','https://wpengine.com/','','2020-03-06 21:07:49','2020-03-06 21:07:49','Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.',0,'1','','',0,0);
/*!40000 ALTER TABLE `wp_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_gf_draft_submissions`
--

DROP TABLE IF EXISTS `wp_gf_draft_submissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_gf_draft_submissions` (
  `uuid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `form_id` mediumint(8) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `ip` varchar(39) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source_url` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `submission` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `form_id` (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_gf_draft_submissions`
--

LOCK TABLES `wp_gf_draft_submissions` WRITE;
/*!40000 ALTER TABLE `wp_gf_draft_submissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_gf_draft_submissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_gf_entry`
--

DROP TABLE IF EXISTS `wp_gf_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_gf_entry` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` mediumint(8) unsigned NOT NULL,
  `post_id` bigint(20) unsigned DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `is_starred` tinyint(1) NOT NULL DEFAULT '0',
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `ip` varchar(39) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_agent` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `currency` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `payment_status` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_amount` decimal(19,2) DEFAULT NULL,
  `payment_method` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `transaction_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_fulfilled` tinyint(1) DEFAULT NULL,
  `created_by` bigint(20) unsigned DEFAULT NULL,
  `transaction_type` tinyint(1) DEFAULT NULL,
  `status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`),
  KEY `form_id` (`form_id`),
  KEY `form_id_status` (`form_id`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_gf_entry`
--

LOCK TABLES `wp_gf_entry` WRITE;
/*!40000 ALTER TABLE `wp_gf_entry` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_gf_entry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_gf_entry_meta`
--

DROP TABLE IF EXISTS `wp_gf_entry_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_gf_entry_meta` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `entry_id` bigint(20) unsigned NOT NULL,
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `item_index` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `meta_key` (`meta_key`(191)),
  KEY `entry_id` (`entry_id`),
  KEY `meta_value` (`meta_value`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_gf_entry_meta`
--

LOCK TABLES `wp_gf_entry_meta` WRITE;
/*!40000 ALTER TABLE `wp_gf_entry_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_gf_entry_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_gf_entry_notes`
--

DROP TABLE IF EXISTS `wp_gf_entry_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_gf_entry_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `user_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `note_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `sub_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `entry_id` (`entry_id`),
  KEY `entry_user_key` (`entry_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_gf_entry_notes`
--

LOCK TABLES `wp_gf_entry_notes` WRITE;
/*!40000 ALTER TABLE `wp_gf_entry_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_gf_entry_notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_gf_form`
--

DROP TABLE IF EXISTS `wp_gf_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_gf_form` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_trash` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_gf_form`
--

LOCK TABLES `wp_gf_form` WRITE;
/*!40000 ALTER TABLE `wp_gf_form` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_gf_form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_gf_form_meta`
--

DROP TABLE IF EXISTS `wp_gf_form_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_gf_form_meta` (
  `form_id` mediumint(8) unsigned NOT NULL,
  `display_meta` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `entries_grid_meta` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `confirmations` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `notifications` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_gf_form_meta`
--

LOCK TABLES `wp_gf_form_meta` WRITE;
/*!40000 ALTER TABLE `wp_gf_form_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_gf_form_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_gf_form_revisions`
--

DROP TABLE IF EXISTS `wp_gf_form_revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_gf_form_revisions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` mediumint(8) unsigned NOT NULL,
  `display_meta` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `date_created` (`date_created`),
  KEY `form_id` (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_gf_form_revisions`
--

LOCK TABLES `wp_gf_form_revisions` WRITE;
/*!40000 ALTER TABLE `wp_gf_form_revisions` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_gf_form_revisions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_gf_form_view`
--

DROP TABLE IF EXISTS `wp_gf_form_view`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_gf_form_view` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` mediumint(8) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `ip` char(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `count` mediumint(8) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `date_created` (`date_created`),
  KEY `form_id` (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_gf_form_view`
--

LOCK TABLES `wp_gf_form_view` WRITE;
/*!40000 ALTER TABLE `wp_gf_form_view` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_gf_form_view` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_links`
--

DROP TABLE IF EXISTS `wp_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_links`
--

LOCK TABLES `wp_links` WRITE;
/*!40000 ALTER TABLE `wp_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_options`
--

DROP TABLE IF EXISTS `wp_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`),
  KEY `autoload` (`autoload`)
) ENGINE=InnoDB AUTO_INCREMENT=1723 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_options`
--

LOCK TABLES `wp_options` WRITE;
/*!40000 ALTER TABLE `wp_options` DISABLE KEYS */;
INSERT INTO `wp_options` VALUES (1,'siteurl','http://localhost:10016','yes');
INSERT INTO `wp_options` VALUES (2,'home','http://localhost:10016','yes');
INSERT INTO `wp_options` VALUES (3,'blogname','Global Sport Matters','yes');
INSERT INTO `wp_options` VALUES (4,'blogdescription','','yes');
INSERT INTO `wp_options` VALUES (5,'users_can_register','0','yes');
INSERT INTO `wp_options` VALUES (6,'admin_email','admin@fervorcreative.com','yes');
INSERT INTO `wp_options` VALUES (7,'start_of_week','1','yes');
INSERT INTO `wp_options` VALUES (8,'use_balanceTags','0','yes');
INSERT INTO `wp_options` VALUES (9,'use_smilies','1','yes');
INSERT INTO `wp_options` VALUES (10,'require_name_email','1','yes');
INSERT INTO `wp_options` VALUES (11,'comments_notify','1','yes');
INSERT INTO `wp_options` VALUES (12,'posts_per_rss','1000','yes');
INSERT INTO `wp_options` VALUES (13,'rss_use_excerpt','0','yes');
INSERT INTO `wp_options` VALUES (14,'mailserver_url','mail.example.com','yes');
INSERT INTO `wp_options` VALUES (15,'mailserver_login','login@example.com','yes');
INSERT INTO `wp_options` VALUES (16,'mailserver_pass','password','yes');
INSERT INTO `wp_options` VALUES (17,'mailserver_port','110','yes');
INSERT INTO `wp_options` VALUES (18,'default_category','1','yes');
INSERT INTO `wp_options` VALUES (19,'default_comment_status','open','yes');
INSERT INTO `wp_options` VALUES (20,'default_ping_status','open','yes');
INSERT INTO `wp_options` VALUES (21,'default_pingback_flag','0','yes');
INSERT INTO `wp_options` VALUES (22,'posts_per_page','1000','yes');
INSERT INTO `wp_options` VALUES (23,'date_format','F j, Y','yes');
INSERT INTO `wp_options` VALUES (24,'time_format','g:i a','yes');
INSERT INTO `wp_options` VALUES (25,'links_updated_date_format','F j, Y g:i a','yes');
INSERT INTO `wp_options` VALUES (26,'comment_moderation','0','yes');
INSERT INTO `wp_options` VALUES (27,'moderation_notify','1','yes');
INSERT INTO `wp_options` VALUES (28,'permalink_structure','/%postname%/','yes');
INSERT INTO `wp_options` VALUES (30,'hack_file','0','yes');
INSERT INTO `wp_options` VALUES (31,'blog_charset','UTF-8','yes');
INSERT INTO `wp_options` VALUES (32,'moderation_keys','','no');
INSERT INTO `wp_options` VALUES (33,'active_plugins','a:5:{i:0;s:29:\"gravityforms/gravityforms.php\";i:1;s:34:\"advanced-custom-fields-pro/acf.php\";i:2;s:19:\"akismet/akismet.php\";i:3;s:33:\"classic-editor/classic-editor.php\";i:4;s:24:\"wordpress-seo/wp-seo.php\";}','yes');
INSERT INTO `wp_options` VALUES (34,'category_base','','yes');
INSERT INTO `wp_options` VALUES (35,'ping_sites','http://rpc.pingomatic.com/','yes');
INSERT INTO `wp_options` VALUES (36,'comment_max_links','2','yes');
INSERT INTO `wp_options` VALUES (37,'gmt_offset','0','yes');
INSERT INTO `wp_options` VALUES (38,'default_email_category','1','yes');
INSERT INTO `wp_options` VALUES (39,'recently_edited','a:4:{i:0;s:107:\"/Users/colechristensen/Local Sites/global-sport-matters/app/public/wp-content/themes/global-sport/style.css\";i:1;s:110:\"/Users/colechristensen/Local Sites/global-sport-matters/app/public/wp-content/themes/global-sport/dist/app.css\";i:3;s:70:\"/nas/content/live/globalsport/wp-content/themes/twentytwenty/style.css\";i:4;s:0:\"\";}','no');
INSERT INTO `wp_options` VALUES (40,'template','global-sport','yes');
INSERT INTO `wp_options` VALUES (41,'stylesheet','global-sport','yes');
INSERT INTO `wp_options` VALUES (42,'comment_whitelist','1','yes');
INSERT INTO `wp_options` VALUES (43,'blacklist_keys','','no');
INSERT INTO `wp_options` VALUES (44,'comment_registration','0','yes');
INSERT INTO `wp_options` VALUES (45,'html_type','text/html','yes');
INSERT INTO `wp_options` VALUES (46,'use_trackback','0','yes');
INSERT INTO `wp_options` VALUES (47,'default_role','subscriber','yes');
INSERT INTO `wp_options` VALUES (48,'db_version','45805','yes');
INSERT INTO `wp_options` VALUES (49,'uploads_use_yearmonth_folders','1','yes');
INSERT INTO `wp_options` VALUES (50,'upload_path','','yes');
INSERT INTO `wp_options` VALUES (51,'blog_public','0','yes');
INSERT INTO `wp_options` VALUES (52,'default_link_category','2','yes');
INSERT INTO `wp_options` VALUES (53,'show_on_front','page','yes');
INSERT INTO `wp_options` VALUES (54,'tag_base','','yes');
INSERT INTO `wp_options` VALUES (55,'show_avatars','1','yes');
INSERT INTO `wp_options` VALUES (56,'avatar_rating','G','yes');
INSERT INTO `wp_options` VALUES (57,'upload_url_path','','yes');
INSERT INTO `wp_options` VALUES (58,'thumbnail_size_w','150','yes');
INSERT INTO `wp_options` VALUES (59,'thumbnail_size_h','150','yes');
INSERT INTO `wp_options` VALUES (60,'thumbnail_crop','1','yes');
INSERT INTO `wp_options` VALUES (61,'medium_size_w','300','yes');
INSERT INTO `wp_options` VALUES (62,'medium_size_h','300','yes');
INSERT INTO `wp_options` VALUES (63,'avatar_default','mystery','yes');
INSERT INTO `wp_options` VALUES (64,'large_size_w','1024','yes');
INSERT INTO `wp_options` VALUES (65,'large_size_h','1024','yes');
INSERT INTO `wp_options` VALUES (66,'image_default_link_type','none','yes');
INSERT INTO `wp_options` VALUES (67,'image_default_size','','yes');
INSERT INTO `wp_options` VALUES (68,'image_default_align','','yes');
INSERT INTO `wp_options` VALUES (69,'close_comments_for_old_posts','0','yes');
INSERT INTO `wp_options` VALUES (70,'close_comments_days_old','14','yes');
INSERT INTO `wp_options` VALUES (71,'thread_comments','1','yes');
INSERT INTO `wp_options` VALUES (72,'thread_comments_depth','5','yes');
INSERT INTO `wp_options` VALUES (73,'page_comments','0','yes');
INSERT INTO `wp_options` VALUES (74,'comments_per_page','50','yes');
INSERT INTO `wp_options` VALUES (75,'default_comments_page','newest','yes');
INSERT INTO `wp_options` VALUES (76,'comment_order','asc','yes');
INSERT INTO `wp_options` VALUES (77,'sticky_posts','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (78,'widget_categories','a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (79,'widget_text','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (80,'widget_rss','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (81,'uninstall_plugins','a:1:{s:33:\"classic-editor/classic-editor.php\";a:2:{i:0;s:14:\"Classic_Editor\";i:1;s:9:\"uninstall\";}}','no');
INSERT INTO `wp_options` VALUES (82,'timezone_string','','yes');
INSERT INTO `wp_options` VALUES (83,'page_for_posts','0','yes');
INSERT INTO `wp_options` VALUES (84,'page_on_front','2','yes');
INSERT INTO `wp_options` VALUES (85,'default_post_format','0','yes');
INSERT INTO `wp_options` VALUES (86,'link_manager_enabled','0','yes');
INSERT INTO `wp_options` VALUES (87,'finished_splitting_shared_terms','1','yes');
INSERT INTO `wp_options` VALUES (88,'site_icon','0','yes');
INSERT INTO `wp_options` VALUES (89,'medium_large_size_w','768','yes');
INSERT INTO `wp_options` VALUES (90,'medium_large_size_h','0','yes');
INSERT INTO `wp_options` VALUES (91,'wp_page_for_privacy_policy','3','yes');
INSERT INTO `wp_options` VALUES (92,'show_comments_cookies_opt_in','1','yes');
INSERT INTO `wp_options` VALUES (93,'admin_email_lifespan','1599080869','yes');
INSERT INTO `wp_options` VALUES (94,'initial_db_version','45805','yes');
INSERT INTO `wp_options` VALUES (95,'wp_user_roles','a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:62:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:20:\"wpseo_manage_options\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:13:\"wpseo_manager\";a:2:{s:4:\"name\";s:11:\"SEO Manager\";s:12:\"capabilities\";a:38:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;s:20:\"wpseo_manage_options\";b:1;s:23:\"view_site_health_checks\";b:1;}}s:12:\"wpseo_editor\";a:2:{s:4:\"name\";s:10:\"SEO Editor\";s:12:\"capabilities\";a:36:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:15:\"wpseo_bulk_edit\";b:1;s:28:\"wpseo_edit_advanced_metadata\";b:1;}}}','yes');
INSERT INTO `wp_options` VALUES (96,'fresh_site','0','yes');
INSERT INTO `wp_options` VALUES (97,'widget_search','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (98,'widget_recent-posts','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (99,'widget_recent-comments','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (100,'widget_archives','a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (101,'widget_meta','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (102,'sidebars_widgets','a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}','yes');
INSERT INTO `wp_options` VALUES (103,'cron','a:8:{i:1585843672;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1585861672;a:4:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1585862137;a:1:{s:39:\"WPEngineSecurityAuditor_Scans_scheduler\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1585863665;a:3:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1585864526;a:1:{s:17:\"gravityforms_cron\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1585864625;a:1:{s:19:\"wpseo-reindex-links\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1585951025;a:1:{s:16:\"wpseo_ryte_fetch\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}','yes');
INSERT INTO `wp_options` VALUES (104,'widget_pages','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (105,'widget_calendar','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (106,'widget_media_audio','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (107,'widget_media_image','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (108,'widget_media_gallery','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (109,'widget_media_video','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (110,'widget_tag_cloud','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (111,'widget_nav_menu','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (112,'widget_custom_html','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (114,'widget_wpe_powered_by_widget','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (116,'_site_transient_update_core','O:8:\"stdClass\":4:{s:7:\"updates\";a:2:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.4.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.4.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-5.4-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.4-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"5.4\";s:7:\"version\";s:3:\"5.4\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.4.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.4.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-5.4-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.4-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"5.4\";s:7:\"version\";s:3:\"5.4\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}}s:12:\"last_checked\";i:1585842289;s:15:\"version_checked\";s:5:\"5.3.2\";s:12:\"translations\";a:0:{}}','no');
INSERT INTO `wp_options` VALUES (121,'theme_mods_twentytwenty','a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1583534108;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}','yes');
INSERT INTO `wp_options` VALUES (127,'wpe_notices','a:1:{s:4:\"read\";s:0:\"\";}','yes');
INSERT INTO `wp_options` VALUES (128,'wpe_notices_ttl','1583534465','yes');
INSERT INTO `wp_options` VALUES (129,'can_compress_scripts','0','no');
INSERT INTO `wp_options` VALUES (144,'WPLANG','','yes');
INSERT INTO `wp_options` VALUES (145,'new_admin_email','admin@fervorcreative.com','yes');
INSERT INTO `wp_options` VALUES (150,'recently_activated','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (152,'widget_akismet_widget','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (153,'akismet_strictness','1','yes');
INSERT INTO `wp_options` VALUES (154,'akismet_show_user_comments_approved','0','yes');
INSERT INTO `wp_options` VALUES (155,'akismet_comment_form_privacy_notice','hide','yes');
INSERT INTO `wp_options` VALUES (156,'wordpress_api_key','248a71147e63','yes');
INSERT INTO `wp_options` VALUES (157,'akismet_spam_count','0','yes');
INSERT INTO `wp_options` VALUES (166,'gf_db_version','2.4.17.9','no');
INSERT INTO `wp_options` VALUES (167,'rg_form_version','2.4.17.9','no');
INSERT INTO `wp_options` VALUES (168,'gform_enable_background_updates','1','yes');
INSERT INTO `wp_options` VALUES (169,'gform_pending_installation','','yes');
INSERT INTO `wp_options` VALUES (170,'widget_gform_widget','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (171,'gravityformsaddon_gravityformswebapi_version','1.0','yes');
INSERT INTO `wp_options` VALUES (172,'gform_version_info','a:11:{s:12:\"is_valid_key\";b:1;s:6:\"reason\";s:0:\"\";s:7:\"version\";s:6:\"2.4.17\";s:3:\"url\";s:166:\"https://s3.amazonaws.com/gravityforms/releases/gravityforms_2.4.17.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=TN4ax4sETiAgtcBRB7Dt7g7zbxQ%3D\";s:15:\"expiration_time\";i:1607011200;s:9:\"offerings\";a:56:{s:12:\"gravityforms\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:6:\"2.4.17\";s:14:\"version_latest\";s:9:\"2.4.17.17\";s:3:\"url\";s:166:\"https://s3.amazonaws.com/gravityforms/releases/gravityforms_2.4.17.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=TN4ax4sETiAgtcBRB7Dt7g7zbxQ%3D\";s:10:\"url_latest\";s:171:\"https://s3.amazonaws.com/gravityforms/releases/gravityforms_2.4.17.17.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=pkXTaJCwlRxI4ifWK%2BHNGkqkhEY%3D\";}s:21:\"gravityforms2checkout\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.0\";s:14:\"version_latest\";s:5:\"1.0.2\";s:3:\"url\";s:182:\"https://s3.amazonaws.com/gravityforms/addons/2checkout/gravityforms2checkout_1.0.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=vUswmMUj5CjWH2rza6f%2FhJJNbig%3D\";s:10:\"url_latest\";s:184:\"https://s3.amazonaws.com/gravityforms/addons/2checkout/gravityforms2checkout_1.0.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=AGU6%2F8MCmhKpp2MA9MCeUnEG8OQ%3D\";}s:26:\"gravityformsactivecampaign\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.7\";s:14:\"version_latest\";s:3:\"1.7\";s:3:\"url\";s:190:\"https://s3.amazonaws.com/gravityforms/addons/activecampaign/gravityformsactivecampaign_1.7.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=BbD644gxHYFHAhRtn4bG5rzGUMk%3D\";s:10:\"url_latest\";s:190:\"https://s3.amazonaws.com/gravityforms/addons/activecampaign/gravityformsactivecampaign_1.7.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=BbD644gxHYFHAhRtn4bG5rzGUMk%3D\";}s:32:\"gravityformsadvancedpostcreation\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:10:\"1.0-beta-3\";s:14:\"version_latest\";s:12:\"1.0-beta-3.5\";s:3:\"url\";s:209:\"https://s3.amazonaws.com/gravityforms/addons/advancedpostcreation/gravityformsadvancedpostcreation_1.0-beta-3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=MWKmdvV3CT7obnfWzvJSezZkMMM%3D\";s:10:\"url_latest\";s:213:\"https://s3.amazonaws.com/gravityforms/addons/advancedpostcreation/gravityformsadvancedpostcreation_1.0-beta-3.5.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=j%2FkxDplsU3hQx1xPyD492DINQlE%3D\";}s:20:\"gravityformsagilecrm\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:3:\"1.3\";s:3:\"url\";s:182:\"https://s3.amazonaws.com/gravityforms/addons/agilecrm/gravityformsagilecrm_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=9vN5XE39OWSJ%2FVb%2BQddqqlgmBAE%3D\";s:10:\"url_latest\";s:182:\"https://s3.amazonaws.com/gravityforms/addons/agilecrm/gravityformsagilecrm_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=9vN5XE39OWSJ%2FVb%2BQddqqlgmBAE%3D\";}s:24:\"gravityformsauthorizenet\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.7\";s:14:\"version_latest\";s:5:\"2.7.1\";s:3:\"url\";s:186:\"https://s3.amazonaws.com/gravityforms/addons/authorizenet/gravityformsauthorizenet_2.7.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=NQ82zxQZeqeMRfeysfsesx0FcKU%3D\";s:10:\"url_latest\";s:188:\"https://s3.amazonaws.com/gravityforms/addons/authorizenet/gravityformsauthorizenet_2.7.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=gsh6RtEjQ94wypMUYP5bgQmXN2U%3D\";}s:18:\"gravityformsaweber\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:4:\"2.10\";s:14:\"version_latest\";s:6:\"2.10.1\";s:3:\"url\";s:179:\"https://s3.amazonaws.com/gravityforms/addons/aweber/gravityformsaweber_2.10.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=0swV6ubAKmf%2F2qL%2FwHOljbils44%3D\";s:10:\"url_latest\";s:177:\"https://s3.amazonaws.com/gravityforms/addons/aweber/gravityformsaweber_2.10.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=9VfgErR3CEYyuheSXMekbwGN8jE%3D\";}s:21:\"gravityformsbatchbook\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:3:\"1.3\";s:3:\"url\";s:184:\"https://s3.amazonaws.com/gravityforms/addons/batchbook/gravityformsbatchbook_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=cyN57%2BYtpFN9paa%2Fqcgt9ukP1k4%3D\";s:10:\"url_latest\";s:184:\"https://s3.amazonaws.com/gravityforms/addons/batchbook/gravityformsbatchbook_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=cyN57%2BYtpFN9paa%2Fqcgt9ukP1k4%3D\";}s:18:\"gravityformsbreeze\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.4\";s:14:\"version_latest\";s:3:\"1.4\";s:3:\"url\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/breeze/gravityformsbreeze_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=tg0Xpr%2BxChj0l0tZLha1v%2BHEpA4%3D\";s:10:\"url_latest\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/breeze/gravityformsbreeze_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=tg0Xpr%2BxChj0l0tZLha1v%2BHEpA4%3D\";}s:27:\"gravityformscampaignmonitor\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.8\";s:14:\"version_latest\";s:3:\"3.8\";s:3:\"url\";s:194:\"https://s3.amazonaws.com/gravityforms/addons/campaignmonitor/gravityformscampaignmonitor_3.8.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=Ao6CCl%2BOFkVUEhP5UNj7ARApa3Q%3D\";s:10:\"url_latest\";s:194:\"https://s3.amazonaws.com/gravityforms/addons/campaignmonitor/gravityformscampaignmonitor_3.8.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=Ao6CCl%2BOFkVUEhP5UNj7ARApa3Q%3D\";}s:20:\"gravityformscampfire\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:5:\"1.2.2\";s:3:\"url\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/campfire/gravityformscampfire_1.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=qRnBT2zoseZk3EE%2F1YhH04VWOzc%3D\";s:10:\"url_latest\";s:184:\"https://s3.amazonaws.com/gravityforms/addons/campfire/gravityformscampfire_1.2.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=EqBiUGIWu%2BMDrbyrCh9NEuYvG%2Fg%3D\";}s:22:\"gravityformscapsulecrm\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.4\";s:14:\"version_latest\";s:3:\"1.4\";s:3:\"url\";s:188:\"https://s3.amazonaws.com/gravityforms/addons/capsulecrm/gravityformscapsulecrm_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=gM%2FoPcQf1k%2BCpTLIng%2FHxKKKJdk%3D\";s:10:\"url_latest\";s:188:\"https://s3.amazonaws.com/gravityforms/addons/capsulecrm/gravityformscapsulecrm_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=gM%2FoPcQf1k%2BCpTLIng%2FHxKKKJdk%3D\";}s:26:\"gravityformschainedselects\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:5:\"1.3.4\";s:3:\"url\";s:190:\"https://s3.amazonaws.com/gravityforms/addons/chainedselects/gravityformschainedselects_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=VcO4h2CNklFApYFYRKwUAzYqTuQ%3D\";s:10:\"url_latest\";s:194:\"https://s3.amazonaws.com/gravityforms/addons/chainedselects/gravityformschainedselects_1.3.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=6YNs37DmSaFyLTv5dCGJ8K%2BkJMM%3D\";}s:23:\"gravityformscleverreach\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.6\";s:14:\"version_latest\";s:5:\"1.6.1\";s:3:\"url\";s:184:\"https://s3.amazonaws.com/gravityforms/addons/cleverreach/gravityformscleverreach_1.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=xIHqmsruGJWuzn3nsQzxO20YUho%3D\";s:10:\"url_latest\";s:188:\"https://s3.amazonaws.com/gravityforms/addons/cleverreach/gravityformscleverreach_1.6.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=93NJdZlE6iBxsxDB1VW4W%2Bym7mE%3D\";}s:27:\"gravityformsconstantcontact\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:3:\"1.3\";s:3:\"url\";s:192:\"https://s3.amazonaws.com/gravityforms/addons/constantcontact/gravityformsconstantcontact_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=13yi6uSS7JWcSEAEk60r7Hglgtg%3D\";s:10:\"url_latest\";s:192:\"https://s3.amazonaws.com/gravityforms/addons/constantcontact/gravityformsconstantcontact_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=13yi6uSS7JWcSEAEk60r7Hglgtg%3D\";}s:19:\"gravityformscoupons\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.9\";s:14:\"version_latest\";s:5:\"2.9.4\";s:3:\"url\";s:176:\"https://s3.amazonaws.com/gravityforms/addons/coupons/gravityformscoupons_2.9.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=gaEmt3WmMgadSL7yUOTty1Fuzpw%3D\";s:10:\"url_latest\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/coupons/gravityformscoupons_2.9.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=ekH2R9fi4NeBqVceVxviP3GGFlY%3D\";}s:17:\"gravityformsdebug\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:0:\"\";s:14:\"version_latest\";s:10:\"1.0.beta12\";s:3:\"url\";s:0:\"\";s:10:\"url_latest\";s:179:\"https://s3.amazonaws.com/gravityforms/addons/debug/gravityformsdebug_1.0.beta12.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=nK45Ev7xik4WCgrOHIqtwVMtjQA%3D\";}s:19:\"gravityformsdropbox\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.6\";s:14:\"version_latest\";s:3:\"2.6\";s:3:\"url\";s:176:\"https://s3.amazonaws.com/gravityforms/addons/dropbox/gravityformsdropbox_2.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=imG7U289jswZL5omKGW1SA7Er9M%3D\";s:10:\"url_latest\";s:176:\"https://s3.amazonaws.com/gravityforms/addons/dropbox/gravityformsdropbox_2.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=imG7U289jswZL5omKGW1SA7Er9M%3D\";}s:24:\"gravityformsemailoctopus\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:3:\"1.1\";s:3:\"url\";s:186:\"https://s3.amazonaws.com/gravityforms/addons/emailoctopus/gravityformsemailoctopus_1.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=HJA8q95Z3ZJOydNKcejCfw5GvFI%3D\";s:10:\"url_latest\";s:186:\"https://s3.amazonaws.com/gravityforms/addons/emailoctopus/gravityformsemailoctopus_1.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=HJA8q95Z3ZJOydNKcejCfw5GvFI%3D\";}s:16:\"gravityformsemma\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:3:\"1.3\";s:3:\"url\";s:172:\"https://s3.amazonaws.com/gravityforms/addons/emma/gravityformsemma_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=z1ITSx%2BxJwelsX58GjozkhHcEVs%3D\";s:10:\"url_latest\";s:172:\"https://s3.amazonaws.com/gravityforms/addons/emma/gravityformsemma_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=z1ITSx%2BxJwelsX58GjozkhHcEVs%3D\";}s:22:\"gravityformsfreshbooks\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.6\";s:14:\"version_latest\";s:5:\"2.6.1\";s:3:\"url\";s:184:\"https://s3.amazonaws.com/gravityforms/addons/freshbooks/gravityformsfreshbooks_2.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=St%2BrwLTuz5HwQwCSw9LbX8VKelg%3D\";s:10:\"url_latest\";s:186:\"https://s3.amazonaws.com/gravityforms/addons/freshbooks/gravityformsfreshbooks_2.6.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=yq0qBfV1HqPDmwOCM%2FU24r4liQ0%3D\";}s:23:\"gravityformsgetresponse\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.4\";s:14:\"version_latest\";s:5:\"1.4.1\";s:3:\"url\";s:186:\"https://s3.amazonaws.com/gravityforms/addons/getresponse/gravityformsgetresponse_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=wE8r3EqWDzgq2rSB%2BaGFEzjadsM%3D\";s:10:\"url_latest\";s:192:\"https://s3.amazonaws.com/gravityforms/addons/getresponse/gravityformsgetresponse_1.4.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=XvqP8vTu2BIry%2B8WxA%2F%2BQKAe4SA%3D\";}s:21:\"gravityformsgutenberg\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:10:\"1.0-rc-1.4\";s:14:\"version_latest\";s:10:\"1.0-rc-1.4\";s:3:\"url\";s:189:\"https://s3.amazonaws.com/gravityforms/addons/gutenberg/gravityformsgutenberg_1.0-rc-1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=CRAx8lpfSiP15GkcqUiEKCs%2BiOQ%3D\";s:10:\"url_latest\";s:189:\"https://s3.amazonaws.com/gravityforms/addons/gutenberg/gravityformsgutenberg_1.0-rc-1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=CRAx8lpfSiP15GkcqUiEKCs%2BiOQ%3D\";}s:21:\"gravityformshelpscout\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:4:\"1.12\";s:14:\"version_latest\";s:6:\"1.12.2\";s:3:\"url\";s:181:\"https://s3.amazonaws.com/gravityforms/addons/helpscout/gravityformshelpscout_1.12.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=Y82PfSROXSx4lmRcGFHonDuM2zs%3D\";s:10:\"url_latest\";s:185:\"https://s3.amazonaws.com/gravityforms/addons/helpscout/gravityformshelpscout_1.12.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=I5KER%2BYkjWhwnaXGqZfHnTre6jw%3D\";}s:20:\"gravityformshighrise\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:3:\"1.3\";s:3:\"url\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/highrise/gravityformshighrise_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=gBqO80O%2BOdeM6JDnaWezDZO9b6I%3D\";s:10:\"url_latest\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/highrise/gravityformshighrise_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=gBqO80O%2BOdeM6JDnaWezDZO9b6I%3D\";}s:19:\"gravityformshipchat\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:3:\"1.2\";}s:19:\"gravityformshubspot\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:5:\"1.2.2\";s:3:\"url\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/hubspot/gravityformshubspot_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=r0Jickkj7%2FIDEW9e3e4PxYYdIgY%3D\";s:10:\"url_latest\";s:182:\"https://s3.amazonaws.com/gravityforms/addons/hubspot/gravityformshubspot_1.2.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=q7kt%2BgkMFNwDIGGbIi%2FM6fX0slc%3D\";}s:20:\"gravityformsicontact\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.4\";s:14:\"version_latest\";s:5:\"1.4.1\";s:3:\"url\";s:182:\"https://s3.amazonaws.com/gravityforms/addons/icontact/gravityformsicontact_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=H0PcC%2F5rT8WKS9byrC7%2BJjKKMsc%3D\";s:10:\"url_latest\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/icontact/gravityformsicontact_1.4.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=i0PuDL0iVzNTdwBYgL3Gmlytxzw%3D\";}s:19:\"gravityformslogging\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:5:\"1.3.1\";s:3:\"url\";s:176:\"https://s3.amazonaws.com/gravityforms/addons/logging/gravityformslogging_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=rU5gZQLrPRfDx9lafdQmIr2eqm0%3D\";s:10:\"url_latest\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/logging/gravityformslogging_1.3.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=Nm9at6zGKEOzONDbYBsJVKMIqr8%3D\";}s:19:\"gravityformsmadmimi\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:5:\"1.3.1\";s:3:\"url\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/madmimi/gravityformsmadmimi_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=93SVjDd8tTOxgbwYIy2%2BvmKwxdw%3D\";s:10:\"url_latest\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/madmimi/gravityformsmadmimi_1.3.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=sY6jrJIQHFZtcwhsqtdoZRsqxnA%3D\";}s:21:\"gravityformsmailchimp\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"4.7\";s:14:\"version_latest\";s:3:\"4.7\";s:3:\"url\";s:184:\"https://s3.amazonaws.com/gravityforms/addons/mailchimp/gravityformsmailchimp_4.7.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=wI7U%2F8aYmB8qwFByICwnLrB%2BEbE%3D\";s:10:\"url_latest\";s:184:\"https://s3.amazonaws.com/gravityforms/addons/mailchimp/gravityformsmailchimp_4.7.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=wI7U%2F8aYmB8qwFByICwnLrB%2BEbE%3D\";}s:19:\"gravityformsmailgun\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:5:\"1.1.4\";s:3:\"url\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/mailgun/gravityformsmailgun_1.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=VSr7vVBHqgT%2FvKJUq3clSzshHtQ%3D\";s:10:\"url_latest\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/mailgun/gravityformsmailgun_1.1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=N3QxoTkZPFXGnbgV%2B27FxJIek1I%3D\";}s:26:\"gravityformspartialentries\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.4\";s:14:\"version_latest\";s:5:\"1.4.1\";s:3:\"url\";s:194:\"https://s3.amazonaws.com/gravityforms/addons/partialentries/gravityformspartialentries_1.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=S21gvH3lck%2FzGz6ogdYcX%2FtelTE%3D\";s:10:\"url_latest\";s:192:\"https://s3.amazonaws.com/gravityforms/addons/partialentries/gravityformspartialentries_1.4.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=fQI7yPOMXZNxDEQ1qdQ61tvXNLg%3D\";}s:18:\"gravityformspaypal\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.2\";s:14:\"version_latest\";s:5:\"3.2.2\";s:3:\"url\";s:174:\"https://s3.amazonaws.com/gravityforms/addons/paypal/gravityformspaypal_3.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=Bj7KjU0CxVm60UM5AhEakIIpJSQ%3D\";s:10:\"url_latest\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/paypal/gravityformspaypal_3.2.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=UYQEmU9i0MrzuO%2BiexoeLSfXWYs%3D\";}s:33:\"gravityformspaypalexpresscheckout\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:0:\"\";s:14:\"version_latest\";N;}s:29:\"gravityformspaypalpaymentspro\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.4\";s:14:\"version_latest\";s:5:\"2.4.1\";s:3:\"url\";s:198:\"https://s3.amazonaws.com/gravityforms/addons/paypalpaymentspro/gravityformspaypalpaymentspro_2.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=OM7DaTnAYCSQTdzUXHuX03El%2Be4%3D\";s:10:\"url_latest\";s:198:\"https://s3.amazonaws.com/gravityforms/addons/paypalpaymentspro/gravityformspaypalpaymentspro_2.4.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=pbaWI5j0Jm5XBPl17RuJBup3EAs%3D\";}s:21:\"gravityformspaypalpro\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:5:\"1.8.1\";s:14:\"version_latest\";s:5:\"1.8.3\";s:3:\"url\";s:190:\"https://s3.amazonaws.com/gravityforms/addons/paypalpro/gravityformspaypalpro_1.8.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=l%2FEu%2FKO%2FxllF8XuE9eWvJyqt%2FK4%3D\";s:10:\"url_latest\";s:182:\"https://s3.amazonaws.com/gravityforms/addons/paypalpro/gravityformspaypalpro_1.8.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=Zx1BJ1FUXanBj4Kf1Z31ioP7mOc%3D\";}s:20:\"gravityformspicatcha\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"2.0\";s:14:\"version_latest\";s:3:\"2.0\";}s:16:\"gravityformspipe\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:5:\"1.1.1\";s:3:\"url\";s:170:\"https://s3.amazonaws.com/gravityforms/addons/pipe/gravityformspipe_1.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=5khLuGMyqbC8EhgOT8hcM3iKZMY%3D\";s:10:\"url_latest\";s:174:\"https://s3.amazonaws.com/gravityforms/addons/pipe/gravityformspipe_1.1.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=u4HQyU8buXQkUJyUkx%2F8cT2pEA8%3D\";}s:17:\"gravityformspolls\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.4\";s:14:\"version_latest\";s:5:\"3.4.4\";s:3:\"url\";s:176:\"https://s3.amazonaws.com/gravityforms/addons/polls/gravityformspolls_3.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=Ix%2B2mPaqLgHYAczaBHuIeuF%2Fb0E%3D\";s:10:\"url_latest\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/polls/gravityformspolls_3.4.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=9nh%2B1x%2F1YEaPI1MV9W2qjyaWeWI%3D\";}s:20:\"gravityformspostmark\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:3:\"1.1\";s:3:\"url\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/postmark/gravityformspostmark_1.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=zspcoozL%2BV3cm4FVGvp9D7BEO1c%3D\";s:10:\"url_latest\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/postmark/gravityformspostmark_1.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=zspcoozL%2BV3cm4FVGvp9D7BEO1c%3D\";}s:16:\"gravityformsppcp\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.0\";s:14:\"version_latest\";s:5:\"1.0.1\";s:3:\"url\";s:172:\"https://s3.amazonaws.com/gravityforms/addons/ppcp/gravityformsppcp_1.0.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=Q0weIqyFx6eIFOuH5xDtxISFmL4%3D\";s:10:\"url_latest\";s:172:\"https://s3.amazonaws.com/gravityforms/addons/ppcp/gravityformsppcp_1.0.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=Q0weIqyFx6eIFOuH5xDtxISFmL4%3D\";}s:16:\"gravityformsquiz\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.2\";s:14:\"version_latest\";s:5:\"3.2.4\";s:3:\"url\";s:172:\"https://s3.amazonaws.com/gravityforms/addons/quiz/gravityformsquiz_3.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=Gp7dPEy0AAQ15dxi2Jp%2Br4Yghq0%3D\";s:10:\"url_latest\";s:174:\"https://s3.amazonaws.com/gravityforms/addons/quiz/gravityformsquiz_3.2.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=BJM0hCuKESr2o%2Bi9VEcL2nnyk4Q%3D\";}s:19:\"gravityformsrestapi\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:10:\"2.0-beta-2\";s:14:\"version_latest\";s:10:\"2.0-beta-2\";s:3:\"url\";s:183:\"https://s3.amazonaws.com/gravityforms/addons/restapi/gravityformsrestapi_2.0-beta-2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=LbxSaihnK9qQpGzjKqomXUWrz1o%3D\";s:10:\"url_latest\";s:183:\"https://s3.amazonaws.com/gravityforms/addons/restapi/gravityformsrestapi_2.0-beta-2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=LbxSaihnK9qQpGzjKqomXUWrz1o%3D\";}s:20:\"gravityformssendgrid\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:3:\"1.3\";s:3:\"url\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/sendgrid/gravityformssendgrid_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=OCrL2mbDIPTqa%2FIaUYGrRKp8IpU%3D\";s:10:\"url_latest\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/sendgrid/gravityformssendgrid_1.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=OCrL2mbDIPTqa%2FIaUYGrRKp8IpU%3D\";}s:21:\"gravityformssignature\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.8\";s:14:\"version_latest\";s:5:\"3.8.3\";s:3:\"url\";s:182:\"https://s3.amazonaws.com/gravityforms/addons/signature/gravityformssignature_3.8.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=AaddrgNwc9hKzLYOtEuuw%2FqRYjc%3D\";s:10:\"url_latest\";s:184:\"https://s3.amazonaws.com/gravityforms/addons/signature/gravityformssignature_3.8.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=ISP3bZXSHupHLGgEbz%2BlU0HAWfk%3D\";}s:17:\"gravityformsslack\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.9\";s:14:\"version_latest\";s:3:\"1.9\";s:3:\"url\";s:172:\"https://s3.amazonaws.com/gravityforms/addons/slack/gravityformsslack_1.9.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=BT3kZHyzQQiH2Uf2oqMYoNmeUUo%3D\";s:10:\"url_latest\";s:172:\"https://s3.amazonaws.com/gravityforms/addons/slack/gravityformsslack_1.9.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=BT3kZHyzQQiH2Uf2oqMYoNmeUUo%3D\";}s:18:\"gravityformssquare\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:5:\"1.0.0\";s:14:\"version_latest\";s:5:\"1.0.2\";s:3:\"url\";s:176:\"https://s3.amazonaws.com/gravityforms/addons/square/gravityformssquare_1.0.0.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=25XCVjgOj6Ko6yJ4a6ORabFyMPE%3D\";s:10:\"url_latest\";s:176:\"https://s3.amazonaws.com/gravityforms/addons/square/gravityformssquare_1.0.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=YACFa2t2GknB0ozY94lITpNZZ04%3D\";}s:18:\"gravityformsstripe\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.4\";s:14:\"version_latest\";s:5:\"3.4.2\";s:3:\"url\";s:174:\"https://s3.amazonaws.com/gravityforms/addons/stripe/gravityformsstripe_3.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=gloQh0rYgc3PpibsXGbZee9j4P4%3D\";s:10:\"url_latest\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/stripe/gravityformsstripe_3.4.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=7zGWiWPzOvTKOxqxZ7Vsr%2BPQVZ8%3D\";}s:18:\"gravityformssurvey\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.4\";s:14:\"version_latest\";s:3:\"3.4\";s:3:\"url\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/survey/gravityformssurvey_3.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=g0RJ8wy4keyNevngN5m%2FWw%2Bl4T0%3D\";s:10:\"url_latest\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/survey/gravityformssurvey_3.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=g0RJ8wy4keyNevngN5m%2FWw%2Bl4T0%3D\";}s:18:\"gravityformstrello\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:5:\"1.2.3\";s:3:\"url\";s:174:\"https://s3.amazonaws.com/gravityforms/addons/trello/gravityformstrello_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=v1pRpvvy9PwUfFeoufB2G64iNGA%3D\";s:10:\"url_latest\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/trello/gravityformstrello_1.2.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=5SyHcG%2BUGEbNWj5zHri535gTf0c%3D\";}s:18:\"gravityformstwilio\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.6\";s:14:\"version_latest\";s:5:\"2.6.2\";s:3:\"url\";s:174:\"https://s3.amazonaws.com/gravityforms/addons/twilio/gravityformstwilio_2.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=owmAM8UdW8ScwPlo52eXKtmDonY%3D\";s:10:\"url_latest\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/twilio/gravityformstwilio_2.6.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=CHwuBNc1bqWZhq0v%2Bpae2xQOTD4%3D\";}s:28:\"gravityformsuserregistration\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"4.4\";s:14:\"version_latest\";s:5:\"4.4.6\";s:3:\"url\";s:196:\"https://s3.amazonaws.com/gravityforms/addons/userregistration/gravityformsuserregistration_4.4.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=J8By1SXTyqkN5%2FQp17ZItAZdpKA%3D\";s:10:\"url_latest\";s:196:\"https://s3.amazonaws.com/gravityforms/addons/userregistration/gravityformsuserregistration_4.4.6.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=LCyMtsHO2Bwnv3PnX33hSCk0y4M%3D\";}s:20:\"gravityformswebhooks\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:5:\"1.2.2\";s:3:\"url\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/webhooks/gravityformswebhooks_1.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=aqqx3DYlzJKF21MMM2vcB%2FsKffo%3D\";s:10:\"url_latest\";s:180:\"https://s3.amazonaws.com/gravityforms/addons/webhooks/gravityformswebhooks_1.2.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=6msBk1iKm6O37GfrBABaI59dt4s%3D\";}s:18:\"gravityformszapier\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.2\";s:14:\"version_latest\";s:5:\"3.2.1\";s:3:\"url\";s:176:\"https://s3.amazonaws.com/gravityforms/addons/zapier/gravityformszapier_3.2.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=RLr1GWcouzMFqnj7afO%2FyWtHduQ%3D\";s:10:\"url_latest\";s:176:\"https://s3.amazonaws.com/gravityforms/addons/zapier/gravityformszapier_3.2.1.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=5U6L3McF9HXmpQuzE7cP5qhKUxo%3D\";}s:19:\"gravityformszohocrm\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.8\";s:14:\"version_latest\";s:5:\"1.8.3\";s:3:\"url\";s:176:\"https://s3.amazonaws.com/gravityforms/addons/zohocrm/gravityformszohocrm_1.8.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=jXIDEpCBD4NgHUIVFcFoUDoGxHc%3D\";s:10:\"url_latest\";s:178:\"https://s3.amazonaws.com/gravityforms/addons/zohocrm/gravityformszohocrm_1.8.3.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=5tiS9W3CLTo2MoTIMeUVPp3cpio%3D\";}}s:9:\"is_active\";s:1:\"1\";s:12:\"product_code\";s:5:\"GFDEV\";s:14:\"version_latest\";s:9:\"2.4.17.17\";s:10:\"url_latest\";s:171:\"https://s3.amazonaws.com/gravityforms/releases/gravityforms_2.4.17.17.zip?AWSAccessKeyId=AKIAJC3LQNDWHBOFBQIA&Expires=1585940998&Signature=pkXTaJCwlRxI4ifWK%2BHNGkqkhEY%3D\";s:9:\"timestamp\";i:1585768198;}','no');
INSERT INTO `wp_options` VALUES (178,'rg_gforms_key','d4f29f9cfd88ff7099aef48dc2c66c38','yes');
INSERT INTO `wp_options` VALUES (179,'gf_site_key','0dc3460a-ebf9-41aa-b592-d1cbcfbb2678','yes');
INSERT INTO `wp_options` VALUES (180,'gf_site_secret','49931e35-7419-4be1-bc92-a58e0af9fd86','yes');
INSERT INTO `wp_options` VALUES (181,'rg_gforms_enable_akismet','1','yes');
INSERT INTO `wp_options` VALUES (182,'rg_gforms_currency','USD','yes');
INSERT INTO `wp_options` VALUES (185,'acf_version','5.8.9','yes');
INSERT INTO `wp_options` VALUES (190,'wpseo','a:20:{s:15:\"ms_defaults_set\";b:0;s:7:\"version\";s:6:\"13.4.1\";s:20:\"disableadvanced_meta\";b:0;s:17:\"ryte_indexability\";b:0;s:11:\"baiduverify\";s:0:\"\";s:12:\"googleverify\";s:0:\"\";s:8:\"msverify\";s:0:\"\";s:12:\"yandexverify\";s:0:\"\";s:9:\"site_type\";s:0:\"\";s:20:\"has_multiple_authors\";s:0:\"\";s:16:\"environment_type\";s:0:\"\";s:23:\"content_analysis_active\";b:0;s:23:\"keyword_analysis_active\";b:1;s:21:\"enable_admin_bar_menu\";b:0;s:26:\"enable_cornerstone_content\";b:0;s:18:\"enable_xml_sitemap\";b:1;s:24:\"enable_text_link_counter\";b:0;s:22:\"show_onboarding_notice\";b:1;s:18:\"first_activated_on\";i:1583355204;s:13:\"myyoast-oauth\";b:0;}','yes');
INSERT INTO `wp_options` VALUES (191,'wpseo_titles','a:94:{s:10:\"title_test\";i:0;s:17:\"forcerewritetitle\";b:0;s:9:\"separator\";s:7:\"sc-pipe\";s:16:\"title-home-wpseo\";s:42:\"%%sitename%% %%page%% %%sep%% %%sitedesc%%\";s:18:\"title-author-wpseo\";s:41:\"%%name%%, Author at %%sitename%% %%page%%\";s:19:\"title-archive-wpseo\";s:38:\"%%date%% %%page%% %%sep%% %%sitename%%\";s:18:\"title-search-wpseo\";s:63:\"You searched for %%searchphrase%% %%page%% %%sep%% %%sitename%%\";s:15:\"title-404-wpseo\";s:35:\"Page not found %%sep%% %%sitename%%\";s:19:\"metadesc-home-wpseo\";s:0:\"\";s:21:\"metadesc-author-wpseo\";s:0:\"\";s:22:\"metadesc-archive-wpseo\";s:0:\"\";s:9:\"rssbefore\";s:0:\"\";s:8:\"rssafter\";s:53:\"The post %%POSTLINK%% appeared first on %%BLOGLINK%%.\";s:20:\"noindex-author-wpseo\";b:0;s:28:\"noindex-author-noposts-wpseo\";b:1;s:21:\"noindex-archive-wpseo\";b:1;s:14:\"disable-author\";b:0;s:12:\"disable-date\";b:0;s:19:\"disable-post_format\";b:0;s:18:\"disable-attachment\";b:1;s:23:\"is-media-purge-relevant\";b:0;s:20:\"breadcrumbs-404crumb\";s:25:\"Error 404: Page not found\";s:29:\"breadcrumbs-display-blog-page\";b:0;s:20:\"breadcrumbs-boldlast\";b:0;s:25:\"breadcrumbs-archiveprefix\";s:12:\"Archives for\";s:18:\"breadcrumbs-enable\";b:0;s:16:\"breadcrumbs-home\";s:4:\"Home\";s:18:\"breadcrumbs-prefix\";s:0:\"\";s:24:\"breadcrumbs-searchprefix\";s:16:\"You searched for\";s:15:\"breadcrumbs-sep\";s:2:\"»\";s:12:\"website_name\";s:0:\"\";s:11:\"person_name\";s:0:\"\";s:11:\"person_logo\";s:0:\"\";s:14:\"person_logo_id\";i:0;s:22:\"alternate_website_name\";s:0:\"\";s:12:\"company_logo\";s:0:\"\";s:15:\"company_logo_id\";i:0;s:12:\"company_name\";s:0:\"\";s:17:\"company_or_person\";s:7:\"company\";s:25:\"company_or_person_user_id\";b:0;s:17:\"stripcategorybase\";b:0;s:10:\"title-post\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-post\";s:0:\"\";s:12:\"noindex-post\";b:0;s:13:\"showdate-post\";b:0;s:23:\"display-metabox-pt-post\";b:1;s:23:\"post_types-post-maintax\";i:0;s:10:\"title-page\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:13:\"metadesc-page\";s:0:\"\";s:12:\"noindex-page\";b:0;s:13:\"showdate-page\";b:0;s:23:\"display-metabox-pt-page\";b:0;s:23:\"post_types-page-maintax\";s:1:\"0\";s:16:\"title-attachment\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:19:\"metadesc-attachment\";s:0:\"\";s:18:\"noindex-attachment\";b:0;s:19:\"showdate-attachment\";b:0;s:29:\"display-metabox-pt-attachment\";b:1;s:29:\"post_types-attachment-maintax\";s:1:\"0\";s:18:\"title-tax-category\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-category\";s:0:\"\";s:28:\"display-metabox-tax-category\";b:0;s:20:\"noindex-tax-category\";b:0;s:18:\"title-tax-post_tag\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:21:\"metadesc-tax-post_tag\";s:0:\"\";s:28:\"display-metabox-tax-post_tag\";b:1;s:20:\"noindex-tax-post_tag\";b:0;s:21:\"title-tax-post_format\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:24:\"metadesc-tax-post_format\";s:0:\"\";s:31:\"display-metabox-tax-post_format\";b:0;s:23:\"noindex-tax-post_format\";b:1;s:12:\"title-issues\";s:39:\"%%title%% %%page%% %%sep%% %%sitename%%\";s:15:\"metadesc-issues\";s:0:\"\";s:14:\"noindex-issues\";b:0;s:15:\"showdate-issues\";b:0;s:25:\"display-metabox-pt-issues\";b:0;s:25:\"post_types-issues-maintax\";i:0;s:22:\"title-ptarchive-issues\";s:51:\"%%pt_plural%% Archive %%page%% %%sep%% %%sitename%%\";s:25:\"metadesc-ptarchive-issues\";s:0:\"\";s:24:\"bctitle-ptarchive-issues\";s:0:\"\";s:24:\"noindex-ptarchive-issues\";b:0;s:24:\"title-tax-monthly-issues\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:27:\"metadesc-tax-monthly-issues\";s:0:\"\";s:34:\"display-metabox-tax-monthly-issues\";b:0;s:26:\"noindex-tax-monthly-issues\";b:0;s:32:\"taxonomy-monthly-issues-ptparent\";i:0;s:33:\"title-tax-monthly-issues-category\";s:53:\"%%term_title%% Archives %%page%% %%sep%% %%sitename%%\";s:36:\"metadesc-tax-monthly-issues-category\";s:0:\"\";s:43:\"display-metabox-tax-monthly-issues-category\";b:0;s:35:\"noindex-tax-monthly-issues-category\";b:0;s:41:\"taxonomy-monthly-issues-category-ptparent\";i:0;s:26:\"taxonomy-category-ptparent\";s:1:\"0\";s:26:\"taxonomy-post_tag-ptparent\";s:1:\"0\";s:29:\"taxonomy-post_format-ptparent\";s:1:\"0\";}','yes');
INSERT INTO `wp_options` VALUES (192,'wpseo_social','a:19:{s:13:\"facebook_site\";s:0:\"\";s:13:\"instagram_url\";s:0:\"\";s:12:\"linkedin_url\";s:0:\"\";s:11:\"myspace_url\";s:0:\"\";s:16:\"og_default_image\";s:0:\"\";s:19:\"og_default_image_id\";s:0:\"\";s:18:\"og_frontpage_title\";s:0:\"\";s:17:\"og_frontpage_desc\";s:0:\"\";s:18:\"og_frontpage_image\";s:0:\"\";s:21:\"og_frontpage_image_id\";s:0:\"\";s:9:\"opengraph\";b:1;s:13:\"pinterest_url\";s:0:\"\";s:15:\"pinterestverify\";s:0:\"\";s:7:\"twitter\";b:1;s:12:\"twitter_site\";s:0:\"\";s:17:\"twitter_card_type\";s:19:\"summary_large_image\";s:11:\"youtube_url\";s:0:\"\";s:13:\"wikipedia_url\";s:0:\"\";s:10:\"fbadminapp\";s:0:\"\";}','yes');
INSERT INTO `wp_options` VALUES (193,'wpseo_flush_rewrite','1','yes');
INSERT INTO `wp_options` VALUES (200,'acf_pro_license','YToyOntzOjM6ImtleSI7czo3MjoiYjNKa1pYSmZhV1E5TXpJNU1EVjhkSGx3WlQxa1pYWmxiRzl3WlhKOFpHRjBaVDB5TURFMExUQTNMVEEzSURFMU9qTTNPakl5IjtzOjM6InVybCI7czoyMjoiaHR0cDovL2xvY2FsaG9zdDoxMDAxNiI7fQ==','yes');
INSERT INTO `wp_options` VALUES (211,'_transient_timeout_wpseo_link_table_inaccessible','1615067914','no');
INSERT INTO `wp_options` VALUES (212,'_transient_wpseo_link_table_inaccessible','0','no');
INSERT INTO `wp_options` VALUES (213,'_transient_timeout_wpseo_meta_table_inaccessible','1615067914','no');
INSERT INTO `wp_options` VALUES (214,'_transient_wpseo_meta_table_inaccessible','0','no');
INSERT INTO `wp_options` VALUES (224,'current_theme','ASU Global Sport Matters','yes');
INSERT INTO `wp_options` VALUES (225,'theme_mods_twentynineteen','a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:6:\"menu-1\";i:2;}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1583798488;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}','yes');
INSERT INTO `wp_options` VALUES (226,'theme_switched','','yes');
INSERT INTO `wp_options` VALUES (227,'recovery_keys','a:1:{s:22:\"INryOnus0kwoKVNy2HQ2lQ\";a:2:{s:10:\"hashed_key\";s:34:\"$P$BXGmdlv2a16RbLyML6HQ6E1wO0vsDb.\";s:10:\"created_at\";i:1585708785;}}','yes');
INSERT INTO `wp_options` VALUES (235,'theme_mods_global-sport','a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:3:{s:11:\"menu-header\";i:2;s:21:\"menu-header-offcanvas\";i:0;s:11:\"menu-footer\";i:0;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1583798485;s:4:\"data\";a:1:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}','yes');
INSERT INTO `wp_options` VALUES (253,'nav_menu_options','a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}','yes');
INSERT INTO `wp_options` VALUES (322,'wpseo_taxonomy_meta','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (1122,'issues-article-categories_children','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (1133,'recovery_mode_email_last_sent','1585708785','yes');
INSERT INTO `wp_options` VALUES (1160,'issues-categories_children','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (1277,'monthly-issues_children','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (1361,'category_children','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (1437,'monthly-issues-category_children','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (1472,'_flush','a:2:{s:7:\"version\";i:1585267587;s:4:\"time\";i:1585267654;}','yes');
INSERT INTO `wp_options` VALUES (1507,'_site_transient_timeout_browser_5a5ed620fb133b36e4eb8231e899bdb4','1585929491','no');
INSERT INTO `wp_options` VALUES (1508,'_site_transient_browser_5a5ed620fb133b36e4eb8231e899bdb4','a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"80.0.3987.149\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}','no');
INSERT INTO `wp_options` VALUES (1644,'_transient_timeout_acf_plugin_updates','1585890088','no');
INSERT INTO `wp_options` VALUES (1645,'_transient_acf_plugin_updates','a:4:{s:7:\"plugins\";a:0:{}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.8.9\";}}','no');
INSERT INTO `wp_options` VALUES (1651,'_site_transient_timeout_browser_c967d2204607f4c8ee0ec615b038ee69','1586282239','no');
INSERT INTO `wp_options` VALUES (1652,'_site_transient_browser_c967d2204607f4c8ee0ec615b038ee69','a:10:{s:4:\"name\";s:6:\"Safari\";s:7:\"version\";s:4:\"13.1\";s:8:\"platform\";s:9:\"Macintosh\";s:10:\"update_url\";s:29:\"https://www.apple.com/safari/\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/safari.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/safari.png?1\";s:15:\"current_version\";s:2:\"11\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}','no');
INSERT INTO `wp_options` VALUES (1653,'_site_transient_timeout_php_check_472f71d2a071d463a95f84346288dc89','1586282239','no');
INSERT INTO `wp_options` VALUES (1654,'_site_transient_php_check_472f71d2a071d463a95f84346288dc89','a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}','no');
INSERT INTO `wp_options` VALUES (1676,'_transient_timeout_plugin_slugs','1585854601','no');
INSERT INTO `wp_options` VALUES (1677,'_transient_plugin_slugs','a:5:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:19:\"akismet/akismet.php\";i:2;s:33:\"classic-editor/classic-editor.php\";i:3;s:29:\"gravityforms/gravityforms.php\";i:4;s:24:\"wordpress-seo/wp-seo.php\";}','no');
INSERT INTO `wp_options` VALUES (1696,'_site_transient_update_plugins','O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1585842290;s:7:\"checked\";a:5:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.8.9\";s:19:\"akismet/akismet.php\";s:5:\"4.1.4\";s:33:\"classic-editor/classic-editor.php\";s:3:\"1.5\";s:29:\"gravityforms/gravityforms.php\";s:8:\"2.4.17.9\";s:24:\"wordpress-seo/wp-seo.php\";s:6:\"13.4.1\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:3:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.4\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"classic-editor/classic-editor.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/classic-editor\";s:4:\"slug\";s:14:\"classic-editor\";s:6:\"plugin\";s:33:\"classic-editor/classic-editor.php\";s:11:\"new_version\";s:3:\"1.5\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/classic-editor/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/classic-editor.1.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-256x256.png?rev=1998671\";s:2:\"1x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-128x128.png?rev=1998671\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/classic-editor/assets/banner-1544x500.png?rev=1998671\";s:2:\"1x\";s:69:\"https://ps.w.org/classic-editor/assets/banner-772x250.png?rev=1998676\";}s:11:\"banners_rtl\";a:0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:6:\"13.4.1\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/wordpress-seo.13.4.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=1834347\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}}}}','no');
INSERT INTO `wp_options` VALUES (1699,'rewrite_rules','a:153:{s:19:\"sitemap_index\\.xml$\";s:19:\"index.php?sitemap=1\";s:31:\"([^/]+?)-sitemap([0-9]+)?\\.xml$\";s:51:\"index.php?sitemap=$matches[1]&sitemap_n=$matches[2]\";s:24:\"([a-z]+)?-?sitemap\\.xsl$\";s:39:\"index.php?yoast-sitemap-xsl=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:7:\"team/?$\";s:24:\"index.php?post_type=team\";s:37:\"team/feed/(feed|rdf|rss|rss2|atom)/?$\";s:41:\"index.php?post_type=team&feed=$matches[1]\";s:32:\"team/(feed|rdf|rss|rss2|atom)/?$\";s:41:\"index.php?post_type=team&feed=$matches[1]\";s:24:\"team/page/([0-9]{1,})/?$\";s:42:\"index.php?post_type=team&paged=$matches[1]\";s:9:\"issues/?$\";s:26:\"index.php?post_type=issues\";s:39:\"issues/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=issues&feed=$matches[1]\";s:34:\"issues/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=issues&feed=$matches[1]\";s:26:\"issues/page/([0-9]{1,})/?$\";s:44:\"index.php?post_type=issues&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?issue_tags=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?issue_tags=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:43:\"index.php?issue_tags=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:50:\"index.php?issue_tags=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:32:\"index.php?issue_tags=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:32:\"team/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:42:\"team/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:62:\"team/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"team/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"team/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:38:\"team/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:21:\"team/([^/]+)/embed/?$\";s:37:\"index.php?team=$matches[1]&embed=true\";s:25:\"team/([^/]+)/trackback/?$\";s:31:\"index.php?team=$matches[1]&tb=1\";s:45:\"team/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?team=$matches[1]&feed=$matches[2]\";s:40:\"team/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?team=$matches[1]&feed=$matches[2]\";s:33:\"team/([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?team=$matches[1]&paged=$matches[2]\";s:40:\"team/([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?team=$matches[1]&cpage=$matches[2]\";s:29:\"team/([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?team=$matches[1]&page=$matches[2]\";s:21:\"team/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:31:\"team/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:51:\"team/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:46:\"team/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:46:\"team/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:27:\"team/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:40:\"issues/[^/]+/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:50:\"issues/[^/]+/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:70:\"issues/[^/]+/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"issues/[^/]+/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"issues/[^/]+/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:46:\"issues/[^/]+/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:31:\"issues/([^/]+)/([^/]+)/embed/?$\";s:66:\"index.php?monthly-issues=$matches[1]&issues=$matches[2]&embed=true\";s:35:\"issues/([^/]+)/([^/]+)/trackback/?$\";s:60:\"index.php?monthly-issues=$matches[1]&issues=$matches[2]&tb=1\";s:55:\"issues/([^/]+)/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:72:\"index.php?monthly-issues=$matches[1]&issues=$matches[2]&feed=$matches[3]\";s:50:\"issues/([^/]+)/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:72:\"index.php?monthly-issues=$matches[1]&issues=$matches[2]&feed=$matches[3]\";s:43:\"issues/([^/]+)/([^/]+)/page/?([0-9]{1,})/?$\";s:73:\"index.php?monthly-issues=$matches[1]&issues=$matches[2]&paged=$matches[3]\";s:50:\"issues/([^/]+)/([^/]+)/comment-page-([0-9]{1,})/?$\";s:73:\"index.php?monthly-issues=$matches[1]&issues=$matches[2]&cpage=$matches[3]\";s:39:\"issues/([^/]+)/([^/]+)(?:/([0-9]+))?/?$\";s:72:\"index.php?monthly-issues=$matches[1]&issues=$matches[2]&page=$matches[3]\";s:29:\"issues/[^/]+/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:39:\"issues/[^/]+/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:59:\"issues/[^/]+/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:54:\"issues/[^/]+/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:54:\"issues/[^/]+/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:35:\"issues/[^/]+/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:47:\"issues/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?monthly-issues=$matches[1]&feed=$matches[2]\";s:42:\"issues/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?monthly-issues=$matches[1]&feed=$matches[2]\";s:23:\"issues/([^/]+)/embed/?$\";s:47:\"index.php?monthly-issues=$matches[1]&embed=true\";s:35:\"issues/([^/]+)/page/?([0-9]{1,})/?$\";s:54:\"index.php?monthly-issues=$matches[1]&paged=$matches[2]\";s:42:\"issues/([^/]+)/comment-page-([0-9]{1,})/?$\";s:54:\"index.php?monthly-issues=$matches[1]&cpage=$matches[2]\";s:17:\"issues/([^/]+)/?$\";s:36:\"index.php?monthly-issues=$matches[1]\";s:56:\"issues/([^/]+)//([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:89:\"index.php?monthly-issues=$matches[1]&monthly-issues-category=$matches[2]&feed=$matches[3]\";s:51:\"issues/([^/]+)//([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:89:\"index.php?monthly-issues=$matches[1]&monthly-issues-category=$matches[2]&feed=$matches[3]\";s:32:\"issues/([^/]+)//([^/]+)/embed/?$\";s:83:\"index.php?monthly-issues=$matches[1]&monthly-issues-category=$matches[2]&embed=true\";s:44:\"issues/([^/]+)//([^/]+)/page/?([0-9]{1,})/?$\";s:90:\"index.php?monthly-issues=$matches[1]&monthly-issues-category=$matches[2]&paged=$matches[3]\";s:26:\"issues/([^/]+)//([^/]+)/?$\";s:72:\"index.php?monthly-issues=$matches[1]&monthly-issues-category=$matches[2]\";s:48:\"issues/([^/]+)//feed/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?monthly-issues=$matches[1]&feed=$matches[2]\";s:43:\"issues/([^/]+)//(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?monthly-issues=$matches[1]&feed=$matches[2]\";s:24:\"issues/([^/]+)//embed/?$\";s:47:\"index.php?monthly-issues=$matches[1]&embed=true\";s:36:\"issues/([^/]+)//page/?([0-9]{1,})/?$\";s:54:\"index.php?monthly-issues=$matches[1]&paged=$matches[2]\";s:18:\"issues/([^/]+)//?$\";s:36:\"index.php?monthly-issues=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=2&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}','yes');
INSERT INTO `wp_options` VALUES (1720,'_site_transient_timeout_theme_roots','1585844090','no');
INSERT INTO `wp_options` VALUES (1721,'_site_transient_theme_roots','a:5:{s:12:\"global-sport\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";}','no');
INSERT INTO `wp_options` VALUES (1722,'_site_transient_update_themes','O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1585842290;s:7:\"checked\";a:5:{s:12:\"global-sport\";s:5:\"1.0.0\";s:14:\"twentynineteen\";s:3:\"1.4\";s:15:\"twentyseventeen\";s:3:\"2.2\";s:13:\"twentysixteen\";s:3:\"2.0\";s:12:\"twentytwenty\";s:3:\"1.1\";}s:8:\"response\";a:4:{s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"1.5\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.1.5.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:15:\"twentyseventeen\";a:6:{s:5:\"theme\";s:15:\"twentyseventeen\";s:11:\"new_version\";s:3:\"2.3\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentyseventeen/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentyseventeen.2.3.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:13:\"twentysixteen\";a:6:{s:5:\"theme\";s:13:\"twentysixteen\";s:11:\"new_version\";s:3:\"2.1\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentysixteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentysixteen.2.1.zip\";s:8:\"requires\";s:3:\"4.4\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:12:\"twentytwenty\";a:6:{s:5:\"theme\";s:12:\"twentytwenty\";s:11:\"new_version\";s:3:\"1.2\";s:3:\"url\";s:42:\"https://wordpress.org/themes/twentytwenty/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/twentytwenty.1.2.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:12:\"translations\";a:0:{}}','no');
/*!40000 ALTER TABLE `wp_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_postmeta`
--

DROP TABLE IF EXISTS `wp_postmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=892 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_postmeta`
--

LOCK TABLES `wp_postmeta` WRITE;
/*!40000 ALTER TABLE `wp_postmeta` DISABLE KEYS */;
INSERT INTO `wp_postmeta` VALUES (1,2,'_wp_page_template','default');
INSERT INTO `wp_postmeta` VALUES (7,2,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (8,2,'_edit_lock','1584730727:2');
INSERT INTO `wp_postmeta` VALUES (27,9,'_menu_item_type','custom');
INSERT INTO `wp_postmeta` VALUES (28,9,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (29,9,'_menu_item_object_id','9');
INSERT INTO `wp_postmeta` VALUES (30,9,'_menu_item_object','custom');
INSERT INTO `wp_postmeta` VALUES (31,9,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (32,9,'_menu_item_classes','a:1:{i:0;s:8:\"sections\";}');
INSERT INTO `wp_postmeta` VALUES (33,9,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (34,9,'_menu_item_url','#');
INSERT INTO `wp_postmeta` VALUES (36,10,'_menu_item_type','custom');
INSERT INTO `wp_postmeta` VALUES (37,10,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (38,10,'_menu_item_object_id','10');
INSERT INTO `wp_postmeta` VALUES (39,10,'_menu_item_object','custom');
INSERT INTO `wp_postmeta` VALUES (40,10,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (41,10,'_menu_item_classes','a:1:{i:0;s:6:\"issues\";}');
INSERT INTO `wp_postmeta` VALUES (42,10,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (43,10,'_menu_item_url','#');
INSERT INTO `wp_postmeta` VALUES (45,12,'_edit_lock','1585115669:2');
INSERT INTO `wp_postmeta` VALUES (46,12,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (47,15,'_edit_lock','1585115661:2');
INSERT INTO `wp_postmeta` VALUES (48,15,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (49,16,'_edit_lock','1585193465:2');
INSERT INTO `wp_postmeta` VALUES (50,16,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (51,17,'_edit_lock','1585351166:2');
INSERT INTO `wp_postmeta` VALUES (52,17,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (55,19,'_wp_attached_file','2020/03/hero.jpg');
INSERT INTO `wp_postmeta` VALUES (56,19,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1600;s:6:\"height\";i:800;s:4:\"file\";s:16:\"2020/03/hero.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"hero-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:17:\"hero-1024x512.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"hero-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"hero-768x384.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:384;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:17:\"hero-1536x768.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (61,1,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (63,1,'_wp_old_slug','hello-world');
INSERT INTO `wp_postmeta` VALUES (64,1,'_edit_lock','1583955119:2');
INSERT INTO `wp_postmeta` VALUES (65,22,'_edit_lock','1583964767:2');
INSERT INTO `wp_postmeta` VALUES (66,22,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (68,22,'_yoast_wpseo_primary_category','3');
INSERT INTO `wp_postmeta` VALUES (69,24,'_edit_lock','1583960201:2');
INSERT INTO `wp_postmeta` VALUES (70,24,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (72,24,'_yoast_wpseo_primary_category','3');
INSERT INTO `wp_postmeta` VALUES (73,26,'_edit_lock','1585179864:2');
INSERT INTO `wp_postmeta` VALUES (74,26,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (76,26,'_yoast_wpseo_primary_category','3');
INSERT INTO `wp_postmeta` VALUES (81,31,'_wp_attached_file','2020/03/soccer.jpg');
INSERT INTO `wp_postmeta` VALUES (82,31,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:695;s:6:\"height\";i:600;s:4:\"file\";s:18:\"2020/03/soccer.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"soccer-300x259.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:259;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"soccer-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (83,26,'_thumbnail_id','31');
INSERT INTO `wp_postmeta` VALUES (89,32,'_wp_attached_file','2020/03/football.jpg');
INSERT INTO `wp_postmeta` VALUES (90,32,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:333;s:6:\"height\";i:300;s:4:\"file\";s:20:\"2020/03/football.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"football-300x270.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:270;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"football-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (91,24,'_thumbnail_id','32');
INSERT INTO `wp_postmeta` VALUES (93,33,'_wp_attached_file','2020/03/runner.jpg');
INSERT INTO `wp_postmeta` VALUES (94,33,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:333;s:6:\"height\";i:300;s:4:\"file\";s:18:\"2020/03/runner.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"runner-300x270.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:270;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"runner-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (95,22,'_thumbnail_id','33');
INSERT INTO `wp_postmeta` VALUES (97,35,'_wp_attached_file','2020/03/video-placeholder.jpg');
INSERT INTO `wp_postmeta` VALUES (98,35,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:815;s:6:\"height\";i:534;s:4:\"file\";s:29:\"2020/03/video-placeholder.jpg\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"video-placeholder-300x197.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:197;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"video-placeholder-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"video-placeholder-768x503.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:503;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (99,36,'_edit_lock','1583972288:2');
INSERT INTO `wp_postmeta` VALUES (100,36,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (101,37,'_wp_attached_file','2020/03/young-female-atheletes.jpg');
INSERT INTO `wp_postmeta` VALUES (102,37,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:454;s:6:\"height\";i:405;s:4:\"file\";s:34:\"2020/03/young-female-atheletes.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"young-female-atheletes-300x268.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:268;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"young-female-atheletes-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (103,36,'_thumbnail_id','37');
INSERT INTO `wp_postmeta` VALUES (105,36,'_yoast_wpseo_primary_category','4');
INSERT INTO `wp_postmeta` VALUES (106,39,'_wp_attached_file','2020/03/jack-rutter.jpg');
INSERT INTO `wp_postmeta` VALUES (107,39,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:454;s:6:\"height\";i:405;s:4:\"file\";s:23:\"2020/03/jack-rutter.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"jack-rutter-300x268.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:268;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"jack-rutter-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (108,40,'_wp_attached_file','2020/03/montanos.jpg');
INSERT INTO `wp_postmeta` VALUES (109,40,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:454;s:6:\"height\";i:405;s:4:\"file\";s:20:\"2020/03/montanos.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"montanos-300x268.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:268;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"montanos-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (110,41,'_edit_lock','1583972314:2');
INSERT INTO `wp_postmeta` VALUES (111,41,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (112,41,'_thumbnail_id','40');
INSERT INTO `wp_postmeta` VALUES (114,41,'_yoast_wpseo_primary_category','4');
INSERT INTO `wp_postmeta` VALUES (115,43,'_edit_lock','1584028724:2');
INSERT INTO `wp_postmeta` VALUES (116,43,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (117,43,'_thumbnail_id','39');
INSERT INTO `wp_postmeta` VALUES (119,43,'_yoast_wpseo_primary_category','4');
INSERT INTO `wp_postmeta` VALUES (120,45,'_edit_lock','1584028824:2');
INSERT INTO `wp_postmeta` VALUES (121,45,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (123,45,'_yoast_wpseo_primary_category','5');
INSERT INTO `wp_postmeta` VALUES (124,45,'_thumbnail_id','19');
INSERT INTO `wp_postmeta` VALUES (126,47,'_edit_lock','1584403015:2');
INSERT INTO `wp_postmeta` VALUES (127,47,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (129,47,'_yoast_wpseo_primary_category','6');
INSERT INTO `wp_postmeta` VALUES (131,50,'_edit_lock','1584029851:2');
INSERT INTO `wp_postmeta` VALUES (132,50,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (134,50,'_yoast_wpseo_primary_category','7');
INSERT INTO `wp_postmeta` VALUES (191,59,'_edit_lock','1585079227:2');
INSERT INTO `wp_postmeta` VALUES (192,59,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (194,59,'_yoast_wpseo_primary_category','10');
INSERT INTO `wp_postmeta` VALUES (195,61,'_wp_attached_file','2020/03/yoga.png');
INSERT INTO `wp_postmeta` VALUES (196,61,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:816;s:6:\"height\";i:607;s:4:\"file\";s:16:\"2020/03/yoga.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"yoga-300x223.png\";s:5:\"width\";i:300;s:6:\"height\";i:223;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"yoga-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"yoga-768x571.png\";s:5:\"width\";i:768;s:6:\"height\";i:571;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (197,59,'_thumbnail_id','61');
INSERT INTO `wp_postmeta` VALUES (199,62,'_wp_attached_file','2020/03/physique.png');
INSERT INTO `wp_postmeta` VALUES (200,62,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:816;s:6:\"height\";i:600;s:4:\"file\";s:20:\"2020/03/physique.png\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"physique-300x221.png\";s:5:\"width\";i:300;s:6:\"height\";i:221;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"physique-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"physique-768x565.png\";s:5:\"width\";i:768;s:6:\"height\";i:565;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (201,47,'_thumbnail_id','62');
INSERT INTO `wp_postmeta` VALUES (203,64,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (204,64,'_edit_lock','1585716937:2');
INSERT INTO `wp_postmeta` VALUES (205,66,'_menu_item_type','post_type');
INSERT INTO `wp_postmeta` VALUES (206,66,'_menu_item_menu_item_parent','0');
INSERT INTO `wp_postmeta` VALUES (207,66,'_menu_item_object_id','64');
INSERT INTO `wp_postmeta` VALUES (208,66,'_menu_item_object','page');
INSERT INTO `wp_postmeta` VALUES (209,66,'_menu_item_target','');
INSERT INTO `wp_postmeta` VALUES (210,66,'_menu_item_classes','a:1:{i:0;s:5:\"about\";}');
INSERT INTO `wp_postmeta` VALUES (211,66,'_menu_item_xfn','');
INSERT INTO `wp_postmeta` VALUES (212,66,'_menu_item_url','');
INSERT INTO `wp_postmeta` VALUES (216,70,'_edit_lock','1584922179:2');
INSERT INTO `wp_postmeta` VALUES (217,70,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (218,2,'current_issue_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (219,2,'_current_issue_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (220,2,'current_issue_background_images','1');
INSERT INTO `wp_postmeta` VALUES (221,2,'_current_issue_background_images','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (222,2,'sections','a:1:{i:0;s:13:\"current_issue\";}');
INSERT INTO `wp_postmeta` VALUES (223,2,'_sections','field_5e73f1c931b4a');
INSERT INTO `wp_postmeta` VALUES (224,73,'current_issue_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (225,73,'_current_issue_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (226,73,'current_issue_background_images','1');
INSERT INTO `wp_postmeta` VALUES (227,73,'_current_issue_background_images','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (228,73,'sections','');
INSERT INTO `wp_postmeta` VALUES (229,73,'_sections','field_5e73f1c931b4a');
INSERT INTO `wp_postmeta` VALUES (230,75,'_wp_attached_file','2020/03/featured-issue-bg.jpg');
INSERT INTO `wp_postmeta` VALUES (231,75,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1600;s:6:\"height\";i:1000;s:4:\"file\";s:29:\"2020/03/featured-issue-bg.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"featured-issue-bg-300x188.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:188;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:30:\"featured-issue-bg-1024x640.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:640;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"featured-issue-bg-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"featured-issue-bg-768x480.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:480;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:30:\"featured-issue-bg-1536x960.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:960;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (232,2,'sections_0_current_issue_0_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (233,2,'_sections_0_current_issue_0_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (234,2,'sections_0_current_issue_0_background_images','');
INSERT INTO `wp_postmeta` VALUES (235,2,'_sections_0_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (236,2,'sections_0_current_issue','1');
INSERT INTO `wp_postmeta` VALUES (237,2,'_sections_0_current_issue','field_5e73f1ed31b4b_field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (238,76,'current_issue_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (239,76,'_current_issue_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (240,76,'current_issue_background_images','1');
INSERT INTO `wp_postmeta` VALUES (241,76,'_current_issue_background_images','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (242,76,'sections','a:1:{i:0;s:13:\"current_issue\";}');
INSERT INTO `wp_postmeta` VALUES (243,76,'_sections','field_5e73f1c931b4a');
INSERT INTO `wp_postmeta` VALUES (244,76,'sections_0_current_issue_0_background_images_0_image','75');
INSERT INTO `wp_postmeta` VALUES (245,76,'_sections_0_current_issue_0_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (246,76,'sections_0_current_issue_0_background_images','1');
INSERT INTO `wp_postmeta` VALUES (247,76,'_sections_0_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (248,76,'sections_0_current_issue','1');
INSERT INTO `wp_postmeta` VALUES (249,76,'_sections_0_current_issue','field_5e73f1ed31b4b_field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (250,77,'_wp_attached_file','2020/03/featured-issue-bg2.jpg');
INSERT INTO `wp_postmeta` VALUES (251,77,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1600;s:6:\"height\";i:1000;s:4:\"file\";s:30:\"2020/03/featured-issue-bg2.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"featured-issue-bg2-300x188.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:188;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:31:\"featured-issue-bg2-1024x640.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:640;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"featured-issue-bg2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"featured-issue-bg2-768x480.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:480;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:31:\"featured-issue-bg2-1536x960.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:960;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (252,78,'_wp_attached_file','2020/03/featured-issue-bg3.jpg');
INSERT INTO `wp_postmeta` VALUES (253,78,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1600;s:6:\"height\";i:1000;s:4:\"file\";s:30:\"2020/03/featured-issue-bg3.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"featured-issue-bg3-300x188.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:188;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:31:\"featured-issue-bg3-1024x640.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:640;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"featured-issue-bg3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"featured-issue-bg3-768x480.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:480;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:31:\"featured-issue-bg3-1536x960.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:960;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (254,2,'sections_0_current_issue_0_background_images_1_image','');
INSERT INTO `wp_postmeta` VALUES (255,2,'_sections_0_current_issue_0_background_images_1_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (256,2,'sections_0_current_issue_0_background_images_2_image','');
INSERT INTO `wp_postmeta` VALUES (257,2,'_sections_0_current_issue_0_background_images_2_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (258,79,'current_issue_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (259,79,'_current_issue_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (260,79,'current_issue_background_images','1');
INSERT INTO `wp_postmeta` VALUES (261,79,'_current_issue_background_images','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (262,79,'sections','a:1:{i:0;s:13:\"current_issue\";}');
INSERT INTO `wp_postmeta` VALUES (263,79,'_sections','field_5e73f1c931b4a');
INSERT INTO `wp_postmeta` VALUES (264,79,'sections_0_current_issue_0_background_images_0_image','75');
INSERT INTO `wp_postmeta` VALUES (265,79,'_sections_0_current_issue_0_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (266,79,'sections_0_current_issue_0_background_images','3');
INSERT INTO `wp_postmeta` VALUES (267,79,'_sections_0_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (268,79,'sections_0_current_issue','1');
INSERT INTO `wp_postmeta` VALUES (269,79,'_sections_0_current_issue','field_5e73f1ed31b4b_field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (270,79,'sections_0_current_issue_0_background_images_1_image','77');
INSERT INTO `wp_postmeta` VALUES (271,79,'_sections_0_current_issue_0_background_images_1_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (272,79,'sections_0_current_issue_0_background_images_2_image','78');
INSERT INTO `wp_postmeta` VALUES (273,79,'_sections_0_current_issue_0_background_images_2_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (274,80,'sections_0_current_issue_0_background_images_0_image','75');
INSERT INTO `wp_postmeta` VALUES (275,80,'_sections_0_current_issue_0_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (276,80,'sections_0_current_issue_0_background_images_1_image','77');
INSERT INTO `wp_postmeta` VALUES (277,80,'_sections_0_current_issue_0_background_images_1_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (278,80,'sections_0_current_issue_0_background_images_2_image','78');
INSERT INTO `wp_postmeta` VALUES (279,80,'_sections_0_current_issue_0_background_images_2_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (280,80,'sections_0_current_issue_0_background_images','3');
INSERT INTO `wp_postmeta` VALUES (281,80,'_sections_0_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (282,80,'sections_0_current_issue','1');
INSERT INTO `wp_postmeta` VALUES (283,80,'_sections_0_current_issue','field_5e73f1ed31b4b_field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (284,80,'sections','a:1:{i:0;s:13:\"current_issue\";}');
INSERT INTO `wp_postmeta` VALUES (285,80,'_sections','field_5e73f1c931b4a');
INSERT INTO `wp_postmeta` VALUES (286,81,'current_issue_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (287,81,'_current_issue_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (288,81,'current_issue_background_images','1');
INSERT INTO `wp_postmeta` VALUES (289,81,'_current_issue_background_images','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (290,81,'sections','a:1:{i:0;s:13:\"current_issue\";}');
INSERT INTO `wp_postmeta` VALUES (291,81,'_sections','field_5e73f1c931b4a');
INSERT INTO `wp_postmeta` VALUES (292,81,'sections_0_current_issue_0_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (293,81,'_sections_0_current_issue_0_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (294,81,'sections_0_current_issue_0_background_images','3');
INSERT INTO `wp_postmeta` VALUES (295,81,'_sections_0_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (296,81,'sections_0_current_issue','1');
INSERT INTO `wp_postmeta` VALUES (297,81,'_sections_0_current_issue','field_5e73f1ed31b4b_field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (298,81,'sections_0_current_issue_0_background_images_1_image','');
INSERT INTO `wp_postmeta` VALUES (299,81,'_sections_0_current_issue_0_background_images_1_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (300,81,'sections_0_current_issue_0_background_images_2_image','');
INSERT INTO `wp_postmeta` VALUES (301,81,'_sections_0_current_issue_0_background_images_2_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (302,83,'current_issue_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (303,83,'_current_issue_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (304,83,'current_issue_background_images','1');
INSERT INTO `wp_postmeta` VALUES (305,83,'_current_issue_background_images','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (306,83,'sections','a:1:{i:0;s:13:\"current_issue\";}');
INSERT INTO `wp_postmeta` VALUES (307,83,'_sections','field_5e73f1c931b4a');
INSERT INTO `wp_postmeta` VALUES (308,83,'sections_0_current_issue_0_background_images_0_image','75');
INSERT INTO `wp_postmeta` VALUES (309,83,'_sections_0_current_issue_0_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (310,83,'sections_0_current_issue_0_background_images','3');
INSERT INTO `wp_postmeta` VALUES (311,83,'_sections_0_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (312,83,'sections_0_current_issue','1');
INSERT INTO `wp_postmeta` VALUES (313,83,'_sections_0_current_issue','field_5e73f1ed31b4b_field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (314,83,'sections_0_current_issue_0_background_images_1_image','');
INSERT INTO `wp_postmeta` VALUES (315,83,'_sections_0_current_issue_0_background_images_1_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (316,83,'sections_0_current_issue_0_background_images_2_image','');
INSERT INTO `wp_postmeta` VALUES (317,83,'_sections_0_current_issue_0_background_images_2_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (318,84,'current_issue_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (319,84,'_current_issue_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (320,84,'current_issue_background_images','1');
INSERT INTO `wp_postmeta` VALUES (321,84,'_current_issue_background_images','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (322,84,'sections','a:1:{i:0;s:13:\"current_issue\";}');
INSERT INTO `wp_postmeta` VALUES (323,84,'_sections','field_5e73f1c931b4a');
INSERT INTO `wp_postmeta` VALUES (324,84,'sections_0_current_issue_0_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (325,84,'_sections_0_current_issue_0_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (326,84,'sections_0_current_issue_0_background_images','3');
INSERT INTO `wp_postmeta` VALUES (327,84,'_sections_0_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (328,84,'sections_0_current_issue','1');
INSERT INTO `wp_postmeta` VALUES (329,84,'_sections_0_current_issue','field_5e73f1ed31b4b_field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (330,84,'sections_0_current_issue_0_background_images_1_image','');
INSERT INTO `wp_postmeta` VALUES (331,84,'_sections_0_current_issue_0_background_images_1_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (332,84,'sections_0_current_issue_0_background_images_2_image','');
INSERT INTO `wp_postmeta` VALUES (333,84,'_sections_0_current_issue_0_background_images_2_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (334,2,'sections_0_current_issue_0_background_images_image_1','75');
INSERT INTO `wp_postmeta` VALUES (335,2,'_sections_0_current_issue_0_background_images_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (336,2,'sections_0_current_issue_0_background_images_image_2','77');
INSERT INTO `wp_postmeta` VALUES (337,2,'_sections_0_current_issue_0_background_images_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (338,2,'sections_0_current_issue_0_background_images_image_3','78');
INSERT INTO `wp_postmeta` VALUES (339,2,'_sections_0_current_issue_0_background_images_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (340,2,'sections_0_current_issue_0_background_images_image_4','75');
INSERT INTO `wp_postmeta` VALUES (341,2,'_sections_0_current_issue_0_background_images_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (342,2,'sections_0_current_issue_0_background_images_image_5','77');
INSERT INTO `wp_postmeta` VALUES (343,2,'_sections_0_current_issue_0_background_images_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (344,90,'current_issue_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (345,90,'_current_issue_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (346,90,'current_issue_background_images','1');
INSERT INTO `wp_postmeta` VALUES (347,90,'_current_issue_background_images','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (348,90,'sections','a:1:{i:0;s:13:\"current_issue\";}');
INSERT INTO `wp_postmeta` VALUES (349,90,'_sections','field_5e73f1c931b4a');
INSERT INTO `wp_postmeta` VALUES (350,90,'sections_0_current_issue_0_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (351,90,'_sections_0_current_issue_0_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (352,90,'sections_0_current_issue_0_background_images','');
INSERT INTO `wp_postmeta` VALUES (353,90,'_sections_0_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (354,90,'sections_0_current_issue','1');
INSERT INTO `wp_postmeta` VALUES (355,90,'_sections_0_current_issue','field_5e73f1ed31b4b_field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (356,90,'sections_0_current_issue_0_background_images_1_image','');
INSERT INTO `wp_postmeta` VALUES (357,90,'_sections_0_current_issue_0_background_images_1_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (358,90,'sections_0_current_issue_0_background_images_2_image','');
INSERT INTO `wp_postmeta` VALUES (359,90,'_sections_0_current_issue_0_background_images_2_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (360,90,'sections_0_current_issue_0_background_images_image_1','75');
INSERT INTO `wp_postmeta` VALUES (361,90,'_sections_0_current_issue_0_background_images_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (362,90,'sections_0_current_issue_0_background_images_image_2','77');
INSERT INTO `wp_postmeta` VALUES (363,90,'_sections_0_current_issue_0_background_images_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (364,90,'sections_0_current_issue_0_background_images_image_3','78');
INSERT INTO `wp_postmeta` VALUES (365,90,'_sections_0_current_issue_0_background_images_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (366,90,'sections_0_current_issue_0_background_images_image_4','75');
INSERT INTO `wp_postmeta` VALUES (367,90,'_sections_0_current_issue_0_background_images_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (368,90,'sections_0_current_issue_0_background_images_image_5','77');
INSERT INTO `wp_postmeta` VALUES (369,90,'_sections_0_current_issue_0_background_images_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (370,2,'current_issue_0_background_images_image_1','75');
INSERT INTO `wp_postmeta` VALUES (371,2,'_current_issue_0_background_images_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (372,2,'current_issue_0_background_images_image_2','77');
INSERT INTO `wp_postmeta` VALUES (373,2,'_current_issue_0_background_images_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (374,2,'current_issue_0_background_images_image_3','78');
INSERT INTO `wp_postmeta` VALUES (375,2,'_current_issue_0_background_images_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (376,2,'current_issue_0_background_images_image_4','75');
INSERT INTO `wp_postmeta` VALUES (377,2,'_current_issue_0_background_images_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (378,2,'current_issue_0_background_images_image_5','77');
INSERT INTO `wp_postmeta` VALUES (379,2,'_current_issue_0_background_images_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (380,2,'current_issue_0_background_images','');
INSERT INTO `wp_postmeta` VALUES (381,2,'_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (382,2,'current_issue','1');
INSERT INTO `wp_postmeta` VALUES (383,2,'_current_issue','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (384,91,'current_issue_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (385,91,'_current_issue_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (386,91,'current_issue_background_images','1');
INSERT INTO `wp_postmeta` VALUES (387,91,'_current_issue_background_images','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (388,91,'sections','a:1:{i:0;s:13:\"current_issue\";}');
INSERT INTO `wp_postmeta` VALUES (389,91,'_sections','field_5e73f1c931b4a');
INSERT INTO `wp_postmeta` VALUES (390,91,'sections_0_current_issue_0_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (391,91,'_sections_0_current_issue_0_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (392,91,'sections_0_current_issue_0_background_images','');
INSERT INTO `wp_postmeta` VALUES (393,91,'_sections_0_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (394,91,'sections_0_current_issue','1');
INSERT INTO `wp_postmeta` VALUES (395,91,'_sections_0_current_issue','field_5e73f1ed31b4b_field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (396,91,'sections_0_current_issue_0_background_images_1_image','');
INSERT INTO `wp_postmeta` VALUES (397,91,'_sections_0_current_issue_0_background_images_1_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (398,91,'sections_0_current_issue_0_background_images_2_image','');
INSERT INTO `wp_postmeta` VALUES (399,91,'_sections_0_current_issue_0_background_images_2_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (400,91,'sections_0_current_issue_0_background_images_image_1','75');
INSERT INTO `wp_postmeta` VALUES (401,91,'_sections_0_current_issue_0_background_images_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (402,91,'sections_0_current_issue_0_background_images_image_2','77');
INSERT INTO `wp_postmeta` VALUES (403,91,'_sections_0_current_issue_0_background_images_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (404,91,'sections_0_current_issue_0_background_images_image_3','78');
INSERT INTO `wp_postmeta` VALUES (405,91,'_sections_0_current_issue_0_background_images_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (406,91,'sections_0_current_issue_0_background_images_image_4','75');
INSERT INTO `wp_postmeta` VALUES (407,91,'_sections_0_current_issue_0_background_images_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (408,91,'sections_0_current_issue_0_background_images_image_5','77');
INSERT INTO `wp_postmeta` VALUES (409,91,'_sections_0_current_issue_0_background_images_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (410,91,'current_issue_0_background_images_image_1','75');
INSERT INTO `wp_postmeta` VALUES (411,91,'_current_issue_0_background_images_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (412,91,'current_issue_0_background_images_image_2','77');
INSERT INTO `wp_postmeta` VALUES (413,91,'_current_issue_0_background_images_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (414,91,'current_issue_0_background_images_image_3','78');
INSERT INTO `wp_postmeta` VALUES (415,91,'_current_issue_0_background_images_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (416,91,'current_issue_0_background_images_image_4','75');
INSERT INTO `wp_postmeta` VALUES (417,91,'_current_issue_0_background_images_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (418,91,'current_issue_0_background_images_image_5','77');
INSERT INTO `wp_postmeta` VALUES (419,91,'_current_issue_0_background_images_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (420,91,'current_issue_0_background_images','');
INSERT INTO `wp_postmeta` VALUES (421,91,'_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (422,91,'current_issue','1');
INSERT INTO `wp_postmeta` VALUES (423,91,'_current_issue','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (424,2,'sections_0_current_issue_0_image_1','');
INSERT INTO `wp_postmeta` VALUES (425,2,'_sections_0_current_issue_0_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (426,2,'sections_0_current_issue_0_image_2','');
INSERT INTO `wp_postmeta` VALUES (427,2,'_sections_0_current_issue_0_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (428,2,'sections_0_current_issue_0_image_3','');
INSERT INTO `wp_postmeta` VALUES (429,2,'_sections_0_current_issue_0_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (430,2,'sections_0_current_issue_0_image_4','');
INSERT INTO `wp_postmeta` VALUES (431,2,'_sections_0_current_issue_0_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (432,2,'sections_0_current_issue_0_image_5','');
INSERT INTO `wp_postmeta` VALUES (433,2,'_sections_0_current_issue_0_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (434,92,'current_issue_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (435,92,'_current_issue_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (436,92,'current_issue_background_images','1');
INSERT INTO `wp_postmeta` VALUES (437,92,'_current_issue_background_images','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (438,92,'sections','a:1:{i:0;s:13:\"current_issue\";}');
INSERT INTO `wp_postmeta` VALUES (439,92,'_sections','field_5e73f1c931b4a');
INSERT INTO `wp_postmeta` VALUES (440,92,'sections_0_current_issue_0_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (441,92,'_sections_0_current_issue_0_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (442,92,'sections_0_current_issue_0_background_images','');
INSERT INTO `wp_postmeta` VALUES (443,92,'_sections_0_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (444,92,'sections_0_current_issue','1');
INSERT INTO `wp_postmeta` VALUES (445,92,'_sections_0_current_issue','field_5e73f1ed31b4b_field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (446,92,'sections_0_current_issue_0_background_images_1_image','');
INSERT INTO `wp_postmeta` VALUES (447,92,'_sections_0_current_issue_0_background_images_1_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (448,92,'sections_0_current_issue_0_background_images_2_image','');
INSERT INTO `wp_postmeta` VALUES (449,92,'_sections_0_current_issue_0_background_images_2_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (450,92,'sections_0_current_issue_0_background_images_image_1','75');
INSERT INTO `wp_postmeta` VALUES (451,92,'_sections_0_current_issue_0_background_images_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (452,92,'sections_0_current_issue_0_background_images_image_2','77');
INSERT INTO `wp_postmeta` VALUES (453,92,'_sections_0_current_issue_0_background_images_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (454,92,'sections_0_current_issue_0_background_images_image_3','78');
INSERT INTO `wp_postmeta` VALUES (455,92,'_sections_0_current_issue_0_background_images_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (456,92,'sections_0_current_issue_0_background_images_image_4','75');
INSERT INTO `wp_postmeta` VALUES (457,92,'_sections_0_current_issue_0_background_images_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (458,92,'sections_0_current_issue_0_background_images_image_5','77');
INSERT INTO `wp_postmeta` VALUES (459,92,'_sections_0_current_issue_0_background_images_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (460,92,'current_issue_0_background_images_image_1','75');
INSERT INTO `wp_postmeta` VALUES (461,92,'_current_issue_0_background_images_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (462,92,'current_issue_0_background_images_image_2','77');
INSERT INTO `wp_postmeta` VALUES (463,92,'_current_issue_0_background_images_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (464,92,'current_issue_0_background_images_image_3','78');
INSERT INTO `wp_postmeta` VALUES (465,92,'_current_issue_0_background_images_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (466,92,'current_issue_0_background_images_image_4','75');
INSERT INTO `wp_postmeta` VALUES (467,92,'_current_issue_0_background_images_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (468,92,'current_issue_0_background_images_image_5','77');
INSERT INTO `wp_postmeta` VALUES (469,92,'_current_issue_0_background_images_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (470,92,'current_issue_0_background_images','');
INSERT INTO `wp_postmeta` VALUES (471,92,'_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (472,92,'current_issue','1');
INSERT INTO `wp_postmeta` VALUES (473,92,'_current_issue','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (474,92,'sections_0_current_issue_0_image_1','75');
INSERT INTO `wp_postmeta` VALUES (475,92,'_sections_0_current_issue_0_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (476,92,'sections_0_current_issue_0_image_2','77');
INSERT INTO `wp_postmeta` VALUES (477,92,'_sections_0_current_issue_0_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (478,92,'sections_0_current_issue_0_image_3','78');
INSERT INTO `wp_postmeta` VALUES (479,92,'_sections_0_current_issue_0_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (480,92,'sections_0_current_issue_0_image_4','75');
INSERT INTO `wp_postmeta` VALUES (481,92,'_sections_0_current_issue_0_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (482,92,'sections_0_current_issue_0_image_5','77');
INSERT INTO `wp_postmeta` VALUES (483,92,'_sections_0_current_issue_0_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (484,93,'current_issue_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (485,93,'_current_issue_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (486,93,'current_issue_background_images','1');
INSERT INTO `wp_postmeta` VALUES (487,93,'_current_issue_background_images','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (488,93,'sections','a:1:{i:0;s:13:\"current_issue\";}');
INSERT INTO `wp_postmeta` VALUES (489,93,'_sections','field_5e73f1c931b4a');
INSERT INTO `wp_postmeta` VALUES (490,93,'sections_0_current_issue_0_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (491,93,'_sections_0_current_issue_0_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (492,93,'sections_0_current_issue_0_background_images','');
INSERT INTO `wp_postmeta` VALUES (493,93,'_sections_0_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (494,93,'sections_0_current_issue','1');
INSERT INTO `wp_postmeta` VALUES (495,93,'_sections_0_current_issue','field_5e73f1ed31b4b_field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (496,93,'sections_0_current_issue_0_background_images_1_image','');
INSERT INTO `wp_postmeta` VALUES (497,93,'_sections_0_current_issue_0_background_images_1_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (498,93,'sections_0_current_issue_0_background_images_2_image','');
INSERT INTO `wp_postmeta` VALUES (499,93,'_sections_0_current_issue_0_background_images_2_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (500,93,'sections_0_current_issue_0_background_images_image_1','75');
INSERT INTO `wp_postmeta` VALUES (501,93,'_sections_0_current_issue_0_background_images_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (502,93,'sections_0_current_issue_0_background_images_image_2','77');
INSERT INTO `wp_postmeta` VALUES (503,93,'_sections_0_current_issue_0_background_images_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (504,93,'sections_0_current_issue_0_background_images_image_3','78');
INSERT INTO `wp_postmeta` VALUES (505,93,'_sections_0_current_issue_0_background_images_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (506,93,'sections_0_current_issue_0_background_images_image_4','75');
INSERT INTO `wp_postmeta` VALUES (507,93,'_sections_0_current_issue_0_background_images_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (508,93,'sections_0_current_issue_0_background_images_image_5','77');
INSERT INTO `wp_postmeta` VALUES (509,93,'_sections_0_current_issue_0_background_images_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (510,93,'current_issue_0_background_images_image_1','75');
INSERT INTO `wp_postmeta` VALUES (511,93,'_current_issue_0_background_images_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (512,93,'current_issue_0_background_images_image_2','77');
INSERT INTO `wp_postmeta` VALUES (513,93,'_current_issue_0_background_images_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (514,93,'current_issue_0_background_images_image_3','78');
INSERT INTO `wp_postmeta` VALUES (515,93,'_current_issue_0_background_images_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (516,93,'current_issue_0_background_images_image_4','75');
INSERT INTO `wp_postmeta` VALUES (517,93,'_current_issue_0_background_images_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (518,93,'current_issue_0_background_images_image_5','77');
INSERT INTO `wp_postmeta` VALUES (519,93,'_current_issue_0_background_images_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (520,93,'current_issue_0_background_images','');
INSERT INTO `wp_postmeta` VALUES (521,93,'_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (522,93,'current_issue','1');
INSERT INTO `wp_postmeta` VALUES (523,93,'_current_issue','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (524,93,'sections_0_current_issue_0_image_1','');
INSERT INTO `wp_postmeta` VALUES (525,93,'_sections_0_current_issue_0_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (526,93,'sections_0_current_issue_0_image_2','');
INSERT INTO `wp_postmeta` VALUES (527,93,'_sections_0_current_issue_0_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (528,93,'sections_0_current_issue_0_image_3','');
INSERT INTO `wp_postmeta` VALUES (529,93,'_sections_0_current_issue_0_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (530,93,'sections_0_current_issue_0_image_4','');
INSERT INTO `wp_postmeta` VALUES (531,93,'_sections_0_current_issue_0_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (532,93,'sections_0_current_issue_0_image_5','');
INSERT INTO `wp_postmeta` VALUES (533,93,'_sections_0_current_issue_0_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (540,2,'sections_0_background_images_image_1','75');
INSERT INTO `wp_postmeta` VALUES (541,2,'_sections_0_background_images_image_1','field_5e74143a7bef0');
INSERT INTO `wp_postmeta` VALUES (542,2,'sections_0_background_images_image_2','77');
INSERT INTO `wp_postmeta` VALUES (543,2,'_sections_0_background_images_image_2','field_5e74145c4fc01');
INSERT INTO `wp_postmeta` VALUES (544,2,'sections_0_background_images','');
INSERT INTO `wp_postmeta` VALUES (545,2,'_sections_0_background_images','field_5e73f1ed31b4b');
INSERT INTO `wp_postmeta` VALUES (546,99,'current_issue_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (547,99,'_current_issue_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (548,99,'current_issue_background_images','1');
INSERT INTO `wp_postmeta` VALUES (549,99,'_current_issue_background_images','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (550,99,'sections','a:1:{i:0;s:13:\"current_issue\";}');
INSERT INTO `wp_postmeta` VALUES (551,99,'_sections','field_5e73f1c931b4a');
INSERT INTO `wp_postmeta` VALUES (552,99,'sections_0_current_issue_0_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (553,99,'_sections_0_current_issue_0_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (554,99,'sections_0_current_issue_0_background_images','');
INSERT INTO `wp_postmeta` VALUES (555,99,'_sections_0_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (556,99,'sections_0_current_issue','1');
INSERT INTO `wp_postmeta` VALUES (557,99,'_sections_0_current_issue','field_5e73f1ed31b4b_field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (558,99,'sections_0_current_issue_0_background_images_1_image','');
INSERT INTO `wp_postmeta` VALUES (559,99,'_sections_0_current_issue_0_background_images_1_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (560,99,'sections_0_current_issue_0_background_images_2_image','');
INSERT INTO `wp_postmeta` VALUES (561,99,'_sections_0_current_issue_0_background_images_2_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (562,99,'sections_0_current_issue_0_background_images_image_1','75');
INSERT INTO `wp_postmeta` VALUES (563,99,'_sections_0_current_issue_0_background_images_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (564,99,'sections_0_current_issue_0_background_images_image_2','77');
INSERT INTO `wp_postmeta` VALUES (565,99,'_sections_0_current_issue_0_background_images_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (566,99,'sections_0_current_issue_0_background_images_image_3','78');
INSERT INTO `wp_postmeta` VALUES (567,99,'_sections_0_current_issue_0_background_images_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (568,99,'sections_0_current_issue_0_background_images_image_4','75');
INSERT INTO `wp_postmeta` VALUES (569,99,'_sections_0_current_issue_0_background_images_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (570,99,'sections_0_current_issue_0_background_images_image_5','77');
INSERT INTO `wp_postmeta` VALUES (571,99,'_sections_0_current_issue_0_background_images_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (572,99,'current_issue_0_background_images_image_1','75');
INSERT INTO `wp_postmeta` VALUES (573,99,'_current_issue_0_background_images_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (574,99,'current_issue_0_background_images_image_2','77');
INSERT INTO `wp_postmeta` VALUES (575,99,'_current_issue_0_background_images_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (576,99,'current_issue_0_background_images_image_3','78');
INSERT INTO `wp_postmeta` VALUES (577,99,'_current_issue_0_background_images_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (578,99,'current_issue_0_background_images_image_4','75');
INSERT INTO `wp_postmeta` VALUES (579,99,'_current_issue_0_background_images_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (580,99,'current_issue_0_background_images_image_5','77');
INSERT INTO `wp_postmeta` VALUES (581,99,'_current_issue_0_background_images_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (582,99,'current_issue_0_background_images','');
INSERT INTO `wp_postmeta` VALUES (583,99,'_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (584,99,'current_issue','1');
INSERT INTO `wp_postmeta` VALUES (585,99,'_current_issue','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (586,99,'sections_0_current_issue_0_image_1','');
INSERT INTO `wp_postmeta` VALUES (587,99,'_sections_0_current_issue_0_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (588,99,'sections_0_current_issue_0_image_2','');
INSERT INTO `wp_postmeta` VALUES (589,99,'_sections_0_current_issue_0_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (590,99,'sections_0_current_issue_0_image_3','');
INSERT INTO `wp_postmeta` VALUES (591,99,'_sections_0_current_issue_0_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (592,99,'sections_0_current_issue_0_image_4','');
INSERT INTO `wp_postmeta` VALUES (593,99,'_sections_0_current_issue_0_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (594,99,'sections_0_current_issue_0_image_5','');
INSERT INTO `wp_postmeta` VALUES (595,99,'_sections_0_current_issue_0_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (596,99,'sections_0_background_images_image_1','75');
INSERT INTO `wp_postmeta` VALUES (597,99,'_sections_0_background_images_image_1','field_5e74143a7bef0');
INSERT INTO `wp_postmeta` VALUES (598,99,'sections_0_background_images_image_2','77');
INSERT INTO `wp_postmeta` VALUES (599,99,'_sections_0_background_images_image_2','field_5e7414594fbfe');
INSERT INTO `wp_postmeta` VALUES (600,99,'sections_0_background_images','');
INSERT INTO `wp_postmeta` VALUES (601,99,'_sections_0_background_images','field_5e73f1ed31b4b');
INSERT INTO `wp_postmeta` VALUES (602,2,'sections_0_background_images_image_3','78');
INSERT INTO `wp_postmeta` VALUES (603,2,'_sections_0_background_images_image_3','field_5e74145c4fc00');
INSERT INTO `wp_postmeta` VALUES (604,2,'sections_0_background_images_image_4','75');
INSERT INTO `wp_postmeta` VALUES (605,2,'_sections_0_background_images_image_4','field_5e74145b4fbff');
INSERT INTO `wp_postmeta` VALUES (606,2,'sections_0_background_images_image_5','77');
INSERT INTO `wp_postmeta` VALUES (607,2,'_sections_0_background_images_image_5','field_5e7414594fbfe');
INSERT INTO `wp_postmeta` VALUES (608,100,'current_issue_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (609,100,'_current_issue_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (610,100,'current_issue_background_images','1');
INSERT INTO `wp_postmeta` VALUES (611,100,'_current_issue_background_images','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (612,100,'sections','a:1:{i:0;s:13:\"current_issue\";}');
INSERT INTO `wp_postmeta` VALUES (613,100,'_sections','field_5e73f1c931b4a');
INSERT INTO `wp_postmeta` VALUES (614,100,'sections_0_current_issue_0_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (615,100,'_sections_0_current_issue_0_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (616,100,'sections_0_current_issue_0_background_images','');
INSERT INTO `wp_postmeta` VALUES (617,100,'_sections_0_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (618,100,'sections_0_current_issue','1');
INSERT INTO `wp_postmeta` VALUES (619,100,'_sections_0_current_issue','field_5e73f1ed31b4b_field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (620,100,'sections_0_current_issue_0_background_images_1_image','');
INSERT INTO `wp_postmeta` VALUES (621,100,'_sections_0_current_issue_0_background_images_1_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (622,100,'sections_0_current_issue_0_background_images_2_image','');
INSERT INTO `wp_postmeta` VALUES (623,100,'_sections_0_current_issue_0_background_images_2_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (624,100,'sections_0_current_issue_0_background_images_image_1','75');
INSERT INTO `wp_postmeta` VALUES (625,100,'_sections_0_current_issue_0_background_images_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (626,100,'sections_0_current_issue_0_background_images_image_2','77');
INSERT INTO `wp_postmeta` VALUES (627,100,'_sections_0_current_issue_0_background_images_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (628,100,'sections_0_current_issue_0_background_images_image_3','78');
INSERT INTO `wp_postmeta` VALUES (629,100,'_sections_0_current_issue_0_background_images_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (630,100,'sections_0_current_issue_0_background_images_image_4','75');
INSERT INTO `wp_postmeta` VALUES (631,100,'_sections_0_current_issue_0_background_images_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (632,100,'sections_0_current_issue_0_background_images_image_5','77');
INSERT INTO `wp_postmeta` VALUES (633,100,'_sections_0_current_issue_0_background_images_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (634,100,'current_issue_0_background_images_image_1','75');
INSERT INTO `wp_postmeta` VALUES (635,100,'_current_issue_0_background_images_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (636,100,'current_issue_0_background_images_image_2','77');
INSERT INTO `wp_postmeta` VALUES (637,100,'_current_issue_0_background_images_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (638,100,'current_issue_0_background_images_image_3','78');
INSERT INTO `wp_postmeta` VALUES (639,100,'_current_issue_0_background_images_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (640,100,'current_issue_0_background_images_image_4','75');
INSERT INTO `wp_postmeta` VALUES (641,100,'_current_issue_0_background_images_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (642,100,'current_issue_0_background_images_image_5','77');
INSERT INTO `wp_postmeta` VALUES (643,100,'_current_issue_0_background_images_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (644,100,'current_issue_0_background_images','');
INSERT INTO `wp_postmeta` VALUES (645,100,'_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (646,100,'current_issue','1');
INSERT INTO `wp_postmeta` VALUES (647,100,'_current_issue','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (648,100,'sections_0_current_issue_0_image_1','');
INSERT INTO `wp_postmeta` VALUES (649,100,'_sections_0_current_issue_0_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (650,100,'sections_0_current_issue_0_image_2','');
INSERT INTO `wp_postmeta` VALUES (651,100,'_sections_0_current_issue_0_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (652,100,'sections_0_current_issue_0_image_3','');
INSERT INTO `wp_postmeta` VALUES (653,100,'_sections_0_current_issue_0_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (654,100,'sections_0_current_issue_0_image_4','');
INSERT INTO `wp_postmeta` VALUES (655,100,'_sections_0_current_issue_0_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (656,100,'sections_0_current_issue_0_image_5','');
INSERT INTO `wp_postmeta` VALUES (657,100,'_sections_0_current_issue_0_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (658,100,'sections_0_background_images_image_1','75');
INSERT INTO `wp_postmeta` VALUES (659,100,'_sections_0_background_images_image_1','field_5e74143a7bef0');
INSERT INTO `wp_postmeta` VALUES (660,100,'sections_0_background_images_image_2','77');
INSERT INTO `wp_postmeta` VALUES (661,100,'_sections_0_background_images_image_2','field_5e74145c4fc01');
INSERT INTO `wp_postmeta` VALUES (662,100,'sections_0_background_images','');
INSERT INTO `wp_postmeta` VALUES (663,100,'_sections_0_background_images','field_5e73f1ed31b4b');
INSERT INTO `wp_postmeta` VALUES (664,100,'sections_0_background_images_image_3','78');
INSERT INTO `wp_postmeta` VALUES (665,100,'_sections_0_background_images_image_3','field_5e74145c4fc00');
INSERT INTO `wp_postmeta` VALUES (666,100,'sections_0_background_images_image_4','75');
INSERT INTO `wp_postmeta` VALUES (667,100,'_sections_0_background_images_image_4','field_5e74145b4fbff');
INSERT INTO `wp_postmeta` VALUES (668,100,'sections_0_background_images_image_5','77');
INSERT INTO `wp_postmeta` VALUES (669,100,'_sections_0_background_images_image_5','field_5e7414594fbfe');
INSERT INTO `wp_postmeta` VALUES (670,101,'current_issue_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (671,101,'_current_issue_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (672,101,'current_issue_background_images','1');
INSERT INTO `wp_postmeta` VALUES (673,101,'_current_issue_background_images','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (674,101,'sections','a:1:{i:0;s:13:\"current_issue\";}');
INSERT INTO `wp_postmeta` VALUES (675,101,'_sections','field_5e73f1c931b4a');
INSERT INTO `wp_postmeta` VALUES (676,101,'sections_0_current_issue_0_background_images_0_image','');
INSERT INTO `wp_postmeta` VALUES (677,101,'_sections_0_current_issue_0_background_images_0_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (678,101,'sections_0_current_issue_0_background_images','');
INSERT INTO `wp_postmeta` VALUES (679,101,'_sections_0_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (680,101,'sections_0_current_issue','1');
INSERT INTO `wp_postmeta` VALUES (681,101,'_sections_0_current_issue','field_5e73f1ed31b4b_field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (682,101,'sections_0_current_issue_0_background_images_1_image','');
INSERT INTO `wp_postmeta` VALUES (683,101,'_sections_0_current_issue_0_background_images_1_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (684,101,'sections_0_current_issue_0_background_images_2_image','');
INSERT INTO `wp_postmeta` VALUES (685,101,'_sections_0_current_issue_0_background_images_2_image','field_5e73f0d7354b6');
INSERT INTO `wp_postmeta` VALUES (686,101,'sections_0_current_issue_0_background_images_image_1','75');
INSERT INTO `wp_postmeta` VALUES (687,101,'_sections_0_current_issue_0_background_images_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (688,101,'sections_0_current_issue_0_background_images_image_2','77');
INSERT INTO `wp_postmeta` VALUES (689,101,'_sections_0_current_issue_0_background_images_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (690,101,'sections_0_current_issue_0_background_images_image_3','78');
INSERT INTO `wp_postmeta` VALUES (691,101,'_sections_0_current_issue_0_background_images_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (692,101,'sections_0_current_issue_0_background_images_image_4','75');
INSERT INTO `wp_postmeta` VALUES (693,101,'_sections_0_current_issue_0_background_images_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (694,101,'sections_0_current_issue_0_background_images_image_5','77');
INSERT INTO `wp_postmeta` VALUES (695,101,'_sections_0_current_issue_0_background_images_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (696,101,'current_issue_0_background_images_image_1','75');
INSERT INTO `wp_postmeta` VALUES (697,101,'_current_issue_0_background_images_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (698,101,'current_issue_0_background_images_image_2','77');
INSERT INTO `wp_postmeta` VALUES (699,101,'_current_issue_0_background_images_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (700,101,'current_issue_0_background_images_image_3','78');
INSERT INTO `wp_postmeta` VALUES (701,101,'_current_issue_0_background_images_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (702,101,'current_issue_0_background_images_image_4','75');
INSERT INTO `wp_postmeta` VALUES (703,101,'_current_issue_0_background_images_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (704,101,'current_issue_0_background_images_image_5','77');
INSERT INTO `wp_postmeta` VALUES (705,101,'_current_issue_0_background_images_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (706,101,'current_issue_0_background_images','');
INSERT INTO `wp_postmeta` VALUES (707,101,'_current_issue_0_background_images','field_5e73f329e9c33');
INSERT INTO `wp_postmeta` VALUES (708,101,'current_issue','1');
INSERT INTO `wp_postmeta` VALUES (709,101,'_current_issue','field_5e73f071354b5');
INSERT INTO `wp_postmeta` VALUES (710,101,'sections_0_current_issue_0_image_1','');
INSERT INTO `wp_postmeta` VALUES (711,101,'_sections_0_current_issue_0_image_1','field_5e73fcf9bfb51');
INSERT INTO `wp_postmeta` VALUES (712,101,'sections_0_current_issue_0_image_2','');
INSERT INTO `wp_postmeta` VALUES (713,101,'_sections_0_current_issue_0_image_2','field_5e73fdd10d359');
INSERT INTO `wp_postmeta` VALUES (714,101,'sections_0_current_issue_0_image_3','');
INSERT INTO `wp_postmeta` VALUES (715,101,'_sections_0_current_issue_0_image_3','field_5e73fdd30d35a');
INSERT INTO `wp_postmeta` VALUES (716,101,'sections_0_current_issue_0_image_4','');
INSERT INTO `wp_postmeta` VALUES (717,101,'_sections_0_current_issue_0_image_4','field_5e73fdd50d35b');
INSERT INTO `wp_postmeta` VALUES (718,101,'sections_0_current_issue_0_image_5','');
INSERT INTO `wp_postmeta` VALUES (719,101,'_sections_0_current_issue_0_image_5','field_5e73fdd60d35c');
INSERT INTO `wp_postmeta` VALUES (720,101,'sections_0_background_images_image_1','75');
INSERT INTO `wp_postmeta` VALUES (721,101,'_sections_0_background_images_image_1','field_5e74143a7bef0');
INSERT INTO `wp_postmeta` VALUES (722,101,'sections_0_background_images_image_2','77');
INSERT INTO `wp_postmeta` VALUES (723,101,'_sections_0_background_images_image_2','field_5e74145c4fc01');
INSERT INTO `wp_postmeta` VALUES (724,101,'sections_0_background_images','');
INSERT INTO `wp_postmeta` VALUES (725,101,'_sections_0_background_images','field_5e73f1ed31b4b');
INSERT INTO `wp_postmeta` VALUES (726,101,'sections_0_background_images_image_3','78');
INSERT INTO `wp_postmeta` VALUES (727,101,'_sections_0_background_images_image_3','field_5e74145c4fc00');
INSERT INTO `wp_postmeta` VALUES (728,101,'sections_0_background_images_image_4','75');
INSERT INTO `wp_postmeta` VALUES (729,101,'_sections_0_background_images_image_4','field_5e74145b4fbff');
INSERT INTO `wp_postmeta` VALUES (730,101,'sections_0_background_images_image_5','77');
INSERT INTO `wp_postmeta` VALUES (731,101,'_sections_0_background_images_image_5','field_5e7414594fbfe');
INSERT INTO `wp_postmeta` VALUES (741,105,'_wp_attached_file','2020/03/olympics.jpg');
INSERT INTO `wp_postmeta` VALUES (742,105,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1424;s:6:\"height\";i:700;s:4:\"file\";s:20:\"2020/03/olympics.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"olympics-300x147.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:147;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"olympics-1024x503.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:503;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"olympics-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"olympics-768x378.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:378;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (745,107,'_edit_lock','1585195620:2');
INSERT INTO `wp_postmeta` VALUES (746,107,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (747,108,'_wp_attached_file','2020/03/issues1.jpg');
INSERT INTO `wp_postmeta` VALUES (748,108,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:453;s:6:\"height\";i:300;s:4:\"file\";s:19:\"2020/03/issues1.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"issues1-300x199.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:199;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"issues1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (749,109,'_wp_attached_file','2020/03/issues2.jpg');
INSERT INTO `wp_postmeta` VALUES (750,109,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:453;s:6:\"height\";i:300;s:4:\"file\";s:19:\"2020/03/issues2.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"issues2-300x199.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:199;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"issues2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (751,110,'_wp_attached_file','2020/03/issues3.jpg');
INSERT INTO `wp_postmeta` VALUES (752,110,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:453;s:6:\"height\";i:300;s:4:\"file\";s:19:\"2020/03/issues3.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"issues3-300x199.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:199;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"issues3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (753,111,'_wp_attached_file','2020/03/issues4.jpg');
INSERT INTO `wp_postmeta` VALUES (754,111,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:453;s:6:\"height\";i:300;s:4:\"file\";s:19:\"2020/03/issues4.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"issues4-300x199.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:199;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"issues4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (755,112,'_wp_attached_file','2020/03/issues5.jpg');
INSERT INTO `wp_postmeta` VALUES (756,112,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:453;s:6:\"height\";i:300;s:4:\"file\";s:19:\"2020/03/issues5.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"issues5-300x199.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:199;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"issues5-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (757,113,'_wp_attached_file','2020/03/issues6.jpg');
INSERT INTO `wp_postmeta` VALUES (758,113,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:453;s:6:\"height\";i:300;s:4:\"file\";s:19:\"2020/03/issues6.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"issues6-300x199.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:199;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"issues6-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (760,107,'_wp_old_date','2020-03-23');
INSERT INTO `wp_postmeta` VALUES (765,17,'_wp_old_slug','phasellus-lobortis-iaculis-egestas');
INSERT INTO `wp_postmeta` VALUES (766,16,'_wp_old_slug','ut-nisl-lacus');
INSERT INTO `wp_postmeta` VALUES (767,15,'_wp_old_slug','pellentesque-et-velit-tincidunt');
INSERT INTO `wp_postmeta` VALUES (768,12,'_wp_old_slug','lorem-ipsum-dolor-sit-amet');
INSERT INTO `wp_postmeta` VALUES (769,107,'_wp_old_slug','aenean-felis-enim');
INSERT INTO `wp_postmeta` VALUES (774,17,'_wp_old_slug','activism');
INSERT INTO `wp_postmeta` VALUES (775,16,'_wp_old_slug','the-future');
INSERT INTO `wp_postmeta` VALUES (776,15,'_wp_old_slug','title-iv');
INSERT INTO `wp_postmeta` VALUES (777,12,'_wp_old_slug','youth-sports-and-culture');
INSERT INTO `wp_postmeta` VALUES (779,116,'_edit_lock','1585097838:2');
INSERT INTO `wp_postmeta` VALUES (780,116,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (783,119,'_wp_attached_file','2020/01/placeholder-banner.png');
INSERT INTO `wp_postmeta` VALUES (784,119,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:675;s:4:\"file\";s:30:\"2020/01/placeholder-banner.png\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"placeholder-banner-300x105.png\";s:5:\"width\";i:300;s:6:\"height\";i:105;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:31:\"placeholder-banner-1024x360.png\";s:5:\"width\";i:1024;s:6:\"height\";i:360;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"placeholder-banner-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"placeholder-banner-768x270.png\";s:5:\"width\";i:768;s:6:\"height\";i:270;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:31:\"placeholder-banner-1536x540.png\";s:5:\"width\";i:1536;s:6:\"height\";i:540;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (786,17,'_thumbnail_id','120');
INSERT INTO `wp_postmeta` VALUES (787,16,'_thumbnail_id','119');
INSERT INTO `wp_postmeta` VALUES (788,15,'_thumbnail_id','119');
INSERT INTO `wp_postmeta` VALUES (789,12,'_thumbnail_id','119');
INSERT INTO `wp_postmeta` VALUES (790,107,'_thumbnail_id','123');
INSERT INTO `wp_postmeta` VALUES (791,120,'_wp_attached_file','2020/03/tackling-mental-health.jpg');
INSERT INTO `wp_postmeta` VALUES (792,120,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1424;s:6:\"height\";i:700;s:4:\"file\";s:34:\"2020/03/tackling-mental-health.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"tackling-mental-health-300x147.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:147;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:35:\"tackling-mental-health-1024x503.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:503;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"tackling-mental-health-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:34:\"tackling-mental-health-768x378.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:378;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (794,123,'_wp_attached_file','2020/01/korean.jpg');
INSERT INTO `wp_postmeta` VALUES (795,123,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:812;s:6:\"height\";i:600;s:4:\"file\";s:18:\"2020/01/korean.jpg\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"korean-300x222.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:222;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"korean-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"korean-768x567.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:567;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (796,125,'_wp_attached_file','2020/03/placeholder-banner.png');
INSERT INTO `wp_postmeta` VALUES (797,125,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:675;s:4:\"file\";s:30:\"2020/03/placeholder-banner.png\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"placeholder-banner-300x105.png\";s:5:\"width\";i:300;s:6:\"height\";i:105;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:31:\"placeholder-banner-1024x360.png\";s:5:\"width\";i:1024;s:6:\"height\";i:360;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"placeholder-banner-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"placeholder-banner-768x270.png\";s:5:\"width\";i:768;s:6:\"height\";i:270;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:31:\"placeholder-banner-1536x540.png\";s:5:\"width\";i:1536;s:6:\"height\";i:540;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (798,126,'_edit_lock','1585244008:2');
INSERT INTO `wp_postmeta` VALUES (799,126,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (800,126,'_thumbnail_id','125');
INSERT INTO `wp_postmeta` VALUES (801,126,'_wp_old_date','2020-03-26');
INSERT INTO `wp_postmeta` VALUES (802,127,'_edit_lock','1585243635:2');
INSERT INTO `wp_postmeta` VALUES (803,127,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (805,127,'_yoast_wpseo_primary_category','45');
INSERT INTO `wp_postmeta` VALUES (806,127,'_thumbnail_id','125');
INSERT INTO `wp_postmeta` VALUES (812,130,'_edit_lock','1585243685:2');
INSERT INTO `wp_postmeta` VALUES (813,130,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (814,130,'_thumbnail_id','125');
INSERT INTO `wp_postmeta` VALUES (815,130,'_wp_old_date','2020-03-26');
INSERT INTO `wp_postmeta` VALUES (816,131,'_edit_lock','1585243997:2');
INSERT INTO `wp_postmeta` VALUES (817,131,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (818,131,'_thumbnail_id','125');
INSERT INTO `wp_postmeta` VALUES (819,131,'_wp_old_date','2020-03-26');
INSERT INTO `wp_postmeta` VALUES (820,131,'_wp_old_date','2019-05-26');
INSERT INTO `wp_postmeta` VALUES (821,126,'_wp_old_date','2019-03-26');
INSERT INTO `wp_postmeta` VALUES (822,132,'_edit_lock','1585246576:2');
INSERT INTO `wp_postmeta` VALUES (823,132,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (824,132,'_wp_old_date','2020-03-26');
INSERT INTO `wp_postmeta` VALUES (825,133,'_edit_lock','1585244102:2');
INSERT INTO `wp_postmeta` VALUES (826,133,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (827,133,'_thumbnail_id','125');
INSERT INTO `wp_postmeta` VALUES (828,133,'_wp_old_date','2020-03-26');
INSERT INTO `wp_postmeta` VALUES (829,134,'_edit_lock','1585246731:2');
INSERT INTO `wp_postmeta` VALUES (830,134,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (831,134,'_thumbnail_id','125');
INSERT INTO `wp_postmeta` VALUES (832,132,'_thumbnail_id','125');
INSERT INTO `wp_postmeta` VALUES (833,134,'_wp_old_date','2020-03-26');
INSERT INTO `wp_postmeta` VALUES (834,135,'_edit_lock','1585324769:2');
INSERT INTO `wp_postmeta` VALUES (835,135,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (836,136,'_wp_attached_file','2020/03/single-issue-banner.jpg');
INSERT INTO `wp_postmeta` VALUES (837,136,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1420;s:6:\"height\";i:600;s:4:\"file\";s:31:\"2020/03/single-issue-banner.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"single-issue-banner-300x127.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:127;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:32:\"single-issue-banner-1024x433.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:433;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"single-issue-banner-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"single-issue-banner-768x325.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:325;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (838,135,'_thumbnail_id','136');
INSERT INTO `wp_postmeta` VALUES (839,137,'_wp_attached_file','2020/03/single-article-img.png');
INSERT INTO `wp_postmeta` VALUES (840,137,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:697;s:6:\"height\";i:452;s:4:\"file\";s:30:\"2020/03/single-article-img.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"single-article-img-300x195.png\";s:5:\"width\";i:300;s:6:\"height\";i:195;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"single-article-img-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (841,135,'_wp_old_date','2020-03-27');
INSERT INTO `wp_postmeta` VALUES (842,143,'_wp_attached_file','2020/03/about-gsm.jpg');
INSERT INTO `wp_postmeta` VALUES (843,143,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1424;s:6:\"height\";i:600;s:4:\"file\";s:21:\"2020/03/about-gsm.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"about-gsm-300x126.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:126;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"about-gsm-1024x431.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:431;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"about-gsm-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"about-gsm-768x324.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (844,64,'_thumbnail_id','143');
INSERT INTO `wp_postmeta` VALUES (845,148,'_wp_attached_file','2020/03/logo-banner.png');
INSERT INTO `wp_postmeta` VALUES (846,148,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:696;s:6:\"height\";i:141;s:4:\"file\";s:23:\"2020/03/logo-banner.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"logo-banner-300x61.png\";s:5:\"width\";i:300;s:6:\"height\";i:61;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"logo-banner-150x141.png\";s:5:\"width\";i:150;s:6:\"height\";i:141;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (847,150,'_wp_attached_file','2020/03/button.png');
INSERT INTO `wp_postmeta` VALUES (848,150,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:197;s:6:\"height\";i:55;s:4:\"file\";s:18:\"2020/03/button.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"button-150x55.png\";s:5:\"width\";i:150;s:6:\"height\";i:55;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (849,153,'_edit_lock','1585707710:2');
INSERT INTO `wp_postmeta` VALUES (850,153,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (851,154,'_wp_attached_file','2020/04/8.png');
INSERT INTO `wp_postmeta` VALUES (852,154,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:212;s:6:\"height\";i:214;s:4:\"file\";s:13:\"2020/04/8.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"8-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (853,155,'_wp_attached_file','2020/04/7.png');
INSERT INTO `wp_postmeta` VALUES (854,155,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:212;s:6:\"height\";i:214;s:4:\"file\";s:13:\"2020/04/7.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"7-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (855,156,'_wp_attached_file','2020/04/5.png');
INSERT INTO `wp_postmeta` VALUES (856,156,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:212;s:6:\"height\";i:214;s:4:\"file\";s:13:\"2020/04/5.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"5-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (857,157,'_wp_attached_file','2020/04/6.png');
INSERT INTO `wp_postmeta` VALUES (858,157,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:212;s:6:\"height\";i:214;s:4:\"file\";s:13:\"2020/04/6.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"6-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (859,158,'_wp_attached_file','2020/04/1.png');
INSERT INTO `wp_postmeta` VALUES (860,158,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:212;s:6:\"height\";i:214;s:4:\"file\";s:13:\"2020/04/1.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (861,159,'_wp_attached_file','2020/04/2.png');
INSERT INTO `wp_postmeta` VALUES (862,159,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:212;s:6:\"height\";i:214;s:4:\"file\";s:13:\"2020/04/2.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (863,160,'_wp_attached_file','2020/04/4.png');
INSERT INTO `wp_postmeta` VALUES (864,160,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:212;s:6:\"height\";i:214;s:4:\"file\";s:13:\"2020/04/4.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (865,161,'_wp_attached_file','2020/04/3.png');
INSERT INTO `wp_postmeta` VALUES (866,161,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:212;s:6:\"height\";i:214;s:4:\"file\";s:13:\"2020/04/3.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:13:\"3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (867,153,'_thumbnail_id','158');
INSERT INTO `wp_postmeta` VALUES (868,162,'_edit_lock','1585707743:2');
INSERT INTO `wp_postmeta` VALUES (869,162,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (870,162,'_thumbnail_id','159');
INSERT INTO `wp_postmeta` VALUES (871,163,'_edit_lock','1585707778:2');
INSERT INTO `wp_postmeta` VALUES (872,163,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (873,163,'_thumbnail_id','161');
INSERT INTO `wp_postmeta` VALUES (874,164,'_edit_lock','1585707808:2');
INSERT INTO `wp_postmeta` VALUES (875,164,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (876,164,'_thumbnail_id','160');
INSERT INTO `wp_postmeta` VALUES (877,165,'_edit_lock','1585707835:2');
INSERT INTO `wp_postmeta` VALUES (878,165,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (879,165,'_thumbnail_id','156');
INSERT INTO `wp_postmeta` VALUES (880,166,'_edit_lock','1585707864:2');
INSERT INTO `wp_postmeta` VALUES (881,166,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (882,166,'_thumbnail_id','157');
INSERT INTO `wp_postmeta` VALUES (883,167,'_edit_lock','1585707901:2');
INSERT INTO `wp_postmeta` VALUES (884,167,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (885,167,'_thumbnail_id','155');
INSERT INTO `wp_postmeta` VALUES (886,168,'_edit_lock','1585707931:2');
INSERT INTO `wp_postmeta` VALUES (887,168,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (888,168,'_thumbnail_id','154');
/*!40000 ALTER TABLE `wp_postmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_posts`
--

LOCK TABLES `wp_posts` WRITE;
/*!40000 ALTER TABLE `wp_posts` DISABLE KEYS */;
INSERT INTO `wp_posts` VALUES (1,1,'2020-03-06 21:07:49','2020-03-06 21:07:49','Proin ante dolor, mollis in velit et, dictum semper nibh. Vivamus sit amet tortor vestibulum, commodo magna sed, facilisis quam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris rhoncus et ipsum ut efficitur. Fusce rutrum dignissim blandit.','Curabitur tortor ex','','publish','open','open','','curabitur-tortor-ex','','','2020-03-11 20:33:41','2020-03-11 20:33:41','',0,'http://localhost:10016/?p=1',0,'post','',1);
INSERT INTO `wp_posts` VALUES (2,1,'2020-03-06 21:07:49','2020-03-06 21:07:49','','Home','','publish','closed','closed','','home','','','2020-03-20 02:17:16','2020-03-20 02:17:16','',0,'http://localhost:10016/?page_id=2',0,'page','',0);
INSERT INTO `wp_posts` VALUES (9,2,'2020-03-09 23:35:57','2020-03-09 23:35:57','','Sections','','publish','closed','closed','','sections','','','2020-03-27 23:36:52','2020-03-27 23:36:52','',0,'http://localhost:10016/?p=9',2,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (10,2,'2020-03-09 23:35:57','2020-03-09 23:35:57','','Issues','','publish','closed','closed','','issues','','','2020-03-27 23:36:52','2020-03-27 23:36:52','',0,'http://localhost:10016/?p=10',1,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (12,2,'2020-03-10 23:00:38','2020-03-10 23:00:38','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu vehicula augue, ac gravida dolor. Pellentesque ultrices augue diam, eget bibendum mi efficitur vel. Curabitur ex risus, euismod eu convallis non, venenatis a diam. Vestibulum non feugiat justo, eget dapibus nulla. Quisque interdum metus non elit elementum, non dignissim turpis rhoncus. Donec tincidunt, sapien at semper ornare, odio dolor auctor ipsum, quis vestibulum ligula turpis et lorem. Fusce semper placerat enim quis dapibus. Proin felis nunc, tincidunt eu tempor et, dictum eu arcu. Vivamus sodales ullamcorper lorem ut venenatis.\r\n\r\nInteger volutpat euismod odio vel dictum. Aenean felis enim, venenatis in sagittis et, feugiat eu erat. Nulla interdum ex ac faucibus rutrum. Nullam vehicula consequat orci, nec tincidunt dui dictum nec. Sed cursus in enim eu finibus. Duis id eros eu massa pretium maximus at id orci. Nulla condimentum sit amet eros vel hendrerit.\r\n\r\nQuisque fringilla quam eu hendrerit sodales. Nam quis elit ac lorem mattis mattis. Fusce ac accumsan ligula. Phasellus elementum sodales leo, in fringilla sapien euismod non. Etiam quis quam in quam pulvinar lobortis quis malesuada sem. Quisque mollis nisi quam, eu hendrerit nisi vehicula nec. Nulla venenatis sapien elit, a dignissim purus tristique vel. Aliquam sed turpis vitae elit eleifend placerat vitae quis eros. Vestibulum metus augue, pretium tempor ultrices non, ullamcorper id odio.\r\n\r\nAliquam erat volutpat. Etiam in neque malesuada, accumsan diam sed, consectetur est. Sed quis auctor nulla, non maximus nibh. Vivamus consequat maximus tincidunt. Etiam mollis ante a metus luctus vulputate. Sed rhoncus sed mauris eget auctor. Nam ultrices nunc mi, non ullamcorper dui blandit vel. Integer consequat viverra fermentum. Donec nec posuere eros. In commodo eu ante vel luctus. Nunc enim diam, gravida sed feugiat at, pharetra quis dolor.\r\n\r\nMorbi gravida sodales velit a aliquam. Etiam a consectetur lorem, non ultricies risus. Vivamus mollis sollicitudin mollis. Praesent sed tincidunt urna. Praesent ac lobortis neque, id tristique metus. Vivamus ipsum augue, sodales vitae ultrices a, accumsan ullamcorper lorem. Fusce augue ex, mollis ac facilisis ut, consectetur sit amet purus.','Mexico City Games Become Launch Pad for Athlete Doping Tests','','publish','closed','closed','','mexico-city-games-became-launch-pad-for-athlete-doping-tests','','','2020-03-25 05:56:53','2020-03-25 05:56:53','',0,'http://localhost:10016/?post_type=issues&#038;p=12',0,'issues','',0);
INSERT INTO `wp_posts` VALUES (15,2,'2020-03-10 23:25:21','2020-03-10 23:25:21','Sed ligula dolor, semper nec varius eget, sodales vel justo. Curabitur et gravida lacus. Sed porta tincidunt ligula, ac ullamcorper est. Ut et mollis leo. Cras augue magna, dapibus non luctus id, lacinia at urna. Nulla imperdiet cursus justo, non efficitur neque dictum eu. Donec posuere nulla vitae risus feugiat luctus. Nam elementum neque vel scelerisque elementum. Fusce a leo turpis. Etiam consequat gravida diam eu tempor. Maecenas gravida libero id nibh vulputate tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi aliquam sodales sapien, et posuere lorem dapibus sed. Duis imperdiet commodo nulla eget ullamcorper. Morbi malesuada dapibus purus ac bibendum.\r\n\r\nPellentesque et velit tincidunt, hendrerit nisl et, faucibus sem. Fusce magna lacus, ultrices sit amet maximus in, dictum at leo. Quisque id tincidunt tellus. Sed interdum auctor nisl vitae finibus. In vitae lectus sapien. Maecenas sit amet porttitor quam, sed accumsan velit. Nulla placerat ut arcu scelerisque lobortis. Cras quis efficitur augue. Etiam laoreet vehicula aliquam. Pellentesque diam mi, feugiat id felis mollis, vestibulum feugiat ipsum. Ut augue velit, volutpat vitae sem quis, cursus elementum augue. Praesent ut lectus id sem blandit consectetur ut sed diam. Etiam ac neque ut nisl tristique maximus vitae eget arcu. Nam interdum dictum nunc sit amet convallis.\r\n\r\nUt nisl lacus, viverra pretium enim sed, iaculis consectetur felis. Maecenas a bibendum augue. Duis dapibus velit mattis augue commodo, a suscipit lorem iaculis. Integer consequat malesuada metus. Mauris lacinia sodales enim et vulputate. Nullam bibendum luctus eros sed consectetur. Maecenas tristique enim sed diam lacinia, a placerat tellus condimentum. Sed vulputate tortor eu odio suscipit congue. Nulla rutrum urna ante, id semper augue fermentum id.\r\n\r\nPhasellus lobortis iaculis egestas. Donec sit amet sem cursus, tempus tellus ac, consectetur lectus. Etiam ipsum enim, lobortis non bibendum in, suscipit eget purus. In aliquet eu ex quis cursus. Mauris quam arcu, porttitor ut eros at, condimentum tincidunt sem. Sed tincidunt, arcu quis lobortis posuere, lectus urna venenatis risus, varius vestibulum justo ligula non tellus. Etiam malesuada ultrices diam, et vulputate augue rhoncus accumsan. Mauris a turpis purus. Sed eros purus, pharetra sed blandit vitae, viverra in dolor. Suspendisse metus lectus, blandit sit amet mi ac, vulputate rhoncus mauris. Etiam tincidunt convallis mi, vel viverra mauris aliquet a. Nunc et urna aliquam, mattis odio ut, vehicula ante. Proin porta mi nec arcu blandit sodales. Etiam nec arcu mi. Mauris fringilla pellentesque nisl ac dignissim.','14 Technologies That Are Changing the Future of the Olympics','','publish','closed','closed','','14-technologies-changing-future-of-olympics','','','2020-03-25 05:56:44','2020-03-25 05:56:44','',0,'http://localhost:10016/?post_type=issues&#038;p=15',0,'issues','',0);
INSERT INTO `wp_posts` VALUES (16,2,'2020-03-10 23:25:54','2020-03-10 23:25:54','Phasellus lobortis iaculis egestas. Donec sit amet sem cursus, tempus tellus ac, consectetur lectus. Etiam ipsum enim, lobortis non bibendum in, suscipit eget purus. In aliquet eu ex quis cursus. Mauris quam arcu, porttitor ut eros at, condimentum tincidunt sem. Sed tincidunt, arcu quis lobortis posuere, lectus urna venenatis risus, varius vestibulum justo ligula non tellus. Etiam malesuada ultrices diam, et vulputate augue rhoncus accumsan. Mauris a turpis purus. Sed eros purus, pharetra sed blandit vitae, viverra in dolor. Suspendisse metus lectus, blandit sit amet mi ac, vulputate rhoncus mauris. Etiam tincidunt convallis mi, vel viverra mauris aliquet a. Nunc et urna aliquam, mattis odio ut, vehicula ante. Proin porta mi nec arcu blandit sodales. Etiam nec arcu mi. Mauris fringilla pellentesque nisl ac dignissim.','Poll: Olympic Athletes Weigh-In on the Protests Surrounding Tokyo','','publish','closed','closed','','poll-olympic-athletes-weigh-in-protests-surrounding-tokyo','','','2020-03-26 01:59:00','2020-03-26 01:59:00','',0,'http://localhost:10016/?post_type=issues&#038;p=16',0,'issues','',0);
INSERT INTO `wp_posts` VALUES (17,2,'2020-03-10 23:26:17','2020-03-10 23:26:17','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam risus turpis, tincidunt eu mattis at, mattis luctus sapien. Etiam id congue felis. Pellentesque vel dictum sapien, in consectetur eros. Proin tellus nunc, varius ut diam nec, pellentesque dignissim nibh. Fusce ipsum nibh, pulvinar in hendrerit a, maximus a erat. Proin et diam ultrices, aliquam mauris nec, venenatis tellus. Morbi eu tellus placerat tellus facilisis ornare.\r\n\r\nSed ligula dolor, semper nec varius eget, sodales vel justo. Curabitur et gravida lacus. Sed porta tincidunt ligula, ac ullamcorper est. Ut et mollis leo. Cras augue magna, dapibus non luctus id, lacinia at urna. Nulla imperdiet cursus justo, non efficitur neque dictum eu. Donec posuere nulla vitae risus feugiat luctus. Nam elementum neque vel scelerisque elementum. Fusce a leo turpis. Etiam consequat gravida diam eu tempor. Maecenas gravida libero id nibh vulputate tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi aliquam sodales sapien, et posuere lorem dapibus sed. Duis imperdiet commodo nulla eget ullamcorper. Morbi malesuada dapibus purus ac bibendum.\r\n\r\n<img class=\"alignnone wp-image-137 size-full\" src=\"http://localhost:10016/wp-content/uploads/2020/03/single-article-img.png\" alt=\"\" width=\"697\" height=\"452\" />\r\n\r\nPellentesque et velit tincidunt, hendrerit nisl et, faucibus sem. Fusce magna lacus, ultrices sit amet maximus in, dictum at leo. Quisque id tincidunt tellus. Sed interdum auctor nisl vitae finibus. In vitae lectus sapien. Maecenas sit amet porttitor quam, sed accumsan velit. Nulla placerat ut arcu scelerisque lobortis. Cras quis efficitur augue. Etiam laoreet vehicula aliquam. Pellentesque diam mi, feugiat id felis mollis, vestibulum feugiat ipsum. Ut augue velit, volutpat vitae sem quis, cursus elementum augue. Praesent ut lectus id sem blandit consectetur ut sed diam. Etiam ac neque ut nisl tristique maximus vitae eget arcu. Nam interdum dictum nunc sit amet convallis.\r\n<blockquote>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ultrices metus placerat egestas sollicitudin. Nullam eleifend massa quis venenatis rhoncus.”</blockquote>\r\nNullam rutrum fringilla risus non rutrum. Suspendisse dictum bibendum ullamcorper. Etiam aliquet erat est, cursus venenatis arcu consequat rhoncus. Integer sit amet ex sed dolor suscipit vulputate et eget nisl. In scelerisque viverra nisi, ac tincidunt ligula sodales vitae. Nam pretium augue gravida turpis accumsan, eget finibus ante blandit. Cras ac odio purus.','Tackling Mental Health in Future Olympic Games','','publish','closed','closed','','tackling-mental-health-in-future-olympic-games','','','2020-03-27 17:30:35','2020-03-27 17:30:35','',0,'http://localhost:10016/?post_type=issues&#038;p=17',0,'issues','',0);
INSERT INTO `wp_posts` VALUES (19,2,'2020-03-11 00:32:40','2020-03-11 00:32:40','','hero','','inherit','open','closed','','hero','','','2020-03-11 00:32:40','2020-03-11 00:32:40','',17,'http://localhost:10016/wp-content/uploads/2020/03/hero.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (21,2,'2020-03-11 19:06:51','2020-03-11 19:06:51','<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->','Curabitur tortor ex','','inherit','closed','closed','','1-revision-v1','','','2020-03-11 19:06:51','2020-03-11 19:06:51','',1,'http://localhost:10016/1-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (22,2,'2020-03-11 19:07:08','2020-03-11 19:07:08','Nunc elementum tortor ac arcu lobortis viverra. Phasellus ut ornare dolor. Morbi pellentesque vitae lectus at vulputate. Ut tempus odio condimentum venenatis malesuada. Mauris ac nunc neque. Curabitur id enim ac libero tempor porta. Curabitur tortor ex, sagittis vitae libero non, consectetur auctor elit.','Sed hendrerit nec lectus non malesuada','','publish','open','open','','sed-hendrerit-nec-lectus-non-malesuada','','','2020-03-11 20:59:22','2020-03-11 20:59:22','',0,'http://localhost:10016/?p=22',0,'post','',0);
INSERT INTO `wp_posts` VALUES (23,2,'2020-03-11 19:07:08','2020-03-11 19:07:08','','Sed hendrerit nec lectus non malesuada','','inherit','closed','closed','','22-revision-v1','','','2020-03-11 19:07:08','2020-03-11 19:07:08','',22,'http://localhost:10016/22-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (24,2,'2020-03-11 19:07:29','2020-03-11 19:07:29','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce odio enim, facilisis eget sodales et, mattis ut dui. Fusce rutrum commodo dui quis sollicitudin. Nam sagittis tellus vestibulum nulla auctor, sed auctor ligula aliquam. Nulla sagittis ipsum vitae ante malesuada, eget pulvinar ipsum egestas. Nunc id cursus ipsum. Phasellus porttitor lobortis sapien.','In eu tincidunt lorem','','publish','open','open','','in-eu-tincidunt-lorem','','','2020-03-11 20:57:11','2020-03-11 20:57:11','',0,'http://localhost:10016/?p=24',0,'post','',0);
INSERT INTO `wp_posts` VALUES (25,2,'2020-03-11 19:07:29','2020-03-11 19:07:29','','In eu tincidunt lorem','','inherit','closed','closed','','24-revision-v1','','','2020-03-11 19:07:29','2020-03-11 19:07:29','',24,'http://localhost:10016/24-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (26,2,'2020-03-11 19:08:41','2020-03-11 19:08:41','Donec ultricies lacus est, nec luctus nulla cursus ac. In nec fringilla mauris, at porttitor lectus. Vestibulum rhoncus et eros ut ornare. Curabitur pellentesque, felis vitae accumsan accumsan, diam erat tincidunt nulla, nec sodales arcu orci vitae leo. Etiam ante lacus, euismod sit amet mattis ac, blandit egestas risus. In sit amet nibh ac risus mattis condimentum posuere sed sapien. Nullam pretium, dolor a sollicitudin faucibus, tellus ex tempor quam, a suscipit sapien sem sed felis. Fusce rhoncus leo ut metus vehicula vulputate. Mauris quis dui ut purus vestibulum finibus. ','Nam sagittis tellus vestibulum nulla auctor','','publish','open','open','','nam-sagittis-tellus-vestibulum-nulla-auctor','','','2020-03-25 23:44:24','2020-03-25 23:44:24','',0,'http://localhost:10016/?p=26',0,'post','',0);
INSERT INTO `wp_posts` VALUES (27,2,'2020-03-11 19:08:41','2020-03-11 19:08:41','Donec ultricies lacus est, nec luctus nulla cursus ac. In nec fringilla mauris, at porttitor lectus. Vestibulum rhoncus et eros ut ornare. Curabitur pellentesque, felis vitae accumsan accumsan, diam erat tincidunt nulla, nec sodales arcu orci vitae leo. Etiam ante lacus, euismod sit amet mattis ac, blandit egestas risus. In sit amet nibh ac risus mattis condimentum posuere sed sapien. Nullam pretium, dolor a sollicitudin faucibus, tellus ex tempor quam, a suscipit sapien sem sed felis. Fusce rhoncus leo ut metus vehicula vulputate. Mauris quis dui ut purus vestibulum finibus. ','Nam sagittis tellus vestibulum nulla auctor','','inherit','closed','closed','','26-revision-v1','','','2020-03-11 19:08:41','2020-03-11 19:08:41','',26,'http://localhost:10016/26-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (28,2,'2020-03-11 19:09:02','2020-03-11 19:09:02','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce odio enim, facilisis eget sodales et, mattis ut dui. Fusce rutrum commodo dui quis sollicitudin. Nam sagittis tellus vestibulum nulla auctor, sed auctor ligula aliquam. Nulla sagittis ipsum vitae ante malesuada, eget pulvinar ipsum egestas. Nunc id cursus ipsum. Phasellus porttitor lobortis sapien.','In eu tincidunt lorem','','inherit','closed','closed','','24-revision-v1','','','2020-03-11 19:09:02','2020-03-11 19:09:02','',24,'http://localhost:10016/24-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (29,2,'2020-03-11 19:09:16','2020-03-11 19:09:16','Nunc elementum tortor ac arcu lobortis viverra. Phasellus ut ornare dolor. Morbi pellentesque vitae lectus at vulputate. Ut tempus odio condimentum venenatis malesuada. Mauris ac nunc neque. Curabitur id enim ac libero tempor porta. Curabitur tortor ex, sagittis vitae libero non, consectetur auctor elit.','Sed hendrerit nec lectus non malesuada','','inherit','closed','closed','','22-revision-v1','','','2020-03-11 19:09:16','2020-03-11 19:09:16','',22,'http://localhost:10016/22-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (30,2,'2020-03-11 19:09:36','2020-03-11 19:09:36','Proin ante dolor, mollis in velit et, dictum semper nibh. Vivamus sit amet tortor vestibulum, commodo magna sed, facilisis quam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris rhoncus et ipsum ut efficitur. Fusce rutrum dignissim blandit.','Curabitur tortor ex','','inherit','closed','closed','','1-revision-v1','','','2020-03-11 19:09:36','2020-03-11 19:09:36','',1,'http://localhost:10016/1-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (31,2,'2020-03-11 19:35:10','2020-03-11 19:35:10','','soccer','','inherit','open','closed','','soccer','','','2020-03-11 19:35:10','2020-03-11 19:35:10','',26,'http://localhost:10016/wp-content/uploads/2020/03/soccer.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (32,2,'2020-03-11 20:56:28','2020-03-11 20:56:28','','football','','inherit','open','closed','','football','','','2020-03-11 20:56:28','2020-03-11 20:56:28','',24,'http://localhost:10016/wp-content/uploads/2020/03/football.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (33,2,'2020-03-11 20:59:19','2020-03-11 20:59:19','','runner','','inherit','open','closed','','runner','','','2020-03-11 20:59:19','2020-03-11 20:59:19','',22,'http://localhost:10016/wp-content/uploads/2020/03/runner.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (34,2,'2020-03-11 20:59:25','2020-03-11 20:59:25','Nunc elementum tortor ac arcu lobortis viverra. Phasellus ut ornare dolor. Morbi pellentesque vitae lectus at vulputate. Ut tempus odio condimentum venenatis malesuada. Mauris ac nunc neque. Curabitur id enim ac libero tempor porta. Curabitur tortor ex, sagittis vitae libero non, consectetur auctor elit.','Sed hendrerit nec lectus non malesuada','','inherit','closed','closed','','22-autosave-v1','','','2020-03-11 20:59:25','2020-03-11 20:59:25','',22,'http://localhost:10016/22-autosave-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (35,2,'2020-03-11 22:15:43','2020-03-11 22:15:43','','video-placeholder','','inherit','open','closed','','video-placeholder','','','2020-03-11 22:15:43','2020-03-11 22:15:43','',0,'http://localhost:10016/wp-content/uploads/2020/03/video-placeholder.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (36,2,'2020-03-12 00:19:59','2020-03-12 00:19:59','In tellus integer feugiat scelerisque. Sed viverra ipsum nunc aliquet bibendum. Morbi tristique senectus et netus et malesuada fames ac. Ut porttitor leo a diam sollicitudin tempor id eu. Id leo in vitae turpis massa sed. Egestas sed tempus urna et. Mi eget mauris pharetra et. Eget nulla facilisi etiam dignissim diam quis enim. ','Ipsum faucibus vitae aliquet','','publish','open','open','','ipsum-faucibus-vitae-aliquet','','','2020-03-12 00:19:59','2020-03-12 00:19:59','',0,'http://localhost:10016/?p=36',0,'post','',0);
INSERT INTO `wp_posts` VALUES (37,2,'2020-03-12 00:19:55','2020-03-12 00:19:55','','young-female-atheletes','','inherit','open','closed','','young-female-atheletes','','','2020-03-12 00:19:55','2020-03-12 00:19:55','',36,'http://localhost:10016/wp-content/uploads/2020/03/young-female-atheletes.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (38,2,'2020-03-12 00:19:59','2020-03-12 00:19:59','In tellus integer feugiat scelerisque. Sed viverra ipsum nunc aliquet bibendum. Morbi tristique senectus et netus et malesuada fames ac. Ut porttitor leo a diam sollicitudin tempor id eu. Id leo in vitae turpis massa sed. Egestas sed tempus urna et. Mi eget mauris pharetra et. Eget nulla facilisi etiam dignissim diam quis enim. ','Ipsum faucibus vitae aliquet','','inherit','closed','closed','','36-revision-v1','','','2020-03-12 00:19:59','2020-03-12 00:19:59','',36,'http://localhost:10016/36-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (39,2,'2020-03-12 00:20:17','2020-03-12 00:20:17','','jack-rutter','','inherit','open','closed','','jack-rutter','','','2020-03-12 00:20:17','2020-03-12 00:20:17','',36,'http://localhost:10016/wp-content/uploads/2020/03/jack-rutter.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (40,2,'2020-03-12 00:20:17','2020-03-12 00:20:17','','montanos','','inherit','open','closed','','montanos','','','2020-03-12 00:20:17','2020-03-12 00:20:17','',36,'http://localhost:10016/wp-content/uploads/2020/03/montanos.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (41,2,'2020-03-12 00:20:55','2020-03-12 00:20:55','In tellus integer feugiat scelerisque. Sed viverra ipsum nunc aliquet bibendum. Morbi tristique senectus et netus et malesuada fames ac. Ut porttitor leo a diam sollicitudin tempor id eu. Id leo in vitae turpis massa sed. Egestas sed tempus urna et. Mi eget mauris pharetra et. Eget nulla facilisi etiam dignissim diam quis enim. Quisque non tellus orci ac auctor augue mauris augue neque. Morbi tincidunt augue interdum velit euismod. Dictumst quisque sagittis purus sit. Mauris cursus mattis molestie a iaculis at erat pellentesque. Tellus in hac habitasse platea dictumst vestibulum rhoncus est pellentesque. Iaculis eu non diam phasellus vestibulum. Eu lobortis elementum nibh tellus molestie nunc non blandit massa.','Ullamcorper a lacus vestibulum sed arcu non','','publish','open','open','','ullamcorper-a-lacus-vestibulum-sed-arcu-non','','','2020-03-12 00:20:55','2020-03-12 00:20:55','',0,'http://localhost:10016/?p=41',0,'post','',0);
INSERT INTO `wp_posts` VALUES (42,2,'2020-03-12 00:20:55','2020-03-12 00:20:55','In tellus integer feugiat scelerisque. Sed viverra ipsum nunc aliquet bibendum. Morbi tristique senectus et netus et malesuada fames ac. Ut porttitor leo a diam sollicitudin tempor id eu. Id leo in vitae turpis massa sed. Egestas sed tempus urna et. Mi eget mauris pharetra et. Eget nulla facilisi etiam dignissim diam quis enim. Quisque non tellus orci ac auctor augue mauris augue neque. Morbi tincidunt augue interdum velit euismod. Dictumst quisque sagittis purus sit. Mauris cursus mattis molestie a iaculis at erat pellentesque. Tellus in hac habitasse platea dictumst vestibulum rhoncus est pellentesque. Iaculis eu non diam phasellus vestibulum. Eu lobortis elementum nibh tellus molestie nunc non blandit massa.','Ullamcorper a lacus vestibulum sed arcu non','','inherit','closed','closed','','41-revision-v1','','','2020-03-12 00:20:55','2020-03-12 00:20:55','',41,'http://localhost:10016/41-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (43,2,'2020-03-12 00:21:25','2020-03-12 00:21:25','Mus mauris vitae ultricies leo integer malesuada. Sapien nec sagittis aliquam malesuada. Mattis rhoncus urna neque viverra justo nec ultrices dui sapien. Tellus mauris a diam maecenas sed. Id consectetur purus ut faucibus pulvinar elementum integer enim. Aliquam ut porttitor leo a diam sollicitudin tempor id. Enim ut sem viverra aliquet eget. Parturient montes nascetur ridiculus mus. Enim facilisis gravida neque convallis a cras semper auctor neque. Tellus elementum sagittis vitae et leo duis ut diam. Nunc faucibus a pellentesque sit amet porttitor eget dolor morbi. Lectus magna fringilla urna porttitor.','Egestas pretium aenean pharetra magna ac','','publish','open','open','','egestas-pretium-aenean-pharetra-magna-ac','','','2020-03-12 00:21:25','2020-03-12 00:21:25','',0,'http://localhost:10016/?p=43',0,'post','',0);
INSERT INTO `wp_posts` VALUES (44,2,'2020-03-12 00:21:25','2020-03-12 00:21:25','Mus mauris vitae ultricies leo integer malesuada. Sapien nec sagittis aliquam malesuada. Mattis rhoncus urna neque viverra justo nec ultrices dui sapien. Tellus mauris a diam maecenas sed. Id consectetur purus ut faucibus pulvinar elementum integer enim. Aliquam ut porttitor leo a diam sollicitudin tempor id. Enim ut sem viverra aliquet eget. Parturient montes nascetur ridiculus mus. Enim facilisis gravida neque convallis a cras semper auctor neque. Tellus elementum sagittis vitae et leo duis ut diam. Nunc faucibus a pellentesque sit amet porttitor eget dolor morbi. Lectus magna fringilla urna porttitor.','Egestas pretium aenean pharetra magna ac','','inherit','closed','closed','','43-revision-v1','','','2020-03-12 00:21:25','2020-03-12 00:21:25','',43,'http://localhost:10016/43-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (45,2,'2020-03-12 16:02:33','2020-03-12 16:02:33','Lorem sed risus ultricies tristique nulla. Donec adipiscing tristique risus nec feugiat in fermentum posuere urna. In fermentum et sollicitudin ac orci phasellus egestas tellus. Est ante in nibh mauris cursus mattis molestie a. Pretium nibh ipsum consequat nisl vel pretium lectus quam. Semper risus in hendrerit gravida rutrum quisque non.','Nisi porta lorem mollis aliquam ut porttitor','','publish','open','open','','nisi-porta-lorem-mollis-aliquam-ut-porttitor','','','2020-03-12 16:02:46','2020-03-12 16:02:46','',0,'http://localhost:10016/?p=45',0,'post','',0);
INSERT INTO `wp_posts` VALUES (46,2,'2020-03-12 16:02:33','2020-03-12 16:02:33','Lorem sed risus ultricies tristique nulla. Donec adipiscing tristique risus nec feugiat in fermentum posuere urna. In fermentum et sollicitudin ac orci phasellus egestas tellus. Est ante in nibh mauris cursus mattis molestie a. Pretium nibh ipsum consequat nisl vel pretium lectus quam. Semper risus in hendrerit gravida rutrum quisque non.','Nisi porta lorem mollis aliquam ut porttitor','','inherit','closed','closed','','45-revision-v1','','','2020-03-12 16:02:33','2020-03-12 16:02:33','',45,'http://localhost:10016/45-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (47,2,'2020-03-12 16:04:09','2020-03-12 16:04:09','Augue eget arcu dictum varius duis. Pellentesque habitant morbi tristique senectus et. Vitae nunc sed velit dignissim. Purus non enim praesent elementum facilisis leo. Volutpat commodo sed egestas egestas fringilla phasellus faucibus. Dolor sed viverra ipsum nunc aliquet bibendum. Nisi porta lorem mollis aliquam ut porttitor. Morbi tristique senectus et netus et. Lorem sed risus ultricies tristique nulla. Donec adipiscing tristique risus nec feugiat in fermentum posuere urna. In fermentum et sollicitudin ac orci phasellus egestas tellus. Est ante in nibh mauris cursus mattis molestie a. Pretium nibh ipsum consequat nisl vel pretium lectus quam. Semper risus in hendrerit gravida rutrum quisque non.','Dolor sed viverra ipsum nunc','','publish','open','open','','dolor-sed-viverra-ipsum-nunc','','','2020-03-16 18:12:21','2020-03-16 18:12:21','',0,'http://localhost:10016/?p=47',0,'post','',0);
INSERT INTO `wp_posts` VALUES (48,2,'2020-03-12 16:04:09','2020-03-12 16:04:09','','Dolor sed viverra ipsum nunc','','inherit','closed','closed','','47-revision-v1','','','2020-03-12 16:04:09','2020-03-12 16:04:09','',47,'http://localhost:10016/47-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (49,2,'2020-03-12 16:04:20','2020-03-12 16:04:20','Augue eget arcu dictum varius duis. Pellentesque habitant morbi tristique senectus et. Vitae nunc sed velit dignissim. Purus non enim praesent elementum facilisis leo. Volutpat commodo sed egestas egestas fringilla phasellus faucibus. Dolor sed viverra ipsum nunc aliquet bibendum. Nisi porta lorem mollis aliquam ut porttitor. Morbi tristique senectus et netus et. Lorem sed risus ultricies tristique nulla. Donec adipiscing tristique risus nec feugiat in fermentum posuere urna. In fermentum et sollicitudin ac orci phasellus egestas tellus. Est ante in nibh mauris cursus mattis molestie a. Pretium nibh ipsum consequat nisl vel pretium lectus quam. Semper risus in hendrerit gravida rutrum quisque non.','Dolor sed viverra ipsum nunc','','inherit','closed','closed','','47-revision-v1','','','2020-03-12 16:04:20','2020-03-12 16:04:20','',47,'http://localhost:10016/47-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (50,2,'2020-03-12 16:19:52','2020-03-12 16:19:52','At urna condimentum mattis pellentesque id nibh tortor. Enim ut tellus elementum sagittis. In vitae turpis massa sed elementum tempus egestas sed. Tortor at auctor urna nunc id. Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant. Praesent elementum facilisis leo vel fringilla. Lacus suspendisse faucibus interdum posuere. Ut faucibus pulvinar elementum integer enim neque volutpat ac tincidunt. Facilisi morbi tempus iaculis urna.','Quam adipiscing vitae proin','','publish','open','open','','quam-adipiscing-vitae-proin','','','2020-03-12 16:19:52','2020-03-12 16:19:52','',0,'http://localhost:10016/?p=50',0,'post','',0);
INSERT INTO `wp_posts` VALUES (51,2,'2020-03-12 16:19:52','2020-03-12 16:19:52','At urna condimentum mattis pellentesque id nibh tortor. Enim ut tellus elementum sagittis. In vitae turpis massa sed elementum tempus egestas sed. Tortor at auctor urna nunc id. Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant. Praesent elementum facilisis leo vel fringilla. Lacus suspendisse faucibus interdum posuere. Ut faucibus pulvinar elementum integer enim neque volutpat ac tincidunt. Facilisi morbi tempus iaculis urna.','Quam adipiscing vitae proin','','inherit','closed','closed','','50-revision-v1','','','2020-03-12 16:19:52','2020-03-12 16:19:52','',50,'http://localhost:10016/50-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (59,2,'2020-03-13 23:50:45','2020-03-13 23:50:45','Proin rhoncus enim vel elit mollis consequat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla et ante sit amet ex tincidunt porttitor vel non lacus. Vivamus rutrum orci justo, ut porta odio finibus ac. Nulla sit amet lectus dui. Sed diam ipsum, rutrum ut consequat quis, aliquet non tellus. Aenean eget quam metus. Pellentesque dui est, gravida porta nunc id, semper molestie urna. Nullam tincidunt metus id nunc ornare vehicula. Donec in magna at purus dictum volutpat eu vitae enim.','Ut sed fringilla mauris','','publish','open','open','','ut-sed-fringilla-mauris','','','2020-03-13 23:51:03','2020-03-13 23:51:03','',0,'http://localhost:10016/?p=59',0,'post','',0);
INSERT INTO `wp_posts` VALUES (60,2,'2020-03-13 23:50:45','2020-03-13 23:50:45','Proin rhoncus enim vel elit mollis consequat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla et ante sit amet ex tincidunt porttitor vel non lacus. Vivamus rutrum orci justo, ut porta odio finibus ac. Nulla sit amet lectus dui. Sed diam ipsum, rutrum ut consequat quis, aliquet non tellus. Aenean eget quam metus. Pellentesque dui est, gravida porta nunc id, semper molestie urna. Nullam tincidunt metus id nunc ornare vehicula. Donec in magna at purus dictum volutpat eu vitae enim.','Ut sed fringilla mauris','','inherit','closed','closed','','59-revision-v1','','','2020-03-13 23:50:45','2020-03-13 23:50:45','',59,'http://localhost:10016/59-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (61,2,'2020-03-13 23:50:59','2020-03-13 23:50:59','','yoga','','inherit','open','closed','','yoga','','','2020-03-13 23:50:59','2020-03-13 23:50:59','',59,'http://localhost:10016/wp-content/uploads/2020/03/yoga.png',0,'attachment','image/png',0);
INSERT INTO `wp_posts` VALUES (62,2,'2020-03-16 18:12:18','2020-03-16 18:12:18','','physique','','inherit','open','closed','','physique','','','2020-03-16 18:12:18','2020-03-16 18:12:18','',47,'http://localhost:10016/wp-content/uploads/2020/03/physique.png',0,'attachment','image/png',0);
INSERT INTO `wp_posts` VALUES (63,2,'2020-03-16 18:12:23','2020-03-16 18:12:23','Augue eget arcu dictum varius duis. Pellentesque habitant morbi tristique senectus et. Vitae nunc sed velit dignissim. Purus non enim praesent elementum facilisis leo. Volutpat commodo sed egestas egestas fringilla phasellus faucibus. Dolor sed viverra ipsum nunc aliquet bibendum. Nisi porta lorem mollis aliquam ut porttitor. Morbi tristique senectus et netus et. Lorem sed risus ultricies tristique nulla. Donec adipiscing tristique risus nec feugiat in fermentum posuere urna. In fermentum et sollicitudin ac orci phasellus egestas tellus. Est ante in nibh mauris cursus mattis molestie a. Pretium nibh ipsum consequat nisl vel pretium lectus quam. Semper risus in hendrerit gravida rutrum quisque non.','Dolor sed viverra ipsum nunc','','inherit','closed','closed','','47-autosave-v1','','','2020-03-16 18:12:23','2020-03-16 18:12:23','',47,'http://localhost:10016/47-autosave-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (64,2,'2020-03-19 00:08:41','2020-03-19 00:08:41','<h2>Amplifying the Power of Sport</h2>\r\n<ul>\r\n 	<li>By connecting peoople with research insights</li>\r\n 	<li>By examining the impact on society</li>\r\n 	<li>By providing context to sports globally</li>\r\n</ul>\r\n\r\n<hr />\r\n\r\nGlobalSport Matters is a multimedia content hub that delivers timely information on critical issues affecting sport and connects people with the knowledge they need. Praesent sed purus vitae turpis placerat venenatis. Donec nec magna sed metus finibus tempus. Etiam tincidunt justo non felis varius bibendum. Praesent bibendum vitae justo eu semper.\r\n\r\nVestibulum porttitor cursus purus a mattis. Duis scelerisque justo id ex interdum, vitae congue sem blandit. Mauris tempus quis quam at interdum. Praesent sit amet massa quis odio interdum tincidunt.\r\n\r\n<img class=\"alignnone size-full wp-image-150\" src=\"http://localhost:10016/wp-content/uploads/2020/03/button.png\" alt=\"\" width=\"197\" height=\"55\" />\r\n\r\nSed laoreet turpis sem, a semper eros varius ut. In pharetra leo libero, vitae eleifend libero feugiat ac. Maecenas risus velit, vestibulum vel erat non, viverra ullamcorper tortor. Suspendisse urna purus, sollicitudin in eros vel, gravida malesuada dolor. Nulla aliquam vestibulum odio, non sollicitudin nibh sollicitudin at. Suspendisse pretium velit tellus, pretium porttitor elit imperdiet nec. Maecenas eu nunc ligula. Proin id vulputate ante.\r\n\r\n<hr />\r\n\r\n<h2>Headline About Collaboration</h2>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sagittis mi dui, sit amet pellentesque ipsum posuere at. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin magna nisi, porta sit amet consectetur feugiat, laoreet nec mauris. Donec tempus finibus ipsum eu convallis.\r\n\r\n<img class=\"alignnone wp-image-148 size-full\" src=\"http://localhost:10016/wp-content/uploads/2020/03/logo-banner.png\" alt=\"\" width=\"696\" height=\"141\" />\r\n\r\n<hr />\r\n\r\n&nbsp;','About GSM','','publish','closed','closed','','about','','','2020-04-01 04:53:45','2020-04-01 04:53:45','',0,'http://localhost:10016/?page_id=64',0,'page','',0);
INSERT INTO `wp_posts` VALUES (65,2,'2020-03-19 00:08:41','2020-03-19 00:08:41','','About','','inherit','closed','closed','','64-revision-v1','','','2020-03-19 00:08:41','2020-03-19 00:08:41','',64,'http://localhost:10016/64-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (66,2,'2020-03-19 00:09:03','2020-03-19 00:09:03','','About','','publish','closed','closed','','66','','','2020-03-27 23:36:52','2020-03-27 23:36:52','',0,'http://localhost:10016/?p=66',3,'nav_menu_item','',0);
INSERT INTO `wp_posts` VALUES (70,2,'2020-03-19 22:28:32','2020-03-19 22:28:32','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"2\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:14:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:7:\"excerpt\";i:3;s:10:\"discussion\";i:4;s:8:\"comments\";i:5;s:9:\"revisions\";i:6;s:4:\"slug\";i:7;s:6:\"author\";i:8;s:6:\"format\";i:9;s:15:\"page_attributes\";i:10;s:14:\"featured_image\";i:11;s:10:\"categories\";i:12;s:4:\"tags\";i:13;s:15:\"send-trackbacks\";}s:11:\"description\";s:0:\"\";}','Sections','sections','publish','closed','closed','','group_5e73f1c29ff68','','','2020-03-23 00:12:00','2020-03-23 00:12:00','',0,'http://localhost:10016/?post_type=acf-field-group&#038;p=70',0,'acf-field-group','',0);
INSERT INTO `wp_posts` VALUES (71,2,'2020-03-19 22:28:32','2020-03-19 22:28:32','a:9:{s:4:\"type\";s:16:\"flexible_content\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"layouts\";a:1:{s:20:\"layout_5e73f1ddd6577\";a:6:{s:3:\"key\";s:20:\"layout_5e73f1ddd6577\";s:5:\"label\";s:21:\"Current Issue Section\";s:4:\"name\";s:13:\"current_issue\";s:7:\"display\";s:5:\"block\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}}s:12:\"button_label\";s:7:\"Add Row\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";}','Sections','sections','publish','closed','closed','','field_5e73f1c931b4a','','','2020-03-19 22:51:35','2020-03-19 22:51:35','',70,'http://localhost:10016/?post_type=acf-field&#038;p=71',0,'acf-field','',0);
INSERT INTO `wp_posts` VALUES (72,2,'2020-03-19 22:28:32','2020-03-19 22:28:32','a:8:{s:4:\"type\";s:5:\"group\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"parent_layout\";s:20:\"layout_5e73f1ddd6577\";s:6:\"layout\";s:5:\"table\";s:10:\"sub_fields\";a:0:{}}','Background Images','background_images','publish','closed','closed','','field_5e73f1ed31b4b','','','2020-03-20 00:55:58','2020-03-20 00:55:58','',71,'http://localhost:10016/?post_type=acf-field&#038;p=72',0,'acf-field','',0);
INSERT INTO `wp_posts` VALUES (73,2,'2020-03-19 22:30:38','2020-03-19 22:30:38','','Home','','inherit','closed','closed','','2-revision-v1','','','2020-03-19 22:30:38','2020-03-19 22:30:38','',2,'http://localhost:10016/2-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (75,2,'2020-03-19 22:41:08','2020-03-19 22:41:08','','featured-issue-bg','','inherit','open','closed','','featured-issue-bg','','','2020-03-19 22:41:08','2020-03-19 22:41:08','',2,'http://localhost:10016/wp-content/uploads/2020/03/featured-issue-bg.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (76,2,'2020-03-19 22:41:15','2020-03-19 22:41:15','','Home','','inherit','closed','closed','','2-revision-v1','','','2020-03-19 22:41:15','2020-03-19 22:41:15','',2,'http://localhost:10016/2-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (77,2,'2020-03-19 22:41:39','2020-03-19 22:41:39','','featured-issue-bg2','','inherit','open','closed','','featured-issue-bg2','','','2020-03-19 22:41:39','2020-03-19 22:41:39','',2,'http://localhost:10016/wp-content/uploads/2020/03/featured-issue-bg2.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (78,2,'2020-03-19 22:41:39','2020-03-19 22:41:39','','featured-issue-bg3','','inherit','open','closed','','featured-issue-bg3','','','2020-03-19 22:41:39','2020-03-19 22:41:39','',2,'http://localhost:10016/wp-content/uploads/2020/03/featured-issue-bg3.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (79,2,'2020-03-19 22:41:50','2020-03-19 22:41:50','','Home','','inherit','closed','closed','','2-revision-v1','','','2020-03-19 22:41:50','2020-03-19 22:41:50','',2,'http://localhost:10016/2-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (80,2,'2020-03-19 22:41:53','2020-03-19 22:41:53','','Home','','inherit','closed','closed','','2-autosave-v1','','','2020-03-19 22:41:53','2020-03-19 22:41:53','',2,'http://localhost:10016/2-autosave-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (81,2,'2020-03-19 22:49:20','2020-03-19 22:49:20','','Home','','inherit','closed','closed','','2-revision-v1','','','2020-03-19 22:49:20','2020-03-19 22:49:20','',2,'http://localhost:10016/2-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (83,2,'2020-03-19 22:56:56','2020-03-19 22:56:56','','Home','','inherit','closed','closed','','2-revision-v1','','','2020-03-19 22:56:56','2020-03-19 22:56:56','',2,'http://localhost:10016/2-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (84,2,'2020-03-19 23:13:42','2020-03-19 23:13:42','','Home','','inherit','closed','closed','','2-revision-v1','','','2020-03-19 23:13:42','2020-03-19 23:13:42','',2,'http://localhost:10016/2-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (90,2,'2020-03-19 23:19:29','2020-03-19 23:19:29','','Home','','inherit','closed','closed','','2-revision-v1','','','2020-03-19 23:19:29','2020-03-19 23:19:29','',2,'http://localhost:10016/2-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (91,2,'2020-03-20 00:13:03','2020-03-20 00:13:03','','Home','','inherit','closed','closed','','2-revision-v1','','','2020-03-20 00:13:03','2020-03-20 00:13:03','',2,'http://localhost:10016/2-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (92,2,'2020-03-20 00:42:58','2020-03-20 00:42:58','','Home','','inherit','closed','closed','','2-revision-v1','','','2020-03-20 00:42:58','2020-03-20 00:42:58','',2,'http://localhost:10016/2-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (93,2,'2020-03-20 00:51:11','2020-03-20 00:51:11','','Home','','inherit','closed','closed','','2-revision-v1','','','2020-03-20 00:51:11','2020-03-20 00:51:11','',2,'http://localhost:10016/2-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (94,2,'2020-03-20 00:54:41','2020-03-20 00:54:41','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"20\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}','Image #1','image_1','publish','closed','closed','','field_5e74143a7bef0','','','2020-03-20 00:55:36','2020-03-20 00:55:36','',72,'http://localhost:10016/?post_type=acf-field&#038;p=94',0,'acf-field','',0);
INSERT INTO `wp_posts` VALUES (95,2,'2020-03-20 00:54:55','2020-03-20 00:54:55','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}','Image #2','image_2','publish','closed','closed','','field_5e74145c4fc01','','','2020-03-20 00:54:55','2020-03-20 00:54:55','',72,'http://localhost:10016/?post_type=acf-field&p=95',1,'acf-field','',0);
INSERT INTO `wp_posts` VALUES (96,2,'2020-03-20 00:54:55','2020-03-20 00:54:55','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}','Image #3','image_3','publish','closed','closed','','field_5e74145c4fc00','','','2020-03-20 01:09:26','2020-03-20 01:09:26','',72,'http://localhost:10016/?post_type=acf-field&#038;p=96',2,'acf-field','',0);
INSERT INTO `wp_posts` VALUES (97,2,'2020-03-20 00:54:55','2020-03-20 00:54:55','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}','Image #4','image_4','publish','closed','closed','','field_5e74145b4fbff','','','2020-03-20 01:09:26','2020-03-20 01:09:26','',72,'http://localhost:10016/?post_type=acf-field&#038;p=97',3,'acf-field','',0);
INSERT INTO `wp_posts` VALUES (98,2,'2020-03-20 00:54:55','2020-03-20 00:54:55','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}','Image #5','image_5','publish','closed','closed','','field_5e7414594fbfe','','','2020-03-20 01:09:26','2020-03-20 01:09:26','',72,'http://localhost:10016/?post_type=acf-field&#038;p=98',4,'acf-field','',0);
INSERT INTO `wp_posts` VALUES (99,2,'2020-03-20 00:56:26','2020-03-20 00:56:26','','Home','','inherit','closed','closed','','2-revision-v1','','','2020-03-20 00:56:26','2020-03-20 00:56:26','',2,'http://localhost:10016/2-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (100,2,'2020-03-20 01:09:43','2020-03-20 01:09:43','','Home','','inherit','closed','closed','','2-revision-v1','','','2020-03-20 01:09:43','2020-03-20 01:09:43','',2,'http://localhost:10016/2-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (101,2,'2020-03-20 02:17:16','2020-03-20 02:17:16','','Home','','inherit','closed','closed','','2-revision-v1','','','2020-03-20 02:17:16','2020-03-20 02:17:16','',2,'http://localhost:10016/2-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (105,2,'2020-03-23 00:12:36','2020-03-23 00:12:36','','olympics','','inherit','open','closed','','olympics','','','2020-03-23 00:12:36','2020-03-23 00:12:36','',0,'http://localhost:10016/wp-content/uploads/2020/03/olympics.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (107,2,'2020-01-23 16:36:02','2020-01-23 16:36:02','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Unified Korean Hockey Team Showed Impact of Sport on Global Issues','','publish','closed','closed','','women-in-sports','','','2020-03-26 03:34:34','2020-03-26 03:34:34','',0,'http://localhost:10016/?post_type=issues&#038;p=107',0,'issues','',0);
INSERT INTO `wp_posts` VALUES (108,2,'2020-03-23 16:35:46','2020-03-23 16:35:46','','issues1','','inherit','open','closed','','issues1','','','2020-03-23 16:35:46','2020-03-23 16:35:46','',107,'http://localhost:10016/wp-content/uploads/2020/03/issues1.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (109,2,'2020-03-23 16:35:46','2020-03-23 16:35:46','','issues2','','inherit','open','closed','','issues2','','','2020-03-23 16:35:46','2020-03-23 16:35:46','',107,'http://localhost:10016/wp-content/uploads/2020/03/issues2.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (110,2,'2020-03-23 16:35:46','2020-03-23 16:35:46','','issues3','','inherit','open','closed','','issues3','','','2020-03-23 16:35:46','2020-03-23 16:35:46','',107,'http://localhost:10016/wp-content/uploads/2020/03/issues3.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (111,2,'2020-03-23 16:35:46','2020-03-23 16:35:46','','issues4','','inherit','open','closed','','issues4','','','2020-03-23 16:35:46','2020-03-23 16:35:46','',107,'http://localhost:10016/wp-content/uploads/2020/03/issues4.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (112,2,'2020-03-23 16:35:46','2020-03-23 16:35:46','','issues5','','inherit','open','closed','','issues5','','','2020-03-23 16:35:46','2020-03-23 16:35:46','',107,'http://localhost:10016/wp-content/uploads/2020/03/issues5.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (113,2,'2020-03-23 16:35:47','2020-03-23 16:35:47','','issues6','','inherit','open','closed','','issues6','','','2020-03-23 16:35:47','2020-03-23 16:35:47','',107,'http://localhost:10016/wp-content/uploads/2020/03/issues6.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (115,2,'2020-03-24 22:21:44','2020-03-24 22:21:44','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu vehicula augue, ac gravida dolor. Pellentesque ultrices augue diam, eget bibendum mi efficitur vel. Curabitur ex risus, euismod eu convallis non, venenatis a diam. Vestibulum non feugiat justo, eget dapibus nulla. Quisque interdum metus non elit elementum, non dignissim turpis rhoncus. Donec tincidunt, sapien at semper ornare, odio dolor auctor ipsum, quis vestibulum ligula turpis et lorem. Fusce semper placerat enim quis dapibus. Proin felis nunc, tincidunt eu tempor et, dictum eu arcu. Vivamus sodales ullamcorper lorem ut venenatis.\n\nInteger volutpat euismod odio vel dictum. Aenean felis enim, venenatis in sagittis et, feugiat eu erat. Nulla interdum ex ac faucibus rutrum. Nullam vehicula consequat orci, nec tincidunt dui dictum nec. Sed cursus in enim eu finibus. Duis id eros eu massa pretium maximus at id orci. Nulla condimentum sit amet eros vel hendrerit.\n\nQuisque fringilla quam eu hendrerit sodales. Nam quis elit ac lorem mattis mattis. Fusce ac accumsan ligula. Phasellus elementum sodales leo, in fringilla sapien euismod non. Etiam quis quam in quam pulvinar lobortis quis malesuada sem. Quisque mollis nisi quam, eu hendrerit nisi vehicula nec. Nulla venenatis sapien elit, a dignissim purus tristique vel. Aliquam sed turpis vitae elit eleifend placerat vitae quis eros. Vestibulum metus augue, pretium tempor ultrices non, ullamcorper id odio.\n\nAliquam erat volutpat. Etiam in neque malesuada, accumsan diam sed, consectetur est. Sed quis auctor nulla, non maximus nibh. Vivamus consequat maximus tincidunt. Etiam mollis ante a metus luctus vulputate. Sed rhoncus sed mauris eget auctor. Nam ultrices nunc mi, non ullamcorper dui blandit vel. Integer consequat viverra fermentum. Donec nec posuere eros. In commodo eu ante vel luctus. Nunc enim diam, gravida sed feugiat at, pharetra quis dolor.\n\nMorbi gravida sodales velit a aliquam. Etiam a consectetur lorem, non ultricies risus. Vivamus mollis sollicitudin mollis. Praesent sed tincidunt urna. Praesent ac lobortis neque, id tristique metus. Vivamus ipsum augue, sodales vitae ultrices a, accumsan ullamcorper lorem. Fusce augue ex, mollis ac facilisis ut, consectetur sit amet purus.','Mexico City Games Become Launch Pad for Athlete Doping Tests','','inherit','closed','closed','','12-autosave-v1','','','2020-03-24 22:21:44','2020-03-24 22:21:44','',12,'http://localhost:10016/12-autosave-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (116,2,'2020-03-25 00:47:20','2020-03-25 00:47:20','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:8:\"taxonomy\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"monthly-issues\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}','Monthly Issue Image','monthly-issue-image','publish','closed','closed','','group_5e7aa9ef562c4','','','2020-03-25 00:51:08','2020-03-25 00:51:08','',0,'http://localhost:10016/?post_type=acf-field-group&#038;p=116',0,'acf-field-group','',0);
INSERT INTO `wp_posts` VALUES (117,2,'2020-03-25 00:47:20','2020-03-25 00:47:20','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}','Monthly Issue Image','monthly_issue_image','publish','closed','closed','','field_5e7aa9f709819','','','2020-03-25 00:51:08','2020-03-25 00:51:08','',116,'http://localhost:10016/?post_type=acf-field&#038;p=117',0,'acf-field','',0);
INSERT INTO `wp_posts` VALUES (119,2,'2020-03-25 05:56:04','2020-03-25 05:56:04','','placeholder-banner','','inherit','open','closed','','placeholder-banner','','','2020-03-25 05:56:04','2020-03-25 05:56:04','',0,'http://localhost:10016/wp-content/uploads/2020/01/placeholder-banner.png',0,'attachment','image/png',0);
INSERT INTO `wp_posts` VALUES (120,2,'2020-03-25 20:34:47','2020-03-25 20:34:47','','tackling-mental-health','','inherit','open','closed','','tackling-mental-health','','','2020-03-25 20:34:47','2020-03-25 20:34:47','',17,'http://localhost:10016/wp-content/uploads/2020/03/tackling-mental-health.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (123,2,'2020-03-26 03:33:58','2020-03-26 03:33:58','','korean','','inherit','open','closed','','korean','','','2020-03-26 03:33:58','2020-03-26 03:33:58','',107,'http://localhost:10016/wp-content/uploads/2020/01/korean.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (124,2,'2020-03-26 03:34:18','2020-03-26 03:34:18','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Unified Korean Hockey Team Showed Impact of Sport on Global Issues','','inherit','closed','closed','','107-autosave-v1','','','2020-03-26 03:34:18','2020-03-26 03:34:18','',107,'http://localhost:10016/107-autosave-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (125,2,'2020-03-26 04:09:42','2020-03-26 04:09:42','','placeholder-banner','','inherit','open','closed','','placeholder-banner-2','','','2020-03-26 04:09:42','2020-03-26 04:09:42','',0,'http://localhost:10016/wp-content/uploads/2020/03/placeholder-banner.png',0,'attachment','image/png',0);
INSERT INTO `wp_posts` VALUES (126,2,'2019-05-26 17:19:06','2019-05-26 17:19:06','Nunc magna lacus, maximus gravida placerat ac, eleifend sed lectus. Proin rhoncus nunc id dolor scelerisque pellentesque. Nulla ullamcorper neque sit amet quam efficitur eleifend. Nulla vitae nibh laoreet, consequat odio id, facilisis urna. Nam pellentesque aliquam dictum. Quisque convallis mollis ante, eget cursus dolor pharetra at. ','Cras euismod non enim','','publish','closed','closed','','cras-euismod-non-enim','','','2020-03-26 17:33:28','2020-03-26 17:33:28','',0,'http://localhost:10016/?post_type=issues&#038;p=126',0,'issues','',0);
INSERT INTO `wp_posts` VALUES (127,2,'2020-03-26 17:27:50','2020-03-26 17:27:50','Id diam maecenas ultricies mi. Euismod lacinia at quis risus sed vulputate odio. Ac placerat vestibulum lectus mauris ultrices eros in cursus turpis. Fermentum dui faucibus in ornare quam viverra orci sagittis eu. Ultrices sagittis orci a scelerisque purus semper. Dui nunc mattis enim ut tellus elementum sagittis. Fusce id velit ut tortor pretium viverra suspendisse potenti. Volutpat sed cras ornare arcu dui. Tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras. Tincidunt vitae semper quis lectus nulla at volutpat diam. Posuere lorem ipsum dolor sit amet consectetur.','Music Does More than Hype Athletes, It Helps Prep the Brain for Action','','publish','open','open','','music-does-more-than-hype-athletes-it-helps-prep-the-brain-for-action','','','2020-03-26 17:28:32','2020-03-26 17:28:32','',0,'http://localhost:10016/?p=127',0,'post','',0);
INSERT INTO `wp_posts` VALUES (128,2,'2020-03-26 17:27:50','2020-03-26 17:27:50','','Music Does More than Hype Athletes, It Helps Prep the Brain for Action','','inherit','closed','closed','','127-revision-v1','','','2020-03-26 17:27:50','2020-03-26 17:27:50','',127,'http://localhost:10016/127-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (129,2,'2020-03-26 17:28:32','2020-03-26 17:28:32','Id diam maecenas ultricies mi. Euismod lacinia at quis risus sed vulputate odio. Ac placerat vestibulum lectus mauris ultrices eros in cursus turpis. Fermentum dui faucibus in ornare quam viverra orci sagittis eu. Ultrices sagittis orci a scelerisque purus semper. Dui nunc mattis enim ut tellus elementum sagittis. Fusce id velit ut tortor pretium viverra suspendisse potenti. Volutpat sed cras ornare arcu dui. Tellus pellentesque eu tincidunt tortor aliquam nulla facilisi cras. Tincidunt vitae semper quis lectus nulla at volutpat diam. Posuere lorem ipsum dolor sit amet consectetur.','Music Does More than Hype Athletes, It Helps Prep the Brain for Action','','inherit','closed','closed','','127-revision-v1','','','2020-03-26 17:28:32','2020-03-26 17:28:32','',127,'http://localhost:10016/127-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (130,2,'2019-04-26 17:29:52','2019-04-26 17:29:52','Phasellus non tellus nec ligula mollis ultricies. Etiam porttitor laoreet lorem vel euismod. Etiam iaculis erat semper, tristique lacus eget, sodales lectus. Phasellus vitae elementum lacus. Nunc elementum id nisl ut accumsan. Nulla eget massa vel libero tempus pellentesque at et velit. Curabitur vitae iaculis sem. Quisque in vulputate lectus, non placerat ex. ','Nulla ac faucibus ipsum aliquam aliquet tellus eget','','publish','closed','closed','','nulla-ac-faucibus-ipsum-aliquam-aliquet-tellus-eget','','','2020-03-26 17:30:27','2020-03-26 17:30:27','',0,'http://localhost:10016/?post_type=issues&#038;p=130',0,'issues','',0);
INSERT INTO `wp_posts` VALUES (131,2,'2019-03-26 17:32:19','2019-03-26 17:32:19','Donec ac sodales lacus. Ut faucibus mi id nibh venenatis, in aliquet tellus iaculis. Fusce rhoncus pretium velit, eu porttitor magna tristique nec. Quisque at ligula auctor, viverra felis sed, elementum orci. Donec non tempus mauris. Nulla posuere nisi a nisi pellentesque gravida. Nulla mollis, enim molestie elementum iaculis, ante leo ultrices metus, sit amet tristique felis lectus quis neque. Praesent in augue eu nibh sodales auctor ut auctor turpis. Cras mattis urna eu quam commodo, vel dictum diam hendrerit. Proin vel imperdiet arcu.\r\n\r\nDonec pellentesque lacus at scelerisque bibendum. Quisque vel pharetra erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent libero arcu, sollicitudin nec arcu nec, scelerisque luctus erat. Donec laoreet diam ac sem posuere, in egestas erat tempus. Vivamus vitae purus lectus. Vestibulum vestibulum nunc in augue gravida, mattis interdum eros laoreet. Fusce ut sodales odio. Integer ac dolor fermentum, mattis enim nec, laoreet odio. Ut eget vulputate risus, ut mattis tellus. Proin euismod condimentum ultrices.\r\n\r\nPhasellus non tellus nec ligula mollis ultricies. Etiam porttitor laoreet lorem vel euismod. Etiam iaculis erat semper, tristique lacus eget, sodales lectus. Phasellus vitae elementum lacus. Nunc elementum id nisl ut accumsan. Nulla eget massa vel libero tempus pellentesque at et velit. Curabitur vitae iaculis sem. Quisque in vulputate lectus, non placerat ex. Cras euismod non enim a lacinia. In vestibulum urna quis enim maximus egestas. Mauris volutpat pharetra nibh varius condimentum.','Proin rhoncus nunc id dolor scelerisque pellentesque','','publish','closed','closed','','proin-rhoncus-nunc-id-dolor-scelerisque-pellentesque','','','2020-03-26 17:33:17','2020-03-26 17:33:17','',0,'http://localhost:10016/?post_type=issues&#038;p=131',0,'issues','',0);
INSERT INTO `wp_posts` VALUES (132,2,'2018-01-26 17:35:48','2018-01-26 17:35:48','Quisque convallis mollis ante, eget cursus dolor pharetra at. Fusce id mattis est. Curabitur sit amet sodales risus, sed sollicitudin enim. Mauris lacinia pellentesque justo quis tincidunt. Proin imperdiet turpis quam, hendrerit tincidunt ligula porttitor ut.\r\n\r\nDonec ac sodales lacus. Ut faucibus mi id nibh venenatis, in aliquet tellus iaculis. Fusce rhoncus pretium velit, eu porttitor magna tristique nec. Quisque at ligula auctor, viverra felis sed, elementum orci. Donec non tempus mauris. Nulla posuere nisi a nisi pellentesque gravida. Nulla mollis, enim molestie elementum iaculis, ante leo ultrices metus, sit amet tristique felis lectus quis neque. Praesent in augue eu nibh sodales auctor ut auctor turpis. Cras mattis urna eu quam commodo, vel dictum diam hendrerit. Proin vel imperdiet arcu.\r\n\r\nDonec pellentesque lacus at scelerisque bibendum. Quisque vel pharetra erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent libero arcu, sollicitudin nec arcu nec, scelerisque luctus erat. Donec laoreet diam ac sem posuere, in egestas erat tempus. Vivamus vitae purus lectus. Vestibulum vestibulum nunc in augue gravida, mattis interdum eros laoreet. Fusce ut sodales odio. Integer ac dolor fermentum, mattis enim nec, laoreet odio. Ut eget vulputate risus, ut mattis tellus. Proin euismod condimentum ultrices.\r\n\r\nPhasellus non tellus nec ligula mollis ultricies. Etiam porttitor laoreet lorem vel euismod. Etiam iaculis erat semper, tristique lacus eget, sodales lectus. Phasellus vitae elementum lacus. Nunc elementum id nisl ut accumsan. Nulla eget massa vel libero tempus pellentesque at et velit. Curabitur vitae iaculis sem. Quisque in vulputate lectus, non placerat ex. Cras euismod non enim a lacinia. In vestibulum urna quis enim maximus egestas. Mauris volutpat pharetra nibh varius condimentum.','Duis elit tellus semper nec mi ac sodales bibendum est','','publish','closed','closed','','duis-elit-tellus-semper-nec-mi-ac-sodales-bibendum-est','','','2020-03-26 18:00:30','2020-03-26 18:00:30','',0,'http://localhost:10016/?post_type=issues&#038;p=132',0,'issues','',0);
INSERT INTO `wp_posts` VALUES (133,2,'2018-02-26 17:37:14','2018-02-26 17:37:14','Sed nulla lectus, semper eget mauris quis, fringilla finibus dolor. In elementum, leo et imperdiet vestibulum, diam libero ultricies ex, eu ultrices justo velit a lacus. Suspendisse potenti. Praesent rhoncus elit non leo blandit pulvinar. Aliquam feugiat volutpat erat euismod pellentesque.\r\n\r\nNunc magna lacus, maximus gravida placerat ac, eleifend sed lectus. Proin rhoncus nunc id dolor scelerisque pellentesque. Nulla ullamcorper neque sit amet quam efficitur eleifend. Nulla vitae nibh laoreet, consequat odio id, facilisis urna. Nam pellentesque aliquam dictum. Quisque convallis mollis ante, eget cursus dolor pharetra at. Fusce id mattis est. Curabitur sit amet sodales risus, sed sollicitudin enim. Mauris lacinia pellentesque justo quis tincidunt. Proin imperdiet turpis quam, hendrerit tincidunt ligula porttitor ut.','Curabitur lobortis maximus metus at consequat quam ullamcorper id quisque feugiat luctus varius','','publish','closed','closed','','curabitur-lobortis-maximus-metus-at-consequat-quam-ullamcorper-id-quisque-feugiat-luctus-varius','','','2020-03-26 17:37:24','2020-03-26 17:37:24','',0,'http://localhost:10016/?post_type=issues&#038;p=133',0,'issues','',0);
INSERT INTO `wp_posts` VALUES (134,2,'2020-03-09 17:38:04','2020-03-09 17:38:04','Quisque at ligula auctor, viverra felis sed, elementum orci. Donec non tempus mauris. Nulla posuere nisi a nisi pellentesque gravida. Nulla mollis, enim molestie elementum iaculis, ante leo ultrices metus, sit amet tristique felis lectus quis neque. Praesent in augue eu nibh sodales auctor ut auctor turpis. Cras mattis urna eu quam commodo, vel dictum diam hendrerit. Proin vel imperdiet arcu.\r\n\r\nDonec pellentesque lacus at scelerisque bibendum. Quisque vel pharetra erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent libero arcu, sollicitudin nec arcu nec, scelerisque luctus erat. Donec laoreet diam ac sem posuere, in egestas erat tempus. Vivamus vitae purus lectus. Vestibulum vestibulum nunc in augue gravida, mattis interdum eros laoreet. Fusce ut sodales odio. Integer ac dolor fermentum, mattis enim nec, laoreet odio. Ut eget vulputate risus, ut mattis tellus. Proin euismod condimentum ultrices.\r\n\r\nPhasellus non tellus nec ligula mollis ultricies. Etiam porttitor laoreet lorem vel euismod. Etiam iaculis erat semper, tristique lacus eget, sodales lectus. Phasellus vitae elementum lacus. Nunc elementum id nisl ut accumsan. Nulla eget massa vel libero tempus pellentesque at et velit. Curabitur vitae iaculis sem. Quisque in vulputate lectus, non placerat ex. Cras euismod non enim a lacinia. In vestibulum urna quis enim maximus egestas. Mauris volutpat pharetra nibh varius condimentum.','Vivamus eget massa at arcu congue laoreet liquam','','publish','closed','closed','','vivamus-eget-massa-at-arcu-congue-laoreet-liquam','','','2020-03-26 18:18:51','2020-03-26 18:18:51','',0,'http://localhost:10016/?post_type=issues&#038;p=134',0,'issues','',0);
INSERT INTO `wp_posts` VALUES (135,2,'2020-03-09 00:34:30','2020-03-09 00:34:30','Quisque faucibus tincidunt tortor vitae tristique. Nullam nisl dui, facilisis nec leo vel, viverra consequat nisl. Praesent sed purus vitae turpis placerat venenatis. Donec nec magna sed metus finibus tempus. Etiam tincidunt justo non felis varius bibendum. Praesent bibendum vitae justo eu semper.\r\n\r\nVestibulum porttitor cursus purus a mattis. Duis scelerisque justo id ex interdum, vitae congue sem blandit. Mauris tempus quis quam at interdum. Praesent sit amet massa quis odio interdum tincidunt.\r\n<h3>Subhead for Article</h3>\r\nSed laoreet turpis sem, a semper eros varius ut. In pharetra leo libero, vitae eleifend libero feugiat ac. Maecenas risus velit, vestibulum vel erat non, viverra ullamcorper tortor. Suspendisse urna purus, sollicitudin in eros vel, gravida malesuada dolor. Nulla aliquam vestibulum odio, non sollicitudin nibh sollicitudin at. Suspendisse pretium velit tellus, pretium porttitor elit imperdiet nec. Maecenas eu nunc ligula. Proin id vulputate ante. Nulla venenatis eu ipsum eget ultricies. Quisque imperdiet tristique enim ac convallis. Vivamus scelerisque placerat suscipit. Phasellus lobortis euismod mauris, id sagittis massa molestie nec. Cras auctor imperdiet augue. Mauris iaculis ultrices laoreet.\r\n\r\n<img class=\"alignnone wp-image-137 size-full\" src=\"http://localhost:10016/wp-content/uploads/2020/03/single-article-img.png\" alt=\"\" width=\"697\" height=\"452\" />','Resarch Shows Importance of Sport for Refugee Children','','publish','closed','closed','','resarch-shows-importance-of-sport-for-refugee-children','','','2020-03-27 15:59:29','2020-03-27 15:59:29','',0,'http://localhost:10016/?post_type=issues&#038;p=135',0,'issues','',0);
INSERT INTO `wp_posts` VALUES (136,2,'2020-03-27 00:35:07','2020-03-27 00:35:07','','single-issue-banner','','inherit','open','closed','','single-issue-banner','','','2020-03-27 00:35:07','2020-03-27 00:35:07','',135,'http://localhost:10016/wp-content/uploads/2020/03/single-issue-banner.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (137,2,'2020-03-27 00:35:35','2020-03-27 00:35:35','','single-article-img','','inherit','open','closed','','single-article-img','','','2020-03-27 00:35:35','2020-03-27 00:35:35','',135,'http://localhost:10016/wp-content/uploads/2020/03/single-article-img.png',0,'attachment','image/png',0);
INSERT INTO `wp_posts` VALUES (139,2,'2020-03-27 00:45:22','2020-03-27 00:45:22','Quisque faucibus tincidunt tortor vitae tristique. Nullam nisl dui, facilisis nec leo vel, viverra consequat nisl. Praesent sed purus vitae turpis placerat venenatis. Donec nec magna sed metus finibus tempus. Etiam tincidunt justo non felis varius bibendum. Praesent bibendum vitae justo eu semper.\r\n\r\nVestibulum porttitor cursus purus a mattis. Duis scelerisque justo id ex interdum, vitae congue sem blandit. Mauris tempus quis quam at interdum. Praesent sit amet massa quis odio interdum tincidunt.\r\n<h3>Subhead for Article</h3>\r\nSed laoreet turpis sem, a semper eros varius ut. In pharetra leo libero, vitae eleifend libero feugiat ac. Maecenas risus velit, vestibulum vel erat non, viverra ullamcorper tortor. Suspendisse urna purus, sollicitudin in eros vel, gravida malesuada dolor. Nulla aliquam vestibulum odio, non sollicitudin nibh sollicitudin at. Suspendisse pretium velit tellus, pretium porttitor elit imperdiet nec. Maecenas eu nunc ligula. Proin id vulputate ante. Nulla venenatis eu ipsum eget ultricies. Quisque imperdiet tristique enim ac convallis. Vivamus scelerisque placerat suscipit. Phasellus lobortis euismod mauris, id sagittis massa molestie nec. Cras auctor imperdiet augue. Mauris iaculis ultrices laoreet.\r\n\r\n<img class=\"alignnone wp-image-137 size-full\" src=\"http://localhost:10016/wp-content/uploads/2020/03/single-article-img.png\" alt=\"\" width=\"697\" height=\"452\" />','Resarch Shows Importance of Sport for Refugee Children','','inherit','closed','closed','','135-autosave-v1','','','2020-03-27 00:45:22','2020-03-27 00:45:22','',135,'http://localhost:10016/135-autosave-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (142,2,'2020-03-27 23:22:07','2020-03-27 23:22:07','','About GSM','','inherit','closed','closed','','64-revision-v1','','','2020-03-27 23:22:07','2020-03-27 23:22:07','',64,'http://localhost:10016/64-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (143,2,'2020-03-27 23:27:18','2020-03-27 23:27:18','','about-gsm','','inherit','open','closed','','about-gsm','','','2020-03-27 23:27:18','2020-03-27 23:27:18','',64,'http://localhost:10016/wp-content/uploads/2020/03/about-gsm.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (144,2,'2020-03-27 23:47:20','2020-03-27 23:47:20','<h2>Amplifying the Power of Sport</h2>','About GSM','','inherit','closed','closed','','64-revision-v1','','','2020-03-27 23:47:20','2020-03-27 23:47:20','',64,'http://localhost:10016/64-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (146,2,'2020-03-27 23:53:57','2020-03-27 23:53:57','<h2>Amplifying the Power of Sport</h2>\r\n<ul>\r\n 	<li>By connecting peoople with research insights</li>\r\n 	<li>By examining the impact on society</li>\r\n 	<li>By providing context to sports globally</li>\r\n</ul>','About GSM','','inherit','closed','closed','','64-revision-v1','','','2020-03-27 23:53:57','2020-03-27 23:53:57','',64,'http://localhost:10016/64-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (147,2,'2020-03-27 23:54:24','2020-03-27 23:54:24','<h2>Amplifying the Power of Sport</h2>\r\n<ul>\r\n 	<li>By connecting peoople with research insights</li>\r\n 	<li>By examining the impact on society</li>\r\n 	<li>By providing context to sports globally</li>\r\n</ul>\r\n\r\n<hr />\r\n\r\n&nbsp;','About GSM','','inherit','closed','closed','','64-revision-v1','','','2020-03-27 23:54:24','2020-03-27 23:54:24','',64,'http://localhost:10016/64-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (148,2,'2020-03-27 23:55:20','2020-03-27 23:55:20','','logo-banner','','inherit','open','closed','','logo-banner','','','2020-03-27 23:55:20','2020-03-27 23:55:20','',64,'http://localhost:10016/wp-content/uploads/2020/03/logo-banner.png',0,'attachment','image/png',0);
INSERT INTO `wp_posts` VALUES (150,2,'2020-03-27 23:55:30','2020-03-27 23:55:30','','button','','inherit','open','closed','','button','','','2020-03-27 23:55:30','2020-03-27 23:55:30','',64,'http://localhost:10016/wp-content/uploads/2020/03/button.png',0,'attachment','image/png',0);
INSERT INTO `wp_posts` VALUES (151,2,'2020-03-27 23:57:55','2020-03-27 23:57:55','<h2>Amplifying the Power of Sport</h2>\r\n<ul>\r\n 	<li>By connecting peoople with research insights</li>\r\n 	<li>By examining the impact on society</li>\r\n 	<li>By providing context to sports globally</li>\r\n</ul>\r\n\r\n<hr />\r\n\r\nGlobalSport Matters is a multimedia content hub that delivers timely information on critical issues affecting sport and connects people with the knowledge they need. Praesent sed purus vitae turpis placerat venenatis. Donec nec magna sed metus finibus tempus. Etiam tincidunt justo non felis varius bibendum. Praesent bibendum vitae justo eu semper.\r\n\r\nVestibulum porttitor cursus purus a mattis. Duis scelerisque justo id ex interdum, vitae congue sem blandit. Mauris tempus quis quam at interdum. Praesent sit amet massa quis odio interdum tincidunt.\r\n\r\n<img class=\"alignnone size-full wp-image-150\" src=\"http://localhost:10016/wp-content/uploads/2020/03/button.png\" alt=\"\" width=\"197\" height=\"55\" />\r\n\r\nSed laoreet turpis sem, a semper eros varius ut. In pharetra leo libero, vitae eleifend libero feugiat ac. Maecenas risus velit, vestibulum vel erat non, viverra ullamcorper tortor. Suspendisse urna purus, sollicitudin in eros vel, gravida malesuada dolor. Nulla aliquam vestibulum odio, non sollicitudin nibh sollicitudin at. Suspendisse pretium velit tellus, pretium porttitor elit imperdiet nec. Maecenas eu nunc ligula. Proin id vulputate ante.\r\n\r\n<hr />\r\n\r\n<h2>Headline About Collaboration</h2>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sagittis mi dui, sit amet pellentesque ipsum posuere at. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin magna nisi, porta sit amet consectetur feugiat, laoreet nec mauris. Donec tempus finibus ipsum eu convallis.\r\n\r\n<img class=\"alignnone wp-image-148 size-full\" src=\"http://localhost:10016/wp-content/uploads/2020/03/logo-banner.png\" alt=\"\" width=\"696\" height=\"141\" />','About GSM','','inherit','closed','closed','','64-revision-v1','','','2020-03-27 23:57:55','2020-03-27 23:57:55','',64,'http://localhost:10016/64-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (152,2,'2020-03-27 23:58:36','2020-03-27 23:58:36','<h2>Amplifying the Power of Sport</h2>\r\n<ul>\r\n 	<li>By connecting peoople with research insights</li>\r\n 	<li>By examining the impact on society</li>\r\n 	<li>By providing context to sports globally</li>\r\n</ul>\r\n\r\n<hr />\r\n\r\nGlobalSport Matters is a multimedia content hub that delivers timely information on critical issues affecting sport and connects people with the knowledge they need. Praesent sed purus vitae turpis placerat venenatis. Donec nec magna sed metus finibus tempus. Etiam tincidunt justo non felis varius bibendum. Praesent bibendum vitae justo eu semper.\r\n\r\nVestibulum porttitor cursus purus a mattis. Duis scelerisque justo id ex interdum, vitae congue sem blandit. Mauris tempus quis quam at interdum. Praesent sit amet massa quis odio interdum tincidunt.\r\n\r\n<img class=\"alignnone size-full wp-image-150\" src=\"http://localhost:10016/wp-content/uploads/2020/03/button.png\" alt=\"\" width=\"197\" height=\"55\" />\r\n\r\nSed laoreet turpis sem, a semper eros varius ut. In pharetra leo libero, vitae eleifend libero feugiat ac. Maecenas risus velit, vestibulum vel erat non, viverra ullamcorper tortor. Suspendisse urna purus, sollicitudin in eros vel, gravida malesuada dolor. Nulla aliquam vestibulum odio, non sollicitudin nibh sollicitudin at. Suspendisse pretium velit tellus, pretium porttitor elit imperdiet nec. Maecenas eu nunc ligula. Proin id vulputate ante.\r\n\r\n<hr />\r\n\r\n<h2>Headline About Collaboration</h2>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sagittis mi dui, sit amet pellentesque ipsum posuere at. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin magna nisi, porta sit amet consectetur feugiat, laoreet nec mauris. Donec tempus finibus ipsum eu convallis.\r\n\r\n<img class=\"alignnone wp-image-148 size-full\" src=\"http://localhost:10016/wp-content/uploads/2020/03/logo-banner.png\" alt=\"\" width=\"696\" height=\"141\" />\r\n\r\n<hr />\r\n\r\n&nbsp;','About GSM','','inherit','closed','closed','','64-revision-v1','','','2020-03-27 23:58:36','2020-03-27 23:58:36','',64,'http://localhost:10016/64-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (153,2,'2020-04-01 02:24:11','2020-04-01 02:24:11','Lorem ipsum dolor','Thomas Lin','','publish','closed','closed','','thomas-lin','','','2020-04-01 02:24:11','2020-04-01 02:24:11','',0,'http://localhost:10016/?post_type=team&#038;p=153',0,'team','',0);
INSERT INTO `wp_posts` VALUES (154,2,'2020-04-01 02:23:54','2020-04-01 02:23:54','','8','','inherit','open','closed','','8','','','2020-04-01 02:23:54','2020-04-01 02:23:54','',153,'http://localhost:10016/wp-content/uploads/2020/04/8.png',0,'attachment','image/png',0);
INSERT INTO `wp_posts` VALUES (155,2,'2020-04-01 02:23:54','2020-04-01 02:23:54','','7','','inherit','open','closed','','7','','','2020-04-01 02:23:54','2020-04-01 02:23:54','',153,'http://localhost:10016/wp-content/uploads/2020/04/7.png',0,'attachment','image/png',0);
INSERT INTO `wp_posts` VALUES (156,2,'2020-04-01 02:23:54','2020-04-01 02:23:54','','5','','inherit','open','closed','','5','','','2020-04-01 02:23:54','2020-04-01 02:23:54','',153,'http://localhost:10016/wp-content/uploads/2020/04/5.png',0,'attachment','image/png',0);
INSERT INTO `wp_posts` VALUES (157,2,'2020-04-01 02:23:54','2020-04-01 02:23:54','','6','','inherit','open','closed','','6','','','2020-04-01 02:23:54','2020-04-01 02:23:54','',153,'http://localhost:10016/wp-content/uploads/2020/04/6.png',0,'attachment','image/png',0);
INSERT INTO `wp_posts` VALUES (158,2,'2020-04-01 02:23:54','2020-04-01 02:23:54','','1','','inherit','open','closed','','1','','','2020-04-01 02:23:54','2020-04-01 02:23:54','',153,'http://localhost:10016/wp-content/uploads/2020/04/1.png',0,'attachment','image/png',0);
INSERT INTO `wp_posts` VALUES (159,2,'2020-04-01 02:23:54','2020-04-01 02:23:54','','2','','inherit','open','closed','','2','','','2020-04-01 02:23:54','2020-04-01 02:23:54','',153,'http://localhost:10016/wp-content/uploads/2020/04/2.png',0,'attachment','image/png',0);
INSERT INTO `wp_posts` VALUES (160,2,'2020-04-01 02:23:55','2020-04-01 02:23:55','','4','','inherit','open','closed','','4','','','2020-04-01 02:23:55','2020-04-01 02:23:55','',153,'http://localhost:10016/wp-content/uploads/2020/04/4.png',0,'attachment','image/png',0);
INSERT INTO `wp_posts` VALUES (161,2,'2020-04-01 02:23:55','2020-04-01 02:23:55','','3','','inherit','open','closed','','3','','','2020-04-01 02:23:55','2020-04-01 02:23:55','',153,'http://localhost:10016/wp-content/uploads/2020/04/3.png',0,'attachment','image/png',0);
INSERT INTO `wp_posts` VALUES (162,2,'2020-04-01 02:24:45','2020-04-01 02:24:45','Lorem ipsum dolor','Natalie Wolchover','','publish','closed','closed','','natalie-wolchover','','','2020-04-01 02:24:45','2020-04-01 02:24:45','',0,'http://localhost:10016/?post_type=team&#038;p=162',0,'team','',0);
INSERT INTO `wp_posts` VALUES (163,2,'2020-04-01 02:25:15','2020-04-01 02:25:15','Lorem ipsum dolor','Michael Moyer','','publish','closed','closed','','michael-moyer','','','2020-04-01 02:25:15','2020-04-01 02:25:15','',0,'http://localhost:10016/?post_type=team&#038;p=163',0,'team','',0);
INSERT INTO `wp_posts` VALUES (164,2,'2020-04-01 02:25:50','2020-04-01 02:25:50','Lorem ipsum dolor','Olena Shmahalo','','publish','closed','closed','','olena-shmahalo','','','2020-04-01 02:25:50','2020-04-01 02:25:50','',0,'http://localhost:10016/?post_type=team&#038;p=164',0,'team','',0);
INSERT INTO `wp_posts` VALUES (165,2,'2020-04-01 02:26:18','2020-04-01 02:26:18','Lorem ipsum dolor','Kevin Hartnett','','publish','closed','closed','','kevin-hartnett','','','2020-04-01 02:26:18','2020-04-01 02:26:18','',0,'http://localhost:10016/?post_type=team&#038;p=165',0,'team','',0);
INSERT INTO `wp_posts` VALUES (166,2,'2020-04-01 02:26:45','2020-04-01 02:26:45','Lorem ipsum dolor','Caroline Lee','','publish','closed','closed','','caroline-lee','','','2020-04-01 02:26:45','2020-04-01 02:26:45','',0,'http://localhost:10016/?post_type=team&#038;p=166',0,'team','',0);
INSERT INTO `wp_posts` VALUES (167,2,'2020-04-01 02:27:14','2020-04-01 02:27:14','Lorem ipsum dolor','John Rennie','','publish','closed','closed','','john-rennie','','','2020-04-01 02:27:14','2020-04-01 02:27:14','',0,'http://localhost:10016/?post_type=team&#038;p=167',0,'team','',0);
INSERT INTO `wp_posts` VALUES (168,2,'2020-04-01 02:27:40','2020-04-01 02:27:40','Lorem ipsum dolor','Jordana Cepelewicz','','publish','closed','closed','','jordana-cepelewicz','','','2020-04-01 02:27:40','2020-04-01 02:27:40','',0,'http://localhost:10016/?post_type=team&#038;p=168',0,'team','',0);
/*!40000 ALTER TABLE `wp_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_term_relationships`
--

DROP TABLE IF EXISTS `wp_term_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_term_relationships`
--

LOCK TABLES `wp_term_relationships` WRITE;
/*!40000 ALTER TABLE `wp_term_relationships` DISABLE KEYS */;
INSERT INTO `wp_term_relationships` VALUES (1,3,0);
INSERT INTO `wp_term_relationships` VALUES (9,2,0);
INSERT INTO `wp_term_relationships` VALUES (10,2,0);
INSERT INTO `wp_term_relationships` VALUES (12,1,0);
INSERT INTO `wp_term_relationships` VALUES (12,15,0);
INSERT INTO `wp_term_relationships` VALUES (12,24,0);
INSERT INTO `wp_term_relationships` VALUES (12,36,0);
INSERT INTO `wp_term_relationships` VALUES (12,41,0);
INSERT INTO `wp_term_relationships` VALUES (15,14,0);
INSERT INTO `wp_term_relationships` VALUES (15,15,0);
INSERT INTO `wp_term_relationships` VALUES (15,24,0);
INSERT INTO `wp_term_relationships` VALUES (15,36,0);
INSERT INTO `wp_term_relationships` VALUES (15,41,0);
INSERT INTO `wp_term_relationships` VALUES (16,1,0);
INSERT INTO `wp_term_relationships` VALUES (16,15,0);
INSERT INTO `wp_term_relationships` VALUES (16,22,0);
INSERT INTO `wp_term_relationships` VALUES (16,36,0);
INSERT INTO `wp_term_relationships` VALUES (16,40,0);
INSERT INTO `wp_term_relationships` VALUES (17,1,0);
INSERT INTO `wp_term_relationships` VALUES (17,15,0);
INSERT INTO `wp_term_relationships` VALUES (17,23,0);
INSERT INTO `wp_term_relationships` VALUES (17,36,0);
INSERT INTO `wp_term_relationships` VALUES (17,42,0);
INSERT INTO `wp_term_relationships` VALUES (17,48,0);
INSERT INTO `wp_term_relationships` VALUES (17,49,0);
INSERT INTO `wp_term_relationships` VALUES (17,50,0);
INSERT INTO `wp_term_relationships` VALUES (17,51,0);
INSERT INTO `wp_term_relationships` VALUES (17,52,0);
INSERT INTO `wp_term_relationships` VALUES (17,53,0);
INSERT INTO `wp_term_relationships` VALUES (17,54,0);
INSERT INTO `wp_term_relationships` VALUES (17,55,0);
INSERT INTO `wp_term_relationships` VALUES (17,56,0);
INSERT INTO `wp_term_relationships` VALUES (17,57,0);
INSERT INTO `wp_term_relationships` VALUES (22,3,0);
INSERT INTO `wp_term_relationships` VALUES (24,3,0);
INSERT INTO `wp_term_relationships` VALUES (26,3,0);
INSERT INTO `wp_term_relationships` VALUES (26,45,0);
INSERT INTO `wp_term_relationships` VALUES (36,4,0);
INSERT INTO `wp_term_relationships` VALUES (41,4,0);
INSERT INTO `wp_term_relationships` VALUES (43,4,0);
INSERT INTO `wp_term_relationships` VALUES (45,1,0);
INSERT INTO `wp_term_relationships` VALUES (47,6,0);
INSERT INTO `wp_term_relationships` VALUES (50,7,0);
INSERT INTO `wp_term_relationships` VALUES (58,1,0);
INSERT INTO `wp_term_relationships` VALUES (59,1,0);
INSERT INTO `wp_term_relationships` VALUES (66,2,0);
INSERT INTO `wp_term_relationships` VALUES (107,1,0);
INSERT INTO `wp_term_relationships` VALUES (107,15,0);
INSERT INTO `wp_term_relationships` VALUES (107,22,0);
INSERT INTO `wp_term_relationships` VALUES (107,36,0);
INSERT INTO `wp_term_relationships` VALUES (107,40,0);
INSERT INTO `wp_term_relationships` VALUES (114,1,0);
INSERT INTO `wp_term_relationships` VALUES (114,15,0);
INSERT INTO `wp_term_relationships` VALUES (114,22,0);
INSERT INTO `wp_term_relationships` VALUES (126,36,0);
INSERT INTO `wp_term_relationships` VALUES (126,40,0);
INSERT INTO `wp_term_relationships` VALUES (127,45,0);
INSERT INTO `wp_term_relationships` VALUES (130,36,0);
INSERT INTO `wp_term_relationships` VALUES (130,41,0);
INSERT INTO `wp_term_relationships` VALUES (131,36,0);
INSERT INTO `wp_term_relationships` VALUES (131,46,0);
INSERT INTO `wp_term_relationships` VALUES (132,36,0);
INSERT INTO `wp_term_relationships` VALUES (132,47,0);
INSERT INTO `wp_term_relationships` VALUES (133,36,0);
INSERT INTO `wp_term_relationships` VALUES (133,40,0);
INSERT INTO `wp_term_relationships` VALUES (134,36,0);
INSERT INTO `wp_term_relationships` VALUES (134,40,0);
INSERT INTO `wp_term_relationships` VALUES (135,36,0);
INSERT INTO `wp_term_relationships` VALUES (135,40,0);
/*!40000 ALTER TABLE `wp_term_relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_term_taxonomy`
--

DROP TABLE IF EXISTS `wp_term_taxonomy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_term_taxonomy`
--

LOCK TABLES `wp_term_taxonomy` WRITE;
/*!40000 ALTER TABLE `wp_term_taxonomy` DISABLE KEYS */;
INSERT INTO `wp_term_taxonomy` VALUES (1,1,'category','',0,2);
INSERT INTO `wp_term_taxonomy` VALUES (2,2,'nav_menu','',0,3);
INSERT INTO `wp_term_taxonomy` VALUES (3,3,'category','The Insights category that displays on the homepage. ',0,4);
INSERT INTO `wp_term_taxonomy` VALUES (4,4,'category','',0,3);
INSERT INTO `wp_term_taxonomy` VALUES (6,6,'category','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (7,7,'category','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (14,14,'category','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (15,15,'issues-categories','The Olympics is a monthly theme that examines the impact of lorem ipsum dolor sit amet consectetur adi piscing elit sed malesuada vehicula venenatis.',0,6);
INSERT INTO `wp_term_taxonomy` VALUES (16,16,'issues-categories','Activism is a monthly theme that discusses topics regarding lorem ipsum dolor sit amet consectetur adi piscing elit sed malesuada vehicula venenatis.',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (18,18,'issues-categories','The Future is a monthly theme that discusses topics regarding lorem ipsum dolor sit amet consectetur adi piscing elit sed malesuada vehicula venenatis.',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (19,19,'issues-categories','Youth is a monthly theme that discusses topics regarding lorem ipsum dolor sit amet consectetur adi piscing elit sed malesuada vehicula venenatis.',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (20,20,'issues-categories','Title IX is a monthly theme that discusses topics regarding lorem ipsum dolor sit amet consectetur adi piscing elit sed malesuada vehicula venenatis.',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (21,21,'issues-article-categories','',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (22,22,'issues-article-categories','',0,3);
INSERT INTO `wp_term_taxonomy` VALUES (23,23,'issues-article-categories','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (24,24,'issues-article-categories','',0,2);
INSERT INTO `wp_term_taxonomy` VALUES (25,25,'issues-article-categories','',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (26,26,'issues-article-categories','',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (27,27,'issues-article-categories','',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (28,28,'issues-article-categories','',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (29,29,'issues-article-categories','',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (30,30,'issues-article-categories','',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (31,31,'issues-categories','Women in Sports is a monthly theme that discusses topics regarding lorem ipsum dolor sit amet consectetur adi piscing elit sed malesuada vehicula venenatis.',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (32,32,'issues-categories','Adaptive Sports is a monthly theme that discusses topics regarding lorem ipsum dolor sit amet consectetur adi piscing elit sed malesuada vehicula venenatis.',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (33,33,'monthly-issues','Activism is a monthly theme that discusses topics regarding lorem ipsum dolor sit amet consectetur adi piscing elit sed malesuada vehicula venenatis.',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (34,34,'monthly-issues','Adaptive Sports is a monthly theme that discusses topics regarding lorem ipsum dolor sit amet consectetur adi piscing elit sed malesuada vehicula venenatis.',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (35,35,'monthly-issues','The Future is a monthly theme that discusses topics regarding lorem ipsum dolor sit amet consectetur adi piscing elit sed malesuada vehicula venenatis.',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (36,36,'monthly-issues','The Olympics is a monthly theme that examines the impact of lorem ipsum dolor sit amet consectetur adi piscing elit sed malesuada vehicula venenatis.',0,12);
INSERT INTO `wp_term_taxonomy` VALUES (37,37,'monthly-issues','Title IX is a monthly theme that discusses topics regarding lorem ipsum dolor sit amet consectetur adi piscing elit sed malesuada vehicula venenatis.',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (38,38,'monthly-issues','Women in Sports is a monthly theme that discusses topics regarding lorem ipsum dolor sit amet consectetur adi piscing elit sed malesuada vehicula venenatis.',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (39,39,'monthly-issues','Youth Sport & Culture is a monthly theme that discusses topics regarding lorem ipsum dolor sit amet consectetur adi piscing elit sed malesuada vehicula venenatis.',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (40,40,'monthly-issues-category','',0,6);
INSERT INTO `wp_term_taxonomy` VALUES (41,41,'monthly-issues-category','',0,3);
INSERT INTO `wp_term_taxonomy` VALUES (42,42,'monthly-issues-category','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (43,43,'monthly-issues-category','',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (44,44,'monthly-issues-category','',0,0);
INSERT INTO `wp_term_taxonomy` VALUES (45,45,'category','The Culture article category.',0,2);
INSERT INTO `wp_term_taxonomy` VALUES (46,46,'monthly-issues-category','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (47,47,'monthly-issues-category','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (48,48,'tag','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (49,49,'tag','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (50,50,'tag','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (51,51,'tag','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (52,52,'tag','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (53,53,'issue_tags','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (54,54,'issue_tags','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (55,55,'issue_tags','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (56,56,'issue_tags','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (57,57,'issue_tags','',0,1);
/*!40000 ALTER TABLE `wp_term_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_termmeta`
--

DROP TABLE IF EXISTS `wp_termmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_termmeta`
--

LOCK TABLES `wp_termmeta` WRITE;
/*!40000 ALTER TABLE `wp_termmeta` DISABLE KEYS */;
INSERT INTO `wp_termmeta` VALUES (1,36,'image','105');
INSERT INTO `wp_termmeta` VALUES (2,36,'_image','field_5e7aa9f709819');
INSERT INTO `wp_termmeta` VALUES (3,36,'monthly_issue_image','105');
INSERT INTO `wp_termmeta` VALUES (4,36,'_monthly_issue_image','field_5e7aa9f709819');
INSERT INTO `wp_termmeta` VALUES (5,33,'monthly_issue_image','108');
INSERT INTO `wp_termmeta` VALUES (6,33,'_monthly_issue_image','field_5e7aa9f709819');
INSERT INTO `wp_termmeta` VALUES (7,34,'monthly_issue_image','113');
INSERT INTO `wp_termmeta` VALUES (8,34,'_monthly_issue_image','field_5e7aa9f709819');
INSERT INTO `wp_termmeta` VALUES (9,35,'monthly_issue_image','109');
INSERT INTO `wp_termmeta` VALUES (10,35,'_monthly_issue_image','field_5e7aa9f709819');
INSERT INTO `wp_termmeta` VALUES (11,37,'monthly_issue_image','110');
INSERT INTO `wp_termmeta` VALUES (12,37,'_monthly_issue_image','field_5e7aa9f709819');
INSERT INTO `wp_termmeta` VALUES (13,38,'monthly_issue_image','112');
INSERT INTO `wp_termmeta` VALUES (14,38,'_monthly_issue_image','field_5e7aa9f709819');
INSERT INTO `wp_termmeta` VALUES (15,39,'monthly_issue_image','111');
INSERT INTO `wp_termmeta` VALUES (16,39,'_monthly_issue_image','field_5e7aa9f709819');
/*!40000 ALTER TABLE `wp_termmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_terms`
--

LOCK TABLES `wp_terms` WRITE;
/*!40000 ALTER TABLE `wp_terms` DISABLE KEYS */;
INSERT INTO `wp_terms` VALUES (1,'Youth','youth',0);
INSERT INTO `wp_terms` VALUES (2,'Header Menu','header-menu',0);
INSERT INTO `wp_terms` VALUES (3,'Insights','insights',0);
INSERT INTO `wp_terms` VALUES (4,'Listen','listen',0);
INSERT INTO `wp_terms` VALUES (6,'Health','health',0);
INSERT INTO `wp_terms` VALUES (7,'Business','business',0);
INSERT INTO `wp_terms` VALUES (14,'Science','science',0);
INSERT INTO `wp_terms` VALUES (15,'The Olympics','olympics',0);
INSERT INTO `wp_terms` VALUES (16,'Activism','activism',0);
INSERT INTO `wp_terms` VALUES (18,'The Future','future',0);
INSERT INTO `wp_terms` VALUES (19,'Youth Sport and Culture','youth-sport-and-culture',0);
INSERT INTO `wp_terms` VALUES (20,'Title IX','title-ix',0);
INSERT INTO `wp_terms` VALUES (21,'Business','business',0);
INSERT INTO `wp_terms` VALUES (22,'Culture','culture',0);
INSERT INTO `wp_terms` VALUES (23,'Health','health',0);
INSERT INTO `wp_terms` VALUES (24,'Science','science',0);
INSERT INTO `wp_terms` VALUES (25,'Youth','youth',0);
INSERT INTO `wp_terms` VALUES (26,'Watch','watch',0);
INSERT INTO `wp_terms` VALUES (27,'Listen','listen',0);
INSERT INTO `wp_terms` VALUES (28,'Opinion','opinion',0);
INSERT INTO `wp_terms` VALUES (29,'Fiction','fiction',0);
INSERT INTO `wp_terms` VALUES (30,'Events','events',0);
INSERT INTO `wp_terms` VALUES (31,'Women in Sports','women-in-sports',0);
INSERT INTO `wp_terms` VALUES (32,'Adaptive Sports','adaptive-sports',0);
INSERT INTO `wp_terms` VALUES (33,'Activism','activism',0);
INSERT INTO `wp_terms` VALUES (34,'Adaptive Sports','adaptive-sports',0);
INSERT INTO `wp_terms` VALUES (35,'The Future','future',0);
INSERT INTO `wp_terms` VALUES (36,'The Olympics','olympics',0);
INSERT INTO `wp_terms` VALUES (37,'Title IX','title-ix',0);
INSERT INTO `wp_terms` VALUES (38,'Women in Sports','women-in-sports',0);
INSERT INTO `wp_terms` VALUES (39,'Youth Sport and Culture','youth-sport-and-culture',0);
INSERT INTO `wp_terms` VALUES (40,'Culture','culture',0);
INSERT INTO `wp_terms` VALUES (41,'Science','science',0);
INSERT INTO `wp_terms` VALUES (42,'Health','health',0);
INSERT INTO `wp_terms` VALUES (43,'Business','business',0);
INSERT INTO `wp_terms` VALUES (44,'Youth','youth',0);
INSERT INTO `wp_terms` VALUES (45,'Culture','culture',0);
INSERT INTO `wp_terms` VALUES (46,'Opinion','opinion',0);
INSERT INTO `wp_terms` VALUES (47,'Listen','listen',0);
INSERT INTO `wp_terms` VALUES (48,'Refugee','refugee',0);
INSERT INTO `wp_terms` VALUES (49,'Youth','youth',0);
INSERT INTO `wp_terms` VALUES (50,'Health','health',0);
INSERT INTO `wp_terms` VALUES (51,'Swimming','swimming',0);
INSERT INTO `wp_terms` VALUES (52,'Climbing','climbing',0);
INSERT INTO `wp_terms` VALUES (53,'Refugee','refugee',0);
INSERT INTO `wp_terms` VALUES (54,'Youth','youth',0);
INSERT INTO `wp_terms` VALUES (55,'Health','health',0);
INSERT INTO `wp_terms` VALUES (56,'Swimming','swimming',0);
INSERT INTO `wp_terms` VALUES (57,'Climbing','climbing',0);
/*!40000 ALTER TABLE `wp_terms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_usermeta`
--

DROP TABLE IF EXISTS `wp_usermeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_usermeta`
--

LOCK TABLES `wp_usermeta` WRITE;
/*!40000 ALTER TABLE `wp_usermeta` DISABLE KEYS */;
INSERT INTO `wp_usermeta` VALUES (1,1,'nickname','wpengine');
INSERT INTO `wp_usermeta` VALUES (2,1,'first_name','');
INSERT INTO `wp_usermeta` VALUES (3,1,'last_name','');
INSERT INTO `wp_usermeta` VALUES (4,1,'description','This is the \"wpengine\" admin user that our staff uses to gain access to your admin area to provide support and troubleshooting. It can only be accessed by a button in our secure log that auto generates a password and dumps that password after the staff member has logged in. We have taken extreme measures to ensure that our own user is not going to be misused to harm any of our clients sites.');
INSERT INTO `wp_usermeta` VALUES (5,1,'rich_editing','true');
INSERT INTO `wp_usermeta` VALUES (6,1,'syntax_highlighting','true');
INSERT INTO `wp_usermeta` VALUES (7,1,'comment_shortcuts','false');
INSERT INTO `wp_usermeta` VALUES (8,1,'admin_color','fresh');
INSERT INTO `wp_usermeta` VALUES (9,1,'use_ssl','0');
INSERT INTO `wp_usermeta` VALUES (10,1,'show_admin_bar_front','true');
INSERT INTO `wp_usermeta` VALUES (11,1,'locale','');
INSERT INTO `wp_usermeta` VALUES (12,1,'wp_capabilities','a:1:{s:13:\"administrator\";b:1;}');
INSERT INTO `wp_usermeta` VALUES (13,1,'wp_user_level','10');
INSERT INTO `wp_usermeta` VALUES (14,1,'dismissed_wp_pointers','');
INSERT INTO `wp_usermeta` VALUES (15,1,'show_welcome_panel','1');
INSERT INTO `wp_usermeta` VALUES (16,2,'nickname','Fervor Creative');
INSERT INTO `wp_usermeta` VALUES (17,2,'first_name','Fervor');
INSERT INTO `wp_usermeta` VALUES (18,2,'last_name','Creative');
INSERT INTO `wp_usermeta` VALUES (19,2,'description','<a href=\"#\">Marco Quezada</a> is a senior sports journalism major at Arizona State University');
INSERT INTO `wp_usermeta` VALUES (20,2,'rich_editing','true');
INSERT INTO `wp_usermeta` VALUES (21,2,'syntax_highlighting','false');
INSERT INTO `wp_usermeta` VALUES (22,2,'comment_shortcuts','false');
INSERT INTO `wp_usermeta` VALUES (23,2,'admin_color','fresh');
INSERT INTO `wp_usermeta` VALUES (24,2,'use_ssl','0');
INSERT INTO `wp_usermeta` VALUES (25,2,'show_admin_bar_front','false');
INSERT INTO `wp_usermeta` VALUES (26,2,'locale','');
INSERT INTO `wp_usermeta` VALUES (27,2,'wp_capabilities','a:1:{s:13:\"administrator\";b:1;}');
INSERT INTO `wp_usermeta` VALUES (28,2,'wp_user_level','10');
INSERT INTO `wp_usermeta` VALUES (30,2,'wp_dashboard_quick_press_last_post_id','4');
INSERT INTO `wp_usermeta` VALUES (31,2,'community-events-location','a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}');
INSERT INTO `wp_usermeta` VALUES (32,2,'session_tokens','a:2:{s:64:\"f56befb35a048df3e37af501ddc66fd01006ffc0e68ece9dae449821ff5d049a\";a:4:{s:10:\"expiration\";i:1585850239;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:117:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Safari/605.1.15\";s:5:\"login\";i:1585677439;}s:64:\"63ee97a8434828c5e5fb09ba58a279845747536a0af4777ffb5260efdc8d81aa\";a:4:{s:10:\"expiration\";i:1585940997;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36\";s:5:\"login\";i:1585768197;}}');
INSERT INTO `wp_usermeta` VALUES (34,2,'dismissed_wp_pointers','theme_editor_notice');
INSERT INTO `wp_usermeta` VALUES (35,2,'_yoast_wpseo_profile_updated','1585326451');
INSERT INTO `wp_usermeta` VALUES (36,1,'_yoast_wpseo_profile_updated','1583798488');
INSERT INTO `wp_usermeta` VALUES (37,2,'managenav-menuscolumnshidden','a:4:{i:0;s:11:\"link-target\";i:1;s:15:\"title-attribute\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}');
INSERT INTO `wp_usermeta` VALUES (38,2,'metaboxhidden_nav-menus','a:1:{i:0;s:12:\"add-post_tag\";}');
INSERT INTO `wp_usermeta` VALUES (39,2,'closedpostboxes_issues','a:0:{}');
INSERT INTO `wp_usermeta` VALUES (40,2,'metaboxhidden_issues','a:2:{i:0;s:11:\"postexcerpt\";i:1;s:7:\"slugdiv\";}');
INSERT INTO `wp_usermeta` VALUES (41,2,'wp_user-settings','editor=tinymce&libraryContent=browse&hidetb=1');
INSERT INTO `wp_usermeta` VALUES (42,2,'wp_user-settings-time','1585353259');
INSERT INTO `wp_usermeta` VALUES (43,2,'wpseo_title','');
INSERT INTO `wp_usermeta` VALUES (44,2,'wpseo_metadesc','');
INSERT INTO `wp_usermeta` VALUES (45,2,'wpseo_noindex_author','');
INSERT INTO `wp_usermeta` VALUES (46,2,'wpseo_content_analysis_disable','');
INSERT INTO `wp_usermeta` VALUES (47,2,'wpseo_keyword_analysis_disable','');
INSERT INTO `wp_usermeta` VALUES (48,2,'facebook','');
INSERT INTO `wp_usermeta` VALUES (49,2,'instagram','');
INSERT INTO `wp_usermeta` VALUES (50,2,'linkedin','');
INSERT INTO `wp_usermeta` VALUES (51,2,'myspace','');
INSERT INTO `wp_usermeta` VALUES (52,2,'pinterest','');
INSERT INTO `wp_usermeta` VALUES (53,2,'soundcloud','');
INSERT INTO `wp_usermeta` VALUES (54,2,'tumblr','');
INSERT INTO `wp_usermeta` VALUES (55,2,'twitter','');
INSERT INTO `wp_usermeta` VALUES (56,2,'youtube','');
INSERT INTO `wp_usermeta` VALUES (57,2,'wikipedia','');
INSERT INTO `wp_usermeta` VALUES (58,2,'nav_menu_recently_edited','2');
INSERT INTO `wp_usermeta` VALUES (59,2,'meta-box-order_page','a:4:{s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:36:\"submitdiv,pageparentdiv,postimagediv\";s:6:\"normal\";s:105:\"acf-group_5e73f064451c1,wpseo_meta,acf-group_5e73f1c29ff68,commentstatusdiv,commentsdiv,slugdiv,authordiv\";s:8:\"advanced\";s:0:\"\";}');
INSERT INTO `wp_usermeta` VALUES (60,2,'screen_layout_page','2');
INSERT INTO `wp_usermeta` VALUES (61,2,'closedpostboxes_acf-field-group','a:0:{}');
INSERT INTO `wp_usermeta` VALUES (62,2,'metaboxhidden_acf-field-group','a:1:{i:0;s:7:\"slugdiv\";}');
INSERT INTO `wp_usermeta` VALUES (65,2,'meta-box-order_issues','a:4:{s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:67:\"submitdiv,monthly-issuesdiv,monthly-issues-categorydiv,postimagediv\";s:6:\"normal\";s:19:\"postexcerpt,slugdiv\";s:8:\"advanced\";s:0:\"\";}');
INSERT INTO `wp_usermeta` VALUES (66,2,'screen_layout_issues','2');
INSERT INTO `wp_usermeta` VALUES (68,2,'wp_yoast_notifications','a:1:{i:0;a:2:{s:7:\"message\";s:219:\"<strong>Huge SEO Issue: You\'re blocking access to robots.</strong> You must <a href=\"http://localhost:10016/wp-admin/options-reading.php\">go to your Reading Settings</a> and uncheck the box for Search Engine Visibility.\";s:7:\"options\";a:10:{s:4:\"type\";s:5:\"error\";s:2:\"id\";s:32:\"wpseo-dismiss-blog-public-notice\";s:4:\"user\";O:7:\"WP_User\":8:{s:4:\"data\";O:8:\"stdClass\":10:{s:2:\"ID\";s:1:\"2\";s:10:\"user_login\";s:12:\"fervor_admin\";s:9:\"user_pass\";s:34:\"$P$BlTqhxutKW9ei063AnqGpNLmR6be1I1\";s:13:\"user_nicename\";s:15:\"fervor-creative\";s:10:\"user_email\";s:22:\"rob@fervorcreative.com\";s:8:\"user_url\";s:26:\"https://fervorcreative.com\";s:15:\"user_registered\";s:19:\"2020-03-06 21:15:37\";s:19:\"user_activation_key\";s:0:\"\";s:11:\"user_status\";s:1:\"0\";s:12:\"display_name\";s:15:\"Fervor Creative\";}s:2:\"ID\";i:2;s:4:\"caps\";a:1:{s:13:\"administrator\";b:1;}s:7:\"cap_key\";s:15:\"wp_capabilities\";s:5:\"roles\";a:1:{i:0;s:13:\"administrator\";}s:7:\"allcaps\";a:63:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:20:\"wpseo_manage_options\";b:1;s:13:\"administrator\";b:1;}s:6:\"filter\";N;s:16:\"\0WP_User\0site_id\";i:1;}s:5:\"nonce\";N;s:8:\"priority\";i:1;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";s:14:\"yoast_branding\";b:0;}}}');
/*!40000 ALTER TABLE `wp_usermeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_users`
--

DROP TABLE IF EXISTS `wp_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_users`
--

LOCK TABLES `wp_users` WRITE;
/*!40000 ALTER TABLE `wp_users` DISABLE KEYS */;
INSERT INTO `wp_users` VALUES (1,'wpengine','hn-iaC9ahgRqEaL6dKEPbcNnecIDsp2XLaLepJ9VRzDDXkNQFNPLSel7J1qNccPM','wpengine','bitbucket@wpengine.com','http://wpengine.com','2020-03-06 21:07:49','',0,'wpengine');
INSERT INTO `wp_users` VALUES (2,'fervor_admin','$P$BlTqhxutKW9ei063AnqGpNLmR6be1I1','fervor-creative','rob@fervorcreative.com','https://fervorcreative.com','2020-03-06 21:15:37','',0,'Fervor Creative');
/*!40000 ALTER TABLE `wp_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_yoast_seo_links`
--

DROP TABLE IF EXISTS `wp_yoast_seo_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_yoast_seo_links` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_id` bigint(20) unsigned NOT NULL,
  `target_post_id` bigint(20) unsigned NOT NULL,
  `type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `link_direction` (`post_id`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_yoast_seo_links`
--

LOCK TABLES `wp_yoast_seo_links` WRITE;
/*!40000 ALTER TABLE `wp_yoast_seo_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_yoast_seo_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_yoast_seo_meta`
--

DROP TABLE IF EXISTS `wp_yoast_seo_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_yoast_seo_meta` (
  `object_id` bigint(20) unsigned NOT NULL,
  `internal_link_count` int(10) unsigned DEFAULT NULL,
  `incoming_link_count` int(10) unsigned DEFAULT NULL,
  UNIQUE KEY `object_id` (`object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_yoast_seo_meta`
--

LOCK TABLES `wp_yoast_seo_meta` WRITE;
/*!40000 ALTER TABLE `wp_yoast_seo_meta` DISABLE KEYS */;
INSERT INTO `wp_yoast_seo_meta` VALUES (1,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (2,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (3,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (4,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (7,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (8,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (11,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (12,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (13,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (14,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (15,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (16,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (17,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (18,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (20,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (22,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (24,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (26,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (36,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (41,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (43,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (45,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (47,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (50,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (52,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (53,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (54,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (55,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (56,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (57,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (58,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (59,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (64,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (67,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (68,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (69,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (74,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (82,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (85,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (86,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (87,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (88,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (89,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (102,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (103,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (104,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (106,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (107,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (114,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (118,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (121,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (122,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (126,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (127,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (130,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (131,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (132,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (133,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (134,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (135,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (138,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (140,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (141,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (145,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (149,0,0);
INSERT INTO `wp_yoast_seo_meta` VALUES (169,0,0);
/*!40000 ALTER TABLE `wp_yoast_seo_meta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-02  9:09:37
