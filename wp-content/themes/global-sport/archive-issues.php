<?php 

// This template displays the Issues Custom Post Type archive.

get_header(); 

?>

<section id="headline" class="section issues-archive">
  <div class="grid-container full">
    <div class="grid-x align-center text-center">
      <div class="cell headline__container">
        <div class="headline__content">
          <h1 class="headline__title">Issues</h2>
          <h3 class="headline__subtitle">Every month Global Sport Matters examines the impact of lorem ipsum dolor sit amet consectetur adi piscing elit sed malesuada vehicula venenatis.</h3>
        </div> <!-- .headline__content -->
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section> <!-- .section -->

<section id="featured-issue" class="section issues-archive">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="cell">
        <?php 

            $taxonomy = 'monthly-issues';
            $current_monthly_issue = 'olympics';          
            $terms = get_terms($taxonomy);
            
            foreach ($terms as $term):
              if ($term->slug == $current_monthly_issue):
                
                $monthly_issue_img = get_field('monthly_issue_image', $term);
                echo '<div class="issues_box">';
                  echo '<img class="issues__box__image" src="'.$monthly_issue_img['url'].'" alt="Global Sport Matters Current Issue" />';
                  echo '<h2 class="issues__box__title">This month: ' . $term->name . '</h2>';
                echo '</div>';
              endif;
            endforeach;
        ?>
    
      </div> <!-- .cell --> 
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>

<section id="post-grid" class="section issues-archive">
  <div class="grid-container full">
  <ul class="grid-x grid-padding-x">
    <?php 

      // $featured_issues_args = array( 
      //   'post_type' => 'issues',
      //   'offset' => 1      
      // );

      // $featured_issues = new WP_Query($featured_issues_args);

      $taxonomy = 'monthly-issues';
      
      $terms = get_terms($taxonomy, array('hide_empty' => false));

      $numOfCols = 3;
      $rowCount = 0;
      $colWidth = 12 / $numOfCols;

      foreach ($terms as $term):
        if ($term->slug):
          $monthly_issue_img = get_field('monthly_issue_image', $term);

          if ($term->slug != $current_monthly_issue):
          
          echo '<li class="cell large-'.$colWidth.'">';
            echo '<a class="issues_link" href="'.get_term_link($term->slug, $taxonomy).'" title='.get_the_title().'">';
            echo '<div class="box">';
              echo '<div class="box__image">';
                echo '<img src="'.$monthly_issue_img['url'].'" alt="" />';
              echo '</div>';
              echo '<div class="box_inner">'; 
                echo '<h2 class="box__title">' . $term->name . '</h2>';
                echo '<div class="issues-content">' . $term->description . '</div>';
              echo '</div> <!-- .issues_box_inner -->';
            echo '</div> <!-- .issues_box -->';
            echo '</a>';
          echo '</li> <!-- .cell -->';

          endif;

      
        endif; 
      endforeach; ?>

    </ul>

    <div class="grid-x grid-padding-x align-center">
      <a class="issues-cta gsm-button" href="#">Load More</a>
    </div> <!-- .grid-x -->

  </div> <!-- .grid-container -->
  
</section>

<?php get_footer(); ?>




