<?php
/**
 * The front page template
 */

get_header(); ?>

    <section id="headline" class="section gsm-home">
      <div class="grid-container full">
        <div class="this__month"></div>
        <div class="grid-x text-right align-middle">
          <div class="cell small-auto medium-auto large-auto"></div> <!-- .cell --> 
          <div class="headline__container cell small-12 medium-6 large-6 text-left">
            <div class="headline__content">
              <h1 class="headline__title">Amplifying the<br/>power of sport</h2>
              <h3 class="headline__subtitle">By connecting people with research insights</h3>
            </div> <!-- .headline__content -->
          </div> <!-- .headline__container --> 
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>

    <?php 

    if (have_rows('sections')): while (have_rows('sections')): the_row();

      if (get_row_layout() == 'current_issue'):
        $bg_images = get_sub_field('background_images');
        shuffle($bg_images);  
      endif;
    endwhile; endif; ?>
     
    <section id="featured-issue" class="section gsm-home" style="background-image:url(<?= $bg_images[0]['url']; ?>)">
      <div class="grid-container full">
        <div class="grid-x align-justify">
          <div class="cell large-5 featured__left">
          
            <?php 

              $taxonomy = 'monthly-issues';
              $current_issue = 'olympics';          
              $terms = get_terms($taxonomy);

              foreach ($terms as $term):
                if ($term->slug == $current_issue):
                  
                  echo '<h2 class="issues-title"><a href="'.get_term_link($term->slug, $taxonomy).'">' . $term->name . '</a></h2>';
                  echo '<div class="issues-content"><a href="'.get_term_link($term->slug, $taxonomy).'">' . $term->description . '</a></div>';
                  echo '<a class="issues-cta" href="'.get_term_link($term->slug, $taxonomy).'">View Full Issue</a>';
                endif;
              endforeach;
            ?>
        
          </div> <!-- .cell --> 

          <div class="cell large-4 featured__right">
            
            <?php 

              $taxonomy1 = 'monthly-issues';
              $taxonomy2 = 'monthly-issues-category';

              $terms = get_terms(array($taxonomy1, $taxonomy2));
              $term_ids = wp_list_pluck( $terms, 'term_id' );

              //var_dump($term_ids);
              
              $args = array( 
                'post_type' => 'issues',
                'tax_query' => array(
                  'relation' => 'AND',
                  array(
                    'taxonomy' => $taxonomy1,
                    'field'    => 'term_id',
                    'terms'    => $term_ids,
                  ),
                  array(
                    'taxonomy' => $taxonomy2,
                    'field'    => 'term_id',
                    'terms'    => $term_ids,
                  ),
                ),
                'posts_per_page' => 3,     
              );
            
              $articles = new WP_Query($args);        
            
              if ($articles->have_posts()): 
                while($articles->have_posts()): 
                  $articles->the_post();
                  $featured_img_url = get_the_post_thumbnail_url();
                  $terms = get_the_terms($articles->post->ID, array($taxonomy2));
                  
                  echo '<a href="'.get_the_permalink().'">';
                  echo '<div class="issues_box">';
                    echo '<div class="issues__box__image"><img src="'.$featured_img_url.'" alt=""/></div>';
                    echo '<div class="issues_box_inner">'; 
                      echo '<div class="issues__box__category">' . $terms[0]->name . '</div>';
                      echo '<h2 class="issues__box__title">' . get_the_title() . '</h2>';
                    echo '</div>';
                  echo '</div>';
                  echo '</a>';
                endwhile;
              else:
                echo 'Sorry, there are currently no articles in this category.';
              endif;
              wp_reset_postdata();
            ?>
          
          </div> <!-- .cell -->
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>

    <section id="top-insights" class="section gsm-home">
      <div class="grid-container full">
        <h2 class="section-title">Top Insights</h2>
        <div class="grid-x grid-padding-x">
          <div class="cell small-12 medium-6 large-6 insights__left">
            <?php 

              $args = array( 
                'category_name' => 'insights',
                'posts_per_page' => 1,            
              );
            
              $articles = new WP_Query($args);
            
              if ($articles->have_posts()): 
                while($articles->have_posts()): 
                  $articles->the_post();
                  $featured_img_url = get_the_post_thumbnail_url();
                  echo '<div class="insights_box">';
                    echo '<div class="insights__box__image"><img src="'.$featured_img_url.'" alt=""/></div>';
                      echo '<div class="insights_box_inner">';
                      echo '<div class="insights__box__category">' . get_the_category()[0]->cat_name . '</div>';
                      echo '<h2 class="insights__box__title">' . get_the_title() . '</h2>';
                    echo '</div>';
                  echo '</div>';
                endwhile;
              else:
                echo 'Sorry, there are currently no articles in this category.';
              endif;
              wp_reset_postdata();
            ?>
          </div> <!-- .cell -->

          <div class="cell small-12 medium-6 large-6 insights__right">
            <div class="grid-container grid-container-inner full">
              <div class="grid-x grid-padding-x">
                <?php 

                  $args = array( 
                    'category_name' => 'insights',
                    'posts_per_page' => 2,
                    'offset' => 1            
                  );

                  $articles = new WP_Query($args);

                  if ($articles->have_posts()): 
                    while($articles->have_posts()): 
                      $articles->the_post();
                      $featured_img_url = get_the_post_thumbnail_url();
                      echo '<div class="cell small-12 medium-6 large-6">';
                        echo '<div class="insights_box">';
                          echo '<div class="insights__box__image"><img src="'.$featured_img_url.'" alt=""/></div>';
                          echo '<div class="insights_box_inner">';
                            echo '<div class="insights__box__category">' . get_the_category()[0]->cat_name . '</div>';
                            echo '<h2 class="insights__box__title">' . get_the_title() . '</h2>';
                          echo '</div> <!-- .insights_box_inner -->';
                        echo '</div> <!-- .insights_box -->';
                      echo '</div> <!-- .cell -->';
                    endwhile;
                  else:
                    echo 'Sorry, there are currently no articles in this category.';
                  endif;
                  wp_reset_postdata();
                ?>
          </div> <!-- .cell -->
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>

    <section id="post-grid" class="section gsm-home">
      <div class="grid-container full">
        <div class="grid-x grid-padding-x align-middle">
          <div class="cell small-12 medium-5 large-5 xlarge-5 flex-container align-middle">
            <div class="post__links">
              <ul>
                <?php 
                  $args = array( 
                    'category_name' => 'youth, health, business',
                    'posts_per_page' => 3,           
                  );

                  $articles = new WP_Query($args);

                  if ($articles->have_posts()): 
                    while($articles->have_posts()): 
                      $articles->the_post();
                      //$post_grid_img_url = get_the_post_thumbnail_url();
                      echo '<li class="'.strtolower(get_the_category()[0]->cat_name).'"><h6>'.get_the_category()[0]->cat_name.'</h6><a href="'.get_the_permalink().'">' . get_the_title() . '</a></li>';
                    endwhile;
                  endif;
                  wp_reset_postdata();
                ?>      
              </ul>
            </div> <!-- .video__links -->
          </div> <!-- .cell --> 

          <div class="cell small-12 medium-7 large-7 xlarge-7 align-right text-right">
            <?php 
              $args = array( 
                'category_name' => 'culture',
                'posts_per_page' => 1,            
              );

              $articles = new WP_Query($args);

              if ($articles->have_posts()): 
                while($articles->have_posts()): 
                  $articles->the_post();
                  $featured_img_url = get_the_post_thumbnail_url();
                  
                    echo '<div class="culture_box">';
                      echo '<div class="culture__box__image"><img src="'.$featured_img_url.'" alt=""/></div>';
                      echo '<div class="culture_box_inner">';
                        echo '<div class="culture__box__category">' . get_the_category()[0]->cat_name . '</div>';
                        echo '<h2 class="culture__box__title">' . get_the_title() . '</h2>';
                      echo '</div>';
                    echo '</div>';
                    
                endwhile;
              else:
                echo 'Sorry, there are currently no articles in this category.';
              endif;
              wp_reset_postdata();
            ?>
          </div>
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>

    <section id="post-quotebox" class="section gsm-home">
      <div class="grid-container full">
        <div class="grid-x grid-padding-x align-top">
          <div class="cell small-12 medium-7 large-7 xlarge-7 align-right text-right">
            <?php 
              $args = array( 
                'category_name' => 'health',
                'posts_per_page' => 1,            
              );

              $articles = new WP_Query($args);

              if ($articles->have_posts()): 
                while($articles->have_posts()): 
                  $articles->the_post();
                  $featured_img_url = get_the_post_thumbnail_url();
                  
                    echo '<div class="health_box">';
                      echo '<div class="health__box__image" style="background-image:url('.$featured_img_url.')">';
                      echo '<div class="health__box__category">' . get_the_category()[0]->cat_name . '</div>';
                      echo '<h2 class="health__box__title">' . get_the_title() . '</h2>';
                      echo '</div>';
                      echo '<div class="health_box_inner">';
                      echo '</div>';
                    echo '</div>';
                  
                endwhile;
              else:
                echo 'Sorry, there are currently no articles in this category.';
              endif;
              wp_reset_postdata();
            ?>
          </div> <!-- .cell -->
          <div class="cell small-12 medium-5 large-5 xlarge-5 quotebox__container">
            <div class="quotebox">
              <span class="quote">Informational tile card lorem ipsum dolor sit amet consect etur adipiscing elit.</span>
            </div> <!-- .post__links -->
          </div> <!-- .cell -->
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>

    <section id="watch-this" class="section">
      <div class="grid-container full">
      <h2 class="section-title">Watch This</h2>
        <div class="grid-x grid-padding-x align-middle">
          <div class="cell xlarge-8">
            <div class="video__holder">
              <img src="/wp-content/uploads/2020/03/video-placeholder.jpg" alt="" />
            </div> <!-- .video__holder -->
          </div> <!-- .cell --> 

          <div class="cell xlarge-3 copy">
            
            <p>Visual storytelling that examines the impact of lorem ipsum dolor sit amet consectetur adi piscing elit.</p>
           
            <div class="video__links">
              <ul>
                <li class="active">GSM mini documentary: Lauren’s Heart</li>
                <li>Changing the discussion about concussions</li>
                <li>Wrestling gave former Green Beret a second chance</li>
              </ul>
            </div> <!-- .video__links -->
          </div>
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>

    <section id="gregueria-quote" class="section" style="background-image:url(/wp-content/themes/global-sport/assets/images/gregueria-bg.jpg)">
      <div class="grid-container full">
        <div class="grid-x">
          <div class="cell small-12 medium-12 large-12">
            <h4 class="definition">Gregueria: Your philosophical thought of the day</h4>
            <h3 class="quote">A football carries the perpetual stitches of its last encounter.</h3>
          </div> 
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>

    <section id="listen-gsm" class="section" style="">
      <div class="grid-container full">
        <h2 class="section-title">Listen on GSM</h2>
        <div class="grid-x grid-padding-x">
          
            <?php 

              $args = array( 
                'category_name' => 'listen',
                'posts_per_page' => 3,            
              );

              $articles = new WP_Query($args);

              if ($articles->have_posts()): 
                while($articles->have_posts()): 
                  $articles->the_post();
                  $featured_img_url = get_the_post_thumbnail_url();
                  

                  echo '<div class="cell small-12 medium-4 large-4">';
                    echo '<div class="listen_box">';
                      echo '<div class="listen__box__image" style="background-image:url('.$featured_img_url.')">';
                      echo '<div class="listen__box__category">' . get_the_category()[0]->cat_name . '</div>';
                      echo '<h2 class="listen__box__title">' . get_the_title() . '</h2>';
                      echo '</div>';
                      echo '<div class="listen_box_inner">';
                      echo '</div>';
                    echo '</div>';
                  echo '</div>';
                endwhile;
              else:
                echo 'Sorry, there are currently no articles in this category.';
              endif;
              wp_reset_postdata();
              ?>

         
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>

    <section id="twitter-quote" class="section" style="background-image:url(/wp-content/themes/global-sport/assets/images/twitter-quote-bg.png)">
      <div class="grid-container full">
        <div class="grid-x">
          <div class="cell small-12 medium-4 large-4 twitter-left">
            <div class="account-info">
              <strong>Global Sport Matters</strong><br/>
              <span>@GlobalSportMtrs</span><br/>
              <span>Aug 30</span>
            </div> <!-- .account-info -->
          </div> <!-- .cell -->

          <div class="cell small-12 medium-8 large-8 twitter-right">
            <p class="twitter-quote">"I'm tired of hiding." Get ready to experience transgender runner Lauren's transition to being female and how running has played a role in her journey.  https://t.co/xbuxZ0fkNr</p>
          </div> <!-- .cell -->
        </div> <!-- .grid-x -->
      </div> <!-- .grid-container -->
    </section>

<?php get_footer(); ?>
