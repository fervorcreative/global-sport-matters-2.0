// Set up App object and jQuery

var App = App || {},
  $ = $ || jQuery;

App.navLinkOpen = function() {
  $(document).on('click', '.header-nav #access .menu li:not(.about)', function() {
    var menu_class = $(this).attr('class').split(' ')[0];
    if ($('html').hasClass('is-open')) {
      if (menu_class == 'issues') {
        $('.gsm-issues-menu').css({opacity: 1, visibility: 'visible', height: 'auto'});
        $('.gsm-sections-menu').css({opacity: 0, visibility: 'hidden', height: '0'});
      } else if (menu_class == 'sections') {
        $('.gsm-issues-menu').css({opacity: 0, visibility: 'hidden', height: '0'});
        $('.gsm-sections-menu').css({opacity: 1, visibility: 'visible', height: 'auto'});
      } 
    } else {
        $('body').css('overflow','hidden');
        $('html').toggleClass('is-open');
        if (menu_class == 'issues') {
          $('.gsm-issues-menu').css({opacity: 1, visibility: 'visible', height: 'auto'});
          $('.gsm-sections-menu').css({opacity: 0, visibility: 'hidden', height: '0'});
        } else if (menu_class == 'sections') {
          $('.gsm-issues-menu').css({opacity: 0, visibility: 'hidden', height: '0'});
          $('.gsm-sections-menu').css({opacity: 1, visibility: 'visible', height: 'auto'});
        } 
    }
  });
}

App.navLinkClose = function() {
  $(document).on('click', '.js-nav-toggle', function() {
    if ($('html').hasClass('is-open')) {
      $('html').toggleClass('is-open');
      $('body').removeAttr('style');
      $('.gsm-issues-menu, .gsm-sections-menu').css({visibility:'hidden', opacity:'0', height:'0'});
      
    } else {
      $('html').toggleClass('is-open');
    }
  });
}

$(document).ready(function() {
  App.navLinkOpen();
  App.navLinkClose();
});

// Instantiate functions when document has loaded most assets such as images
$(window).on('load', function() {
  
});
