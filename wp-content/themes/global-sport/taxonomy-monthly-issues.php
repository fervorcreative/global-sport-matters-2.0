<?php 

// This template displays the Monthly Issue being viewed

get_header(); 

?>

<section id="headline" class="section issues-archive">
  <div class="grid-container full">
    <div class="grid-x align-center text-center">
      <div class="cell headline__container">
        <div class="headline__content">
          <div class="monthly-issues-arrow"></div>
          <?php $term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy')); ?>
          <h1 class="headline__title"><?= $term->name; ?></h2>
          <h3 class="headline__subtitle"><?= $term->description; ?></h3>
        </div> <!-- .headline__content -->
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section> <!-- .section -->

<section id="featured-issue" class="section issues-archive">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="cell">
        <?php 

          $taxonomy1 = 'monthly-issues';
          $taxonomy2 = 'monthly-issues-category';

          $terms = get_terms(array($taxonomy1, $taxonomy2));
          $term_ids = wp_list_pluck( $terms, 'term_id' );

          //var_dump($term_ids);

          $args = array( 
            'post_type' => 'issues',
            'posts_per_page' => 1,
            'tax_query' => array(
              array(
                'taxonomy' => $taxonomy1,
                'field'    => 'term_id',
                'terms'    => $term_ids,
              ),
              array(
                'taxonomy' => $taxonomy2,
                'field'    => 'term_id',
                'terms'    => $term_ids,
              ),
            ),  
          );

          $articles = new WP_Query($args);        

          if ($articles->have_posts()): 
            while($articles->have_posts()): 
              $articles->the_post();
              $featured_img_url = get_the_post_thumbnail_url();
              $terms = get_the_terms($articles->post->ID, array($taxonomy2));
              echo '<a href="'.get_the_permalink().'">';
              echo '<div class="issues_box">';
                echo '<img class="issues__box__image" src="'.$featured_img_url.'" alt="Global Sport Matters Current Issue" />';
                echo '<div class="issues_box_inner">';
                  echo '<div class="issues__box__category">' . $terms[0]->name . '</div>';
                  echo '<h2 class="issues__box__title">' . get_the_title() . '</h2>';
                echo '</div>';
              echo '</div>';
              echo '</a>';
            endwhile;
          endif;

          wp_reset_postdata();
               
      ?>
  
    </div> <!-- .cell --> 
  </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>

<section id="post-quotebox-right" class="section issues-archive">
  <div class="grid-container full">
    <div class="grid-x grid-padding-x align-top">
      <div class="cell xlarge-8 align-right">
        <div class="grid-container">
          <div class="grid-x grid-padding-x">
            <?php 

              $taxonomy1 = 'monthly-issues';
              $taxonomy2 = 'monthly-issues-category';

              $terms = get_terms(array($taxonomy1, $taxonomy2));
              $term_ids = wp_list_pluck( $terms, 'term_id' );

              //var_dump($term_ids);

              $args = array( 
                'post_type' => 'issues',
                'posts_per_page' => 2,
                'tax_query' => array(
                  array(
                    'taxonomy' => $taxonomy1,
                    'field'    => 'term_id',
                    'terms'    => $term_ids,
                  ),
                  array(
                    'taxonomy' => $taxonomy2,
                    'field'    => 'term_id',
                    'terms'    => $term_ids,
                  ),
                ),
                'offset' => 1
              );

              $articles = new WP_Query($args);

              if ($articles->have_posts()): 
                while($articles->have_posts()): 
                  $articles->the_post();
                  $featured_img_url = get_the_post_thumbnail_url();
                  $terms = get_the_terms($articles->post->ID, array($taxonomy2));

                  echo '<div class="cell small-12 medium-6 large-6">';

                  echo '<a href="'.get_the_permalink().'">';
                  echo '<div class="insights_box">';
                    echo '<div class="insights__box__image"><img src="'.$featured_img_url.'" alt=""/></div>';
                    echo '<div class="insights_box_inner">'; 
                      echo '<div class="insights__box__category">' . $terms[0]->name . '</div>';
                      echo '<h2 class="insights__box__title">' . get_the_title() . '</h2>';
                    echo '</div>';
                  echo '</div>';
                  echo '</a>';

                  echo '</div>';
                endwhile;
              else:
                echo 'Sorry, there are currently no articles in this category.';
              endif;
              wp_reset_postdata();
            ?>
          </div> <!-- .grid-x --> 
        </div> <!-- .grid-container -->
      </div> <!-- .cell -->
      
      <div class="cell xlarge-4 quotebox__container">
        <div class="quotebox">
          <span class="quote">Informational tile card lorem ipsum dolor sit amet consect etur adipiscing elit.</span>
        </div> <!-- .post__links -->
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>

<section id="post-video" class="section issues-archive">
  <div class="grid-container full">
    <div class="grid-x grid-padding-x">
      <div class="cell">
        <div class="videoWrapper">
          <img src="/wp-content/themes/global-sport/assets/images/video-placeholder.png" alt="" />
        </div>
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container --> 
</section>

<section id="callout" class="section issues-archive">
  <div class="grid-container full">
    <div class="grid-x grid-padding-x text-center">
      <div class="cell callout">
        <p>The goal in studying the olympics is to contribute to quisque feugiat luctus varius. Donec metus tellus pharetra sit amet vulputate vehicula eleifend et urna.</p>
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>

<section id="post-quotebox-left" class="section issues-archive">
  <div class="grid-container full">
    <div class="grid-x grid-padding-x align-top">
      <div class="cell xlarge-4 quotebox__container">
        <div class="quotebox">
          <span class="quote">Informational tile card lorem ipsum dolor sit amet consect etur adipiscing elit.</span>
        </div> <!-- .post__links -->
      </div> <!-- .cell -->

      <div class="cell xlarge-8 align-right">
        <div class="grid-container">
          <div class="grid-x grid-padding-x">
            <?php 

              $taxonomy1 = 'monthly-issues';
              $taxonomy2 = 'monthly-issues-category';

              $terms = get_terms(array($taxonomy1, $taxonomy2));
              $term_ids = wp_list_pluck( $terms, 'term_id' );

              //var_dump($term_ids);

              $args = array( 
                'post_type' => 'issues',
                'posts_per_page' => 1,
                'tax_query' => array(
                  array(
                    'taxonomy' => $taxonomy1,
                    'field'    => 'term_id',
                    'terms'    => $term_ids,
                  ),
                  array(
                    'taxonomy' => $taxonomy2,
                    'field'    => 'term_id',
                    'terms'    => $term_ids,
                  ),
                ),
                'offset' => 3
              );

              $articles = new WP_Query($args);

              if ($articles->have_posts()): 
                while($articles->have_posts()): 
                  $articles->the_post();
                  $featured_img_url = get_the_post_thumbnail_url();
                  $terms = get_the_terms($articles->post->ID, array($taxonomy2));

                  echo '<div class="cell">';
                  echo '<a href="'.get_the_permalink().'">';
                  echo '<div class="insights_box">';
                    echo '<div class="insights__box__image"><img src="'.$featured_img_url.'" alt=""/></div>';
                    echo '<div class="insights_box_inner">'; 
                      echo '<div class="insights__box__category">' . $terms[0]->name . '</div>';
                      echo '<h2 class="insights__box__title">' . get_the_title() . '</h2>';
                    echo '</div>';
                  echo '</div>';
                  echo '</a>';
                  echo '</div>';
                endwhile;
              else:
                echo 'Sorry, there are currently no articles in this category.';
              endif;
              wp_reset_postdata(); ?>
          </div> <!-- .grid-x --> 
        </div> <!-- .grid-container -->
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>

<section id="post-grid-list-right" class="section issues-archive">
  <div class="grid-container full">
    <div class="grid-x grid-padding-x align-middle align-justify">
      <div class="cell small-12 medium-7 large-7 xlarge-7 align-right text-right">
        <?php 
          $taxonomy1 = 'monthly-issues';
          $taxonomy2 = 'monthly-issues-category';

          $terms = get_terms(array($taxonomy1, $taxonomy2));
          $term_ids = wp_list_pluck( $terms, 'term_id' );

          //var_dump($term_ids);

          $args = array( 
            'post_type' => 'issues',
            'posts_per_page' => 1,
            'tax_query' => array(
              array(
                'taxonomy' => $taxonomy1,
                'field'    => 'term_id',
                'terms'    => $term_ids,
              ),
              array(
                'taxonomy' => $taxonomy2,
                'field'    => 'term_id',
                'terms'    => $term_ids,
              ),
            ),
            'offset' => 4
          );

          $articles = new WP_Query($args);

          if ($articles->have_posts()): 
            while($articles->have_posts()): 
              $articles->the_post();
              $featured_img_url = get_the_post_thumbnail_url();
              $terms = get_the_terms($articles->post->ID, array($taxonomy2));
              
                echo '<div class="culture_box">';
                  echo '<div class="culture__box__image"><img src="'.$featured_img_url.'" alt=""/></div>';
                  echo '<div class="culture_box_inner">';
                    echo '<div class="culture__box__category">' . $terms[0]->name . '</div>';
                    echo '<h2 class="culture__box__title">' . get_the_title() . '</h2>';
                  echo '</div>';
                echo '</div>';
            endwhile;
          else:
            echo 'Sorry, there are currently no articles in this category.';
          endif;
          wp_reset_postdata();
        ?>
      </div>

      <div class="cell small-12 medium-5 large-5 xlarge-3 flex-container align-middle">
        <div class="post__links">
          <ul>
            <?php 
              $args = array( 
                'post_type' => 'issues',
                'posts_per_page' => 3, 
                'tax_query' => array(
                  array(
                    'taxonomy' => $taxonomy1,
                    'field'    => 'term_id',
                    'terms'    => $term_ids,
                  ),
                  array(
                    'taxonomy' => $taxonomy2,
                    'field'    => 'term_id',
                    'terms'    => $term_ids,
                  ),
                ),  
                'offset' => 5                       
              );

              $articles = new WP_Query($args);

              if ($articles->have_posts()): 
                while($articles->have_posts()): 
                  $articles->the_post();
                  $terms = get_the_terms($articles->post->ID, array($taxonomy2));

                  echo '<li class="'.strtolower($terms[0]->slug).'"><h6>'.$terms[0]->name.'</h6><a href="'.get_the_permalink().'">' . get_the_title() . '</a></li>';

                endwhile;
              endif;
              wp_reset_postdata();
            ?>      
          </ul>
        </div> <!-- .video__links -->
      </div> <!-- .cell --> 
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>

<section id="post-grid-3-col" class="section issues-archive">
  <div class="grid-container full">
  <ul class="grid-x grid-padding-x">
    <?php 

      $taxonomy1 = 'monthly-issues';
      $taxonomy2 = 'monthly-issues-category';

      $terms = get_terms(array($taxonomy1, $taxonomy2));
      $term_ids = wp_list_pluck( $terms, 'term_id' );

      //var_dump($terms);

      $numOfCols = 3;
      $rowCount = 0;
      $colWidth = 12 / $numOfCols;

      //var_dump($term_ids);

      $args = array( 
        'post_type' => 'issues',
        'posts_per_page' => 3,
        'tax_query' => array(
          array(
            'taxonomy' => $taxonomy1,
            'field'    => 'term_id',
            'terms'    => $term_ids,
          ),
          array(
            'taxonomy' => $taxonomy2,
            'field'    => 'term_id',
            'terms'    => $term_ids,
          ),
        ),
        'offset' => 8
      );

      $articles = new WP_Query($args);

      if ($articles->have_posts()): 
        while($articles->have_posts()): 
          $articles->the_post();
          $featured_img_url = get_the_post_thumbnail_url();
          $terms = get_the_terms($articles->post->ID, array($taxonomy2));
          
          echo '<li class="cell large-'.$colWidth.'">';
            echo '<a href="'.get_the_permalink().'">';
            echo '<div class="box">';
              echo '<div class="box__image"><img src="'.$featured_img_url.'" alt=""/></div>';
              echo '<div class="box_inner">'; 
                echo '<div class="box__category">' . $terms[0]->name . '</div>';
                echo '<h2 class="box__title">' . get_the_title() . '</h2>';
              echo '</div>';
            echo '</div>';
            echo '</a>';


            
        endwhile;
      endif; ?>
    </ul>
  </div> <!-- .grid-container -->
</section>

<section id="call-to-action" class="section issues-archive">
  <div class="grid-container">
    <div class="grid-x align-middle align-center text-center">
      <div class="cell">
        <p>Global Sport Matters lorem ipsum dolor sit amet consectetur adi piscing elit sed malesuada vehicula venenatis.</p>
        <a class="issues-cta gsm-button" href="<?= home_url() . '/issues/'; ?>">View All Issues</a>
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>

<?php get_footer(); ?>