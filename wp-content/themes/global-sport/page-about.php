<?php get_header(); ?>

<section id="headline" class="section gsm-about">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="cell text-left headline__container">
        <div class="headline__content">
          <h1 class="headline__title"><?= the_title(); ?></h2>
        </div> <!-- .headline__content -->
      </div> <!-- .headline__container --> 
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>

<section id="featured-issue" class="section gsm-about">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="cell">
        <?php 
          $featured_img = get_the_post_thumbnail_url();
          echo '<div class="issues_box">';
            echo '<img class="issues__box__image" src="'.$featured_img.'" alt="Global Sport Matters Current Issue" />';
          echo '</div>';
        ?>
      </div> <!-- .cell --> 
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>

<section id="wysiwyg-content" class="section gsm-about">
  <div class="grid-container">
    <div class="grid-x align-center">
      <div class="cell large-6">
        <?php the_content(); ?>
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container --> 
</section>

<section id="team" class="section gsm-about">
  <div class="grid-container">
    <ul class="grid-x grid-padding-x align-center">

    <?php 
      $numOfCols = 4;
      $teamCount = 0;
      $colWidth = 12 / $numOfCols;

      $args = array( 
        'post_type' => 'team',
        'posts_per_page' => 12,
        'order' => 'ASC'
      );

      $team_members = new WP_Query($args);

      if ($team_members->have_posts()): 
        while ($team_members->have_posts()): 
          $team_members->the_post();
          $featured_img = get_the_post_thumbnail_url();
          
          echo '<li class="cell large-'.$colWidth.'">';
            //echo '<a class="issues_link" href="/" title='.get_the_title().'">';
            echo '<div class="box">';
              echo '<img src="'.$featured_img.'" alt="GSM Team Image" title="'.get_the_title().'" />';
              echo '<h4>'.get_the_title().'</h4>';
              echo '<h6>'.get_the_content().'</h6>';
            echo '</div> <!-- .box -->';
            //echo '</a>';
          echo '</li> <!-- .cell -->';

        endwhile;
      endif; 

      wp_reset_postdata(); ?>

    </ul> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>

<section id="divider" class="section gsm-about">
  <div class="grid-container">
    <div class="grid-x align-center align-middle">
      <div class="cell">
        <hr/>
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>

<section id="connect" class="section gsm-about">
  <div class="grid-container">
    <div class="grid-x align-center">
      <div class="cell large-6">
        <h2>Connect with us</h2>
        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin magna nisi, porta sit amet consectetur feugiat, laoreet nec mauris. Donec tempus finibus ipsum eu convallis.</p>
        <div class="contact-info">
          Global Sport Matters<br/>
          1365 N. Scottsdale Rd., Suite 180<br/>
          Scottsdale, AZ 85257<br/>
          480-884-1515<br/>
          <a href="mailto:globalsport@asu.edu">globalsport@asu.edu</a>
        </div> <!-- .contact-info -->
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>

<?php get_footer(); ?>