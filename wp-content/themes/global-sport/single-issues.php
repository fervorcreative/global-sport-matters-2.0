<?php get_header(); ?>

<section id="featured-banner" class="section single-issue">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="cell">
        <?php 
          $featured_img_url = get_the_post_thumbnail_url();
          echo '<div class="single-issue-bg" style="background-image:url('.$featured_img_url.')"></div>';
        ?>          
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>

<section id="content" class="section single-issue">
  <div class="grid-container">
    <div class="grid-x align-justify">
      <div class="issues__box__category">
        <?php 
          $taxonomy = 'monthly-issues-category';
          $terms = get_the_terms( $post->ID , $taxonomy);
          foreach ( $terms as $term ) {
            echo $term->slug;
          }
        ?>        
      </div> <!-- .issue__category --> 

      <div class="cell large-8 single-article-copy">

        <h1 class="article-title"><?php the_title(); ?></h1>

        <div class="article-info">
          <span class="author-meta"><?= get_the_author_meta( 'nickname' ); ?> | <?php the_weekday_date('', ', ') . the_date(); ?> </span>
        </div> <!-- .article-info -->

        <div id="why-this-matters">
          <h6>Why This Matters</h6>
          <p>Sport’s camaraderie can help refugee children adapt to a new location because of the temporary failure and success competition entails.</p>
        </div> <!-- #why-this-matters -->

        <?php the_content(); ?>

        <div class="article-info">
          <span class="author-bio"><?= get_the_author_meta('description'); ?></span>

          <?php 
            global $post;    
            $terms = get_terms( array(
              'taxonomy' => 'issue_tags',
              'hide_empty' => false,
            ));

            if (!empty($terms) && !is_wp_error($terms)):
              foreach ($terms as $term):
                $issue_tags[] = $term->name;
              endforeach;

              echo '<span class="issue-tags">Tags: '. implode($issue_tags, ', ') .'</span>'; 
            endif;
          ?>
        </div> <!-- .article-info -->

        <div id="issue-description">
          <?php 
            $taxonomy = 'monthly-issues';
            $terms = get_the_terms($post->ID, $taxonomy);
            foreach ($terms as $term):
              echo '<h3>'.$term->name.'</h3>';
            endforeach;
          ?>
          <img src="/wp-content/themes/global-sport/assets/images/monthly-issue-bg.png" width="695" height="302" alt="Global Sport Matters Current Monthly Issue" />
          <p>This article is part of The Olympics, which examines lorem ipsum dolor sit amet consectetur adipiscing elit. Pellentesque ut lectus urna fusce eu arcu consequat.</p>
        </div> <!-- .issue-description -->

        <div id="related-articles">

          <h2>Related</h2>

          <?php 

            echo '<ul>';

            $taxonomy1 = 'monthly-issues';
            $taxonomy2 = 'monthly-issues-category';

            $terms = get_terms(array($taxonomy1, $taxonomy2));
            $term_ids = wp_list_pluck( $terms, 'term_id' );

            $args = array( 
              'post_type' => 'issues',
              'posts_per_page' => 3,
              'tax_query' => array(
                array(
                  'taxonomy' => $taxonomy1,
                  'field'    => 'term_id',
                  'terms'    => $term_ids,
                ),
                array(
                  'taxonomy' => $taxonomy2,
                  'field'    => 'term_id',
                  'terms'    => $term_ids,
                ),
              ),
              'offset' => 1
            );

            $articles = new WP_Query($args);

            if ($articles->have_posts()): 
              while($articles->have_posts()): 
                $articles->the_post();

                $terms = get_the_terms($articles->post->ID, array($taxonomy2));
                $featured_img_url = get_the_post_thumbnail_url();

                echo '<a href="'.get_the_permalink().'">';
                echo '<li class="flex-container">';
                echo '<div class="article-thumb"><img src="'.$featured_img_url.'" alt="" /></div>';
                echo '<div class="related-article-info">';
                echo '<div class="related-article-category">' . $terms[0]->name . '</div>';
                echo '<div class="related-article-title">' . get_the_title() . '</div>';
                echo '</div> <!-- .related-article-info -->';
                echo '</li>';
                echo '</a>';
              
              endwhile;
            endif; ?>

          </ul>

        </div> <!-- #related-articles -->

      </div> <!-- .cell --> 

      <div class="cell large-3 flex-container align-top align-center sidebar-container">
        <aside id="sidebar-related">
          <ul>

          <?php 

            $taxonomy1 = 'monthly-issues';
            $taxonomy2 = 'monthly-issues-category';

            $terms = get_terms(array($taxonomy1, $taxonomy2));
            $term_ids = wp_list_pluck( $terms, 'term_id' );

            $args = array( 
              'post_type' => 'issues',
              'posts_per_page' => 4,
              'tax_query' => array(
                array(
                  'taxonomy' => $taxonomy1,
                  'field'    => 'term_id',
                  'terms'    => $term_ids,
                ),
                array(
                  'taxonomy' => $taxonomy2,
                  'field'    => 'term_id',
                  'terms'    => $term_ids,
                ),
              ),
              'offset' => 4
            );

            $articles = new WP_Query($args);

            if ($articles->have_posts()): 
              while($articles->have_posts()): 
                $articles->the_post();

                $terms = get_the_terms($articles->post->ID, array($taxonomy2));
                $featured_img_url = get_the_post_thumbnail_url();

                echo '<li>';
                echo '<h3><a href="'.get_the_permalink().'">'.get_the_title().'</a></h3>';
                echo '</li>'; 
              endwhile;
            endif; ?>
          </ul>
        </aside>
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>

<?php get_footer(); ?>
