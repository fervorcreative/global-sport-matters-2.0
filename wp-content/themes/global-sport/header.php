<!DOCTYPE html>

<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
    <link rel="stylesheet" href="https://use.typekit.net/qmq4oti.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Roboto&display=swap" rel="stylesheet">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>
    
    <div id="promo-bar">
      <div id="promo">
        <div class="promo__close">
          <div class="promo-link js-promo-toggle">
            <div class="promo-link__outer">
              <div class="promo-link__icon"></div>
            </div> <!-- .promo-link__outer -->
          </div> <!-- .promo-link -->
        </div> <!-- .promo__close -->
      </div> <!-- #promo -->
    </div> <!-- #promo-bar -->

    <div class="site">
      <a href="#main" class="screen-reader-text">Skip to main content</a>
      <header class="header" role="banner">
        <div class="grid-container full">
          <div class="grid-x align-center align-middle">
            <div class="cell small-12 medium-12 large-12 header-nav">
              <div id="logo">
                <a class="header__logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                  <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo.png" alt="Global Sport Matters">
                </a>
              </div> <!-- #logo -->

              <div id="access">
                <?php wp_nav_menu(array('theme_location' => 'menu-header')); ?>
              </div> <!-- #access -->
            </div>
          </div>
        </div>

        <nav class="header__nav_offcanvas grid-container full">
          <div class="grid-x align-center align-middle flex-dir-column gsm-issues-menu">
            <div class="cell small-12 medium-6 large-6 current__issue">

              <h4>Current Issue</h4>

              <?php 
                $taxonomy = 'monthly-issues';
                $terms = get_terms($taxonomy, array('hide_empty' => false));

                if ($terms):
                  foreach ($terms as $term):
                      echo '<ul class="current-issue-menu">';
                      if ($term->slug == 'olympics'):
                        echo '<li class="current-issue-title"><a href="' . get_term_link($term->slug, $taxonomy) . '">' . $term->name . '</a></li>';
                      endif;
                      echo '</ul>';
                  endforeach;
                endif;
              ?>
            </div> <!-- .cell --> 

            <div class="cell small-9 medium-6 large-6 past__issues">

              <h4>Past Issues</h4>

              <?php 

                if ($terms):
                  foreach ($terms as $term):
                    echo '<ul class="past-issues-menu">';
                    if ($term->slug != 'olympics'):
                      echo '<li class="past-issues-title"><a href="' . get_term_link($term->slug, $taxonomy) . '">' . $term->name . '</a></li>';
                    endif;
                    echo '</ul>';
                  endforeach;
                endif;

                echo '<ul class="past-issue-menu">';
                echo '<li class="past-issue-title"><a href="'.home_url() .'/issues/">View All</a></li>';
                echo '</ul>';
              ?>
            </div> <!-- .cell -->
          </div> <!-- .grid-x -->

          <div class="grid-x align-center align-middle flex-dir-column gsm-sections-menu">
            <div class="cell small-12 medium-6 large-6 sections__menu">

              <h4>Sections</h4>

              <ul>

              <?php 

                $args = array(
                  'child_of'            => 0,
                  'current_category'    => 0,
                  'depth'               => 0,
                  'echo'                => 1,
                  'exclude'             => '',
                  'exclude_tree'        => '',
                  'feed'                => '',
                  'feed_image'          => '',
                  'feed_type'           => '',
                  'hide_empty'          => 0,
                  'hide_title_if_empty' => false,
                  'hierarchical'        => true,
                  'order'               => 'ASC',
                  'orderby'             => 'name',
                  'separator'           => '<br />',
                  'show_count'          => 0,
                  'show_option_all'     => '',
                  'show_option_none'    => __( 'No categories' ),
                  'style'               => 'list',
                  'taxonomy'            => 'category',
                  'title_li'            => false,
                  'use_desc_for_title'  => 1,
              );
              
              print_r(wp_list_categories($args)); ?>

              </ul>
            </div> <!-- .cell --> 

            <div class="cell small-9 medium-6 large-6 past__issues">
              
            </div> <!-- .cell -->
          </div> <!-- .grid-x -->








        </nav>
      </header>

      <div class="nav-link js-nav-toggle">
        <div class="nav-link__outer">
          <div class="nav-link__icon"></div>
        </div> <!-- .nav-link -->
      </div> <!-- .nav-link -->

      <main id="main" role="main">