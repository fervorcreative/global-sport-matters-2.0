<?php
/**
 * Add Support for Custom Menus
 */

add_theme_support( 'post-thumbnails' );

/**
 * Register Custom Menu Locations
 */

register_nav_menu( 'menu-header', 'Header' );
register_nav_menu( 'menu-footer', 'Footer' );
register_nav_menu( 'menu-header-offcanvas', 'OffCanvas Menu');

// Add body classes based on page name

function gsm_body_classes( $classes ) {
  global $post;

  if (is_page()):
    $current_page = $post->post_name;
    $classes[] = $current_page;
  endif;

  //if (is_archive()):
    //$current_archive = $post->post_type;
    //$classes[] = $current_archive . '-archive';
  //endif; 
  
  return $classes;
}
add_filter( 'body_class','gsm_body_classes' );

function init_enqueue_css() {
  wp_enqueue_style( 'slick-css', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', array(), null);
  wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Josefin+Sans:300,400,400i,700', array(), null);
  wp_enqueue_style( 'magnific-poup', get_template_directory_uri() . '/dist/magnific-popup.css', array(), null);
  wp_enqueue_style( 'theme', get_template_directory_uri() . '/dist/app.css', array(), null);
}

add_action( 'wp_enqueue_scripts', 'init_enqueue_css' );

function init_enqueue_js() {
  if ( ! is_admin() ) {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', false, false, true);
    wp_enqueue_script( 'jquery' );
  }
  //wp_enqueue_script( 'slick-js', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array( 'jquery' ), null, true );
  wp_enqueue_script( 'app', get_template_directory_uri() . '/dist/app.js', array( 'jquery' ), null, true );

	// Localize template URL for usage in JS
	$data = array(
    'template_dir' => get_stylesheet_directory_uri(),
    
	);
	wp_localize_script( 'app', 'AppData', $data );
}
add_action( 'wp_enqueue_scripts', 'init_enqueue_js' );

if ( ! function_exists( 'twentyten_continue_reading_link' ) ) :
	function gsm_continue_reading_link() {
		return ' <a class="issues-cta" href="' . get_permalink() . '">' . __( 'View Full Issue', 'gsm' ) . '</a>';
	}
endif;

function gsm_auto_excerpt_more( $more ) {
	if ( ! is_admin() ) {
    if (!is_front_page()):
      return ' &hellip;' . gsm_continue_reading_link();
    else: 
      return ' &hellip;';
    endif;
	}
	return $more;
}

if (!is_front_page()):
  add_filter( 'excerpt_more', 'gsm_auto_excerpt_more' );
endif;

function gsm_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() && ! is_admin() ) {
		$output .= gsm_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'gsm_custom_excerpt_more' );

// Add a user friendly Label for Custom Field Sections

function gsm_layout_title($title, $field, $layout, $i) {
	if($value = get_sub_field('layout_title')) {
		return $value;
	} else {
		foreach($layout['sub_fields'] as $sub) {
			if($sub['name'] == 'layout_title') {
				$key = $sub['key'];
				if(array_key_exists($i, $field['value']) && $value = $field['value'][$i][$key])
					return $value;
			}
		}
	}
	return $title;
}
add_filter('acf/fields/flexible_content/layout_title', 'gsm_layout_title', 10, 4);

// Add a ACF Options page

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Global Sport',
		'menu_title'	=> 'Theme Options',
		'menu_slug' 	=> 'global-sport-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

// Modifier classes to add top padding styles to ACF Field Sections

function padding_top_classes() {
  $section_paddingtop = (!empty(get_sub_field('section_padding_top')) ? get_sub_field('section_padding_top') : '');
  if (!empty($section_paddingtop)):
    if ($section_paddingtop == 'none'):
      echo ' nopadding-top';
    elseif ($section_paddingtop == 'small'):
      echo ' paddingtop-small';
    elseif ($section_paddingtop == 'medium'):
      echo ' paddingtop-medium';
    elseif ($section_paddingtop == 'large'):
      echo ' paddingtop-large';
    elseif ($section_paddingtop == 'xlarge'):
      echo ' paddingtop-xlarge';
    elseif ($section_paddingtop == 'xxlarge'):
      echo ' paddingtop-xxlarge';
    else:
      echo '" style="padding-top:'.$section_paddingtop.'px';
    endif;
  endif;
  } 

  // Modifier classes to add bottom padding styles to ACF Field Sections
  
  function padding_bottom_classes() {
  $section_paddingbottom = (!empty(get_sub_field('section_padding_bottom')) ? get_sub_field('section_padding_bottom') : ''); 
  if (!empty($section_paddingbottom)):
    if ($section_paddingbottom == 'none'):
      echo ' nopadding-bottom';
    elseif ($section_paddingbottom == 'small'):
      echo ' paddingbottom-small';
    elseif ($section_paddingbottom == 'medium'):
      echo ' paddingbottom-medium';
    elseif ($section_paddingbottom == 'large'):
      echo ' paddingbottom-large';
    elseif ($section_paddingbottom == 'xlarge'):
      echo ' paddingbottom-xlarge';
    elseif ($section_paddingbottom == 'xxlarge'):
      echo ' paddingbottom-xxlarge';
    else:
      echo '" style="padding-bottom:'.$section_paddingbottom.'px';
    endif;
  endif;
}

// Create a Post Type for Team Members

function team_cpt() {
  register_post_type('team',
    array(
      'labels' => array(
        'name' => __('Team', 'gsm'),
        'singular_name' => __('Team', 'gsm'),
        
      ),
      'public' => true,
      'has_archive' => 'team',
      'menu_icon' => 'dashicons-businessman',
      'supports' => array( 'title', 'editor', 'custom-fields', 'thumbnail'),
      'publicly_queryable' => false,
    )
  );
}
add_action('init', 'team_cpt');

// Show Team member images in the Post Table

function display_team_images ($columns) {
  $columns = array(
      'cb' => '<input type="checkbox" />',
      'title' => 'Title',
      'featured_image' => 'Featured Image',
      'date' => 'Date'
   );
  return $columns;
}

function display_team_images_data ($column, $post_id) {
  switch ( $column ) {
  case 'featured_image':
      the_post_thumbnail( 'thumbnail' );
      break;
  }
}

// Display featured images only for the Teams CPT 

function display_team_images_data_when() {

  global $pagenow;

  if ('edit.php' === $pagenow && isset($_GET['post_type']) && $_GET['post_type'] === 'team'):
    add_filter('manage_posts_columns' , 'display_team_images');
    add_action( 'manage_posts_custom_column' , 'display_team_images_data', 10, 2 ); 
  endif;
}
add_action( 'admin_init', 'display_team_images_data_when' );

// Create a Post Type for monthly issues

function issues_cpt() {
  register_post_type('issues',
    array(
      'labels' => array(
        'name' => __('Articles', 'gsm'),
        'singular_name' => __('Issue', 'gsm'),
        
      ),
      'public' => true,
      'has_archive' => 'issues',
      'taxonomies' => array('monthly-issues-categories'),
      'rewrite' => array('slug' => 'issues/%monthly-issues%'),
      'menu_icon' => 'dashicons-admin-page',
      'supports' => array( 'title', 'excerpt', 'editor', 'custom-fields', 'thumbnail'),
      'publicly_queryable' => true,
    )
  );
}
add_action('init', 'issues_cpt');

// Create a taxonomy that assigns an article to a monthly issue

function monthly_issues_taxonomy() {

  $labels = array (
    'name' => 'Monthly Issue',
    'menu_name' => 'Issues',
    'singular_name' => 'Monthly Issue',
    'add_new_item' => 'Add Monthly Issue',
    'new_item_name' => 'New Monthly Issue',
    'parent_item' => __( 'Parent Monthly Issue' ),
    'parent_item_colon' => __( 'Parent Monthly Issue' ),
    'update_item' => __( 'Update Monthly Issue' ),
  );

  register_taxonomy(
    'monthly-issues',
    'issues',
    array(
      'labels' => $labels,
      'rewrite' => array('slug' => 'issues', 'with_front' => true),
      'hierarchical' => true,
      'query_var' => true,
      'show_admin_column' => true,
      'publicly_queryable' => true,
    )
  );
}
add_action( 'init', 'monthly_issues_taxonomy' );

// Create a taxonomy that assigns an article to a Category within a monthly issue

function monthly_issues_category_taxonomy() {

  $labels = array (
    'name' => 'Article Category',
    'menu_name' => 'Article Categories',
    'singular_name' => 'Article Category',
    'add_new_item' => 'Add Article Category',
    'new_item_name' => 'New Article Category',
    'parent_item' => __( 'Parent Article Category' ),
    'parent_item_colon' => __( 'Parent Article Category' ),
    'update_item' => __( 'Update Article Category' ),
  );

  register_taxonomy(
    'monthly-issues-category',
    'issues',
    array(
      'labels' => $labels,
      'rewrite' => array('slug' => 'issues/%monthly-issues%/', 'with_front' => true),
      'hierarchical' => true,
      'show_in_menu' => true,
      'show_admin_column' => true,
      'publicly_queryable' => true,
    )
  );
}
add_action( 'init', 'monthly_issues_category_taxonomy' );

// Create a taxonomy that allows Tags for Monthly Issues

function monthly_issues_tag_taxonomy() {
  
  $labels = array(
    'name' => 'Article Tags',
    'singular_name' => 'Article Tag',
    'search_items' =>  __( 'Search Article Tags' ),
    'popular_items' => __( 'Popular Article Tags' ),
    'all_items' => __( 'All Article Tags' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Article Tag' ), 
    'update_item' => __( 'Update Article Tag' ),
    'add_new_item' => __( 'Add New Article Tag' ),
    'new_item_name' => __( 'New Article Tag Name' ),
    'separate_items_with_commas' => __( 'Separate article tags with commas' ),
    'add_or_remove_items' => __( 'Add or remove article tags' ),
    'choose_from_most_used' => __( 'Choose from the most used article tags' ),
    'menu_name' => __( 'Article Tags' ),
  ); 

  register_taxonomy('issue_tags','issues', array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'tag' ),
  ));
}
add_action( 'init', 'monthly_issues_tag_taxonomy', 0 );

// Modify the taxonomy url structure

function monthly_issues_post_link( $post_link, $id = 0 ){
  $post = get_post($id);  
  $terms = wp_get_object_terms( $post->ID, array('monthly-issues') );

  if( $terms ):
    return str_replace( '%monthly-issues%' , $terms[0]->slug , $post_link );
  else:
    return str_replace( '%monthly-issues%/' , '' , $post_link );
  endif;
    return $post_link;  
}
add_filter( 'post_type_link', 'monthly_issues_post_link', 1, 3 );

/**
 * Add theme utility functions
 */

require_once( get_template_directory() . '/includes/functions-defaults.php' );
require_once( get_template_directory() . '/includes/functions-helpers.php' );
