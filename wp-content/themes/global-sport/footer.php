</main>

<footer id="footer" class="section" role="contentinfo">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="cell large-auto">
        <img src="/wp-content/themes/global-sport/assets/images/gsm-footer-logo.png" alt="Global Sport Matters Logo" />
        <span class="gsm-description">Global Sport Matters is a joint initiative between <span class="underline">Arizona State University’s Global Sport Institute<br/> and the Walter Cronkite School of Journalism and Mass Communication</span></span><br/>
        <span class="copyright">© 2020 Global Sport Matters. All rights reserved.</span>
      </div> <!-- .cell --> 

      <div class="cell large-auto align-right text-right">
        <div class="social">
          <ul> 
            <li><a class="twitter" href="#"></a></li>
            <li><a class="facebook" href="#"></a></li>
            <li><a class="youtube" href="#"></a></li>
            <li><a class="instagram" href="#"></a></li>
          </ul>
        </div> <!-- .social -->
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
